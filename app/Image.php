<?php namespace App;
/**
 * Created by PhpStorm.
 * User: hargcb
 * Date: 4/19/2015
 * Time: 10:41 PM
 */
use Illuminate\Database\Eloquent\Model;

/**
 * Class Image
 *
 * @package App
 */
class Image extends Model
{

    /**
     * @return mixed
     */
    public function category()
    {
        return $this->hasOne('App\Category','cat_id','cat_id');
    }

    /**
     * @return mixed
     */
    public function booking()
    {
        return $this->belongsTo('App\Customer','bok_id','bok_id');
    }

    /**
     * @return mixed
     */
    public function group()
    {
        return $this->belongsTo('App\Group', 'group_id', 'group_id');
    }

    /**
     * @var string
     */
    protected $primaryKey = 'img_id';
    /**
     * @var string
     */
    protected $connection = 'mysql3';

    /**
     * @var string
     */
    protected $table = 'image';
    /**
     * @var string
     */

    protected $fillable = ['img_id','group_id','bok_num','cat_id','bok_id','img_name','img_path','img_size','img_url','img_ext','img_mime','img_enc','img_notes','uploaded_by'];


}