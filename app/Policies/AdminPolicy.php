<?php

namespace App\Policies;
use App\Admin;
use App\User;

use Illuminate\Auth\Access\HandlesAuthorization;

class AdminPolicy
{
    use HandlesAuthorization;

    /**
     * Create a new policy instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    public function update(User $user, Admin $post)
    {
        return $user->id === $post->user_id;
    }

    public function newUser(User $user, Post $post)
    {
        return $user->id === $post->user_id;
    }

    public function viewAllAssociates(User $user, Post $post)
    {
        return $user->id === $post->user_id;
    }

}
