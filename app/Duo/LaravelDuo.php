<?php namespace App\Duo;
/**
 * Created by PhpStorm.
 * User: Casey Hargarther
 * Date: 2/2/2016
 * Time: 9:55 PM
 */

/**
 * Class LaravelDuo
 * @package App\Duo
 */
class LaravelDuo extends Duo {

    /**
     * @var string
     */
    private $_AKEY = 'ef03eeb9c45c56337ef82dcdd39d1cd56872fdb8';
    /**
     * @var string
     */
    private $_IKEY = 'DIHNYFLP1AUCZ3PCXF6B';
    /**
     * @var string
     */
    private $_SKEY = 'DHX21DgjUFtTCpzQHbbwyCegnoFXOTfmUzB37LW1';
    /**
     * @var string
     */
    private $_HOST = 'api-7b93c744.duosecurity.com';

    /**
     * @return string
     */
    public function get_akey()
    {
        return $this->_AKEY;
    }

    /**
     * @return string
     */
    public function get_ikey()
    {
        return $this->_IKEY;
    }

    /**
     * @return string
     */
    public function get_skey()
    {
        return $this->_SKEY;
    }

    /**
     * @return string
     */
    public function get_host()
    {
        return $this->_HOST;
    }

}