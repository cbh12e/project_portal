<?php
/**
 * Created by PhpStorm.
 * User: hargcb
 * Date: 1/10/2016
 * Time: 7:23 PM
 */

namespace App\Services;

use App;
use AWS;
use Carbon\Carbon;
use Dflydev\ApacheMimeTypes\PhpRepository;
use Illuminate\Support\Facades\Storage;

/**
 * Class File
 *
 * @package app
 */
class AmazonS3
{

    /**
     * @var
     */
    protected $client;

    /**
     * @var
     */
    protected $bucket;

    /**
     * @var
     */
    protected $file;

    /**
     * @var
     */
    protected $extension;

    /**
     * @var
     */
    protected $key;

    /**
     * @var string
     */
    private $encryption = 'AES256';

    /**
     * @var string
     */
    private $permission = 'authenticated-read';

    /**
     *
     */
    public function __construct()
    {
        $this->client = App::make('aws')->createClient('s3');
        $this->bucket = env('AWS_BUCKET');
    }

    /**
     * @param $file
     * @param $key
     * @param $extension
     */
    public function s3Upload($file, $key, $extension)
    {
        /**
         *  Uploads Documents to Amazon S3.
         */

     return $this->client->putObject
        ([
            'Bucket'               => $this->bucket,
            'Key'                  => $this->getKey($key),
            'SourceFile'           => $this->getSourceFile($file),
            'ContentType'          => $extension,
            'ACL'                  => $this->permission,
            'ServerSideEncryption' => $this->encryption
        ]);
    }

    /**
     * @param $key
     *
     * @return mixed
     */
    public function getKey($key)
    {
        return $key;
    }

    /**
     * @param $file
     *
     * @return mixed
     */
    public function getSourceFile($file)
    {
        return $file->getRealPath();
    }

    /**
     * @param $key
     */
    public function s3Delete($key)
    {
        /**
         * Deletes a file from Amazon S3
         */

      return $this->client->deleteObject
        ([
            'Bucket' => $this->bucket,
            'Key'    => $this->getKey($key)
        ]);
    }

    /**
     * @param $fileName
     * @param $file
     *
     * @return string
     */
    public function getFullFileName($fileName, $file)
    {
        return $fileName. '.'. $this->getExtension($file);
    }

}