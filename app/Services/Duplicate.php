<?php
/**
 * Created by PhpStorm.
 * User: hargcb
 * Date: 1/30/2016
 * Time: 4:05 PM
 */

namespace App\Services;

use App;
use App\Customer;
use Illuminate\Support\Facades\DB;
use Request;
/**
 * Class Duplicate
 *
 * @package App\Services
 */
class Duplicate
{
    /**
     * @var string
     */
    protected $firstName;

    /**
     * @var string
     */
    protected $lastName;

    /**
     * @var int
     */
    protected $associate;

    /**
     * @var int
     */
    protected $count;

    protected $check;

    /**
     * Checks if there is a record in the customer table that already exists the entered customer.
     *
     * @param $firstName = First Name of the Customer
     * @param $lastName = Last Name of the Customer
     * @param $associate = Associate
     *
     * @return integer = Returns the count of the records with the name stored in the database
     */
    public function checkCustomer($firstName, $lastName, $associate)
    {

        $count = Customer::where('cus_fname','=', $firstName)
            ->where('cus_lname','=',$lastName)
            ->where('aso_id',$associate)
            ->count();

          return  $count;
    }


    public function getCustomerCount()
    {

    }


    /**
     * This Checks the booking to see if the customer is already attached to the booking
     *
     * @param integer $customer = Customer ID
     * @param integer $booking = Booking ID
     *
     * @return integer $check = Record Count
     */
    public function checkBooking($customer, $booking)
    {

        $check = DB::connection('mysql3')
            ->table('customer')
            ->select('booking.bok_id', 'customer.cus_id')
            ->join('customer_booking', 'customer.cus_id', '=', 'customer_booking.cus_id')
            ->join('booking', 'customer_booking.bok_id', '=', 'booking.bok_id')
            ->where('customer.cus_id', '=', $customer)
            ->where('booking.bok_id', '=', $booking)
            ->count();

        return $check;
    }

    public function checkGroupCustomer($customer, $group)
    {

        $check = DB::connection('mysql3')
            ->table('customer')
            ->select('booking.bok_id', 'customer.cus_id','group.group_number','group.group_id')
            ->join('customer_booking', 'customer.cus_id', '=', 'customer_booking.cus_id')
            ->join('booking', 'customer_booking.bok_id', '=', 'booking.bok_id')
            ->join('group','booking.group_id', '=', 'group.group_id')
            ->where('customer.cus_id', '=', $customer)
            ->where('group.group_number', '=', $group)
            ->count();

        return $check;
    }


    /**
     * Returns this message when there is a duplicate customer in the database in JSON.
     *
     * @param string $firstName = Customer First Name
     * @param string $lastName  = Customer Last Name
     *
     * @return mixed
     */
    public function customerDuplicate($firstName, $lastName)
    {
        if (Request::ajax()) {
            return response()->json([
                'message' => ['An existing customer with the name of ' . $firstName, ' ', $lastName . ' already exists in the database.']
            ], 422);
        }
        else{
                return back()->with('exception','There is already a record for '. $firstName. ' ' .$lastName .' in the database.')->withInput();
            }
        }


    /**
     * Returns this message when there is a duplicate customer in the database.
     *
     * @param string $firstName = Customer First Name
     * @param string $lastName  = Customer Last Name
     *
     * @return mixed
     */
    public function customerDuplicate1($firstName, $lastName)
    {
        return back()->with('exception','There is already a record for '. $firstName. ' ' .$lastName .' in the database.')->withInput();

    }

    /**
     * Returns this message when a customer is already attached to a booking.
     *
     * @return mixed
     */
    public function customerBookingDuplicate()
    {
        return response()->json([
            'reservation' => ['The customer is already a customer of this booking.']
        ], 422);

    }

    /**
     * Returns this message when a customer is already attached to a booking.
     *
     * @return mixed
     */
    public function groupBookingDuplicate()
    {
        return back()->with('exception','There is already a record for the selected customer in the selected group.')->withInput();

    }

    /**
     * @return mixed
     */
    public function customerLimit()
    {
        /**
         * Returns a validation error message when there are more than five customers attached to a single booking.
         */

        return response()->json([
            'reservation' => ['Bookings cannot have more than five customers.']
        ], 422);

    }
}