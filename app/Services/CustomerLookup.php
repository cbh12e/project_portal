<?php
/**
 * Created by PhpStorm.
 * User: Casey Hargarther
 * Date: 3/14/2016
 * Time: 5:12 PM
 */

namespace App\Services;

use DB;
Use App\Ship;
Use App\Land;
/**
 * Class CustomerLookup
 * @package App\Services
 */
class CustomerLookup{

    /**
     * @param $customer
     * @param $id
     */
    public function defaultLookup($id)
    {
        return DB::connection('mysql3')
            ->table('customer')->distinct()
            ->select('booking.bok_id','booking.bok_selector','booking.bok_type' ,'customer.cus_id AS cust',
                'customer.cus_selector as cus', DB::raw('CONCAT(cus_fname, " ", cus_lname) AS FullName'),'cus_fname', 'cus_lname',
                'land.land_name AS LandProvider', DB::raw('CONCAT(aso_fname, " ", aso_lname) AS aso_name'), 'img_name',
                'cruiseship.ship_name AS bok_ship', 'bok_date', 'associate.aso_id', 'img_path', 'cus_notes', 'bok_reservation')
            ->leftJoin('customer_booking', 'customer.cus_id', '=', 'customer_booking.cus_id')
            ->leftJoin('booking', 'customer_booking.bok_id', '=', 'booking.bok_id')
            ->leftJoin('cruiseship', 'booking.ship_id', '=', 'cruiseship.ship_id')
            ->leftJoin('land', 'booking.land_id', '=', 'land.land_id')
            ->leftJoin('associate', 'customer.aso_id', '=', 'associate.aso_id')
            ->leftJoin('image', 'booking.bok_id', '=', 'image.bok_id')
            ->where('customer.cus_selector', '=', $id)
            ->groupBy('booking.bok_id')
            ->orderBy('bok_date', 'desc')
            ->get();

    }

    /**
     * @param $customer
     * @param $id
     * @return mixed
     */
    public function testLookup($id)
    {
       return DB::connection('mysql3')
           ->table('customer')->distinct()
           ->select('booking.bok_id','booking.bok_selector','booking.bok_type' ,'customer.cus_id AS cust',
                'customer.cus_selector as cus', DB::raw('CONCAT(cus_fname, " ", cus_lname) AS FullName'),'cus_fname', 'cus_lname',
                'land.land_name AS LandProvider', DB::raw('CONCAT(aso_fname, " ", aso_lname) AS aso_name'), 'img_name',
                'cruiseship.ship_name AS bok_ship', 'bok_date', 'associate.aso_id', 'img_path', 'cus_notes', 'bok_reservation')
           ->leftJoin('customer_booking', 'customer.cus_id', '=', 'customer_booking.cus_id')
           ->leftJoin('booking', 'customer_booking.bok_id', '=', 'booking.bok_id')
           ->leftJoin('cruiseship', 'booking.ship_id', '=', 'cruiseship.ship_id')
           ->leftJoin('land', 'booking.land_id', '=', 'land.land_id')
           ->leftJoin('associate', 'customer.aso_id', '=', 'associate.aso_id')
           ->leftJoin('image', 'booking.bok_id', '=', 'image.bok_id')
           ->where('customer.cus_selector', '=', $id)
           ->where('associate.aso_id', 99)
           ->groupBy('booking.bok_id')
           ->orderBy('bok_date', 'desc')
           ->get();
    }

    /**
     * @param $ship
     */
    public function shipIdentity($ship)
    {
        $id = Ship::where('ship_name',$ship)->firstOrFail();

        return $id->ship_id;
    }

    /**
     * @param $land
     */
    public function landIdentity($land)
    {
        $id = Land::where('land_name',$land)->firstOrFail();

        return $id->land_id;
    }

}