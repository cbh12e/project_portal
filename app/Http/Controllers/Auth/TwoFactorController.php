<?php
/**
 * Created by PhpStorm.
 * User: hargcb
 * Date: 12/29/2015
 * Time: 9:14 PM
 */

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Http\Requests\TwoFactorAuth\EnableTwoFactorRequest;
use App\Http\Requests\TwoFactorAuth\DisableTwoFactorRequest;
use App\User;
use PragmaRX\Google2FA\Google2FA as Google2FA;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Request;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\View;

/**
 * Class TwoFactorController
 *
 * @package app\Http\Controllers\Auth
 */
class TwoFactorController extends Controller
{

    protected $user;

    protected $insert;

    protected $google2fa;

    protected $google2fa_url;


    /**
     * TwoFactorController constructor.
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Enable Two Factor Authentication
     *
     * @param \App\Http\Requests\TwoFactorAuth\EnableTwoFactorRequest $enable
     * @param \PragmaRX\Google2FA\Google2FA                           $google2fa
     *
     * @return \Illuminate\Support\Facades\Redirect
     */
    public function enable2Fa(EnableTwoFactorRequest $enable, Google2FA $google2fa)
    {
        $user = User::find(Auth::user()->id);

        if ($user->google_2fa_secret == null) {

            $user->google_2fa_secret = $google2fa->generateSecretKey();

            $user->save();

            return redirect('qr');
        }

        else
        {
            return redirect('home')->with('exception', 'Two Factor Authentication has already been enabled!');
        }
    }

    /**
     * Disable Two Factor Authentication
     *
     * @param \App\Http\Requests\TwoFactorAuth\DisableTwoFactorRequest $disable
     * @param \PragmaRX\Google2FA\Google2FA                            $google2fa
     *
     * @return mixed
     */
    public function disable2Fa(DisableTwoFactorRequest $disable, Google2FA $google2fa)
    {

        $user = User::find(Auth::user()->id);

        if ($user->google_2fa_secret !== null) {

            $user->google_2fa_secret = null;

            $user->save();

            return redirect('home')->with('exception', 'Two Factor Authentication has been successfully disabled!');
        }
        else
        {
            return redirect('home')->with('exception', 'Two Factor Authentication was already disabled!');
        }
    }

    /**
     * Shows the user their QR Barcode
     *
     * @param \PragmaRX\Google2FA\Google2FA $google2fa
     *
     * @return \Illuminate\Support\Facades\View
     */
    public function getQR(Google2FA $google2fa)
    {

        $insert = User::find(Auth::user()->id);

        if(Auth::user()->google_2fa_secret !== null) {
            $google2fa_url = $google2fa->getQRCodeGoogleUrl(
                'CruiseOne',
                $insert->email,
                $insert->google_2fa_secret
            );
            return view('admin.two-factor.qr', compact('google2fa_url'));
        }
        else{

            return redirect('home')->with('exception', 'Two Factor Authentication is not enabled for your account!');
        }
    }
}