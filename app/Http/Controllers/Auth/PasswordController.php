<?php namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Contracts\Auth\Guard;
use Illuminate\Contracts\Auth\PasswordBroker;
use Illuminate\Foundation\Auth\ResetsPasswords;
use App\Http\Requests\Auth\ResetRequest;
use Mail;

/**
 * Class PasswordController
 *
 * @package App\Http\Controllers\Auth
 */
class PasswordController extends Controller {

	/*
	|--------------------------------------------------------------------------
	| Password Reset Controller
	|--------------------------------------------------------------------------
	|
	| This controller is responsible for handling password reset requests
	| and uses a simple trait to include this behavior. You're free to
	| explore this trait and override any methods you wish to tweak.
	|
	*/

	use ResetsPasswords;

	/**
	 * Create a new password controller instance.
	 *
	 * @param  \Illuminate\Contracts\Auth\Guard  $auth
	 * @param  \Illuminate\Contracts\Auth\PasswordBroker  $passwords
	 * @return void
	 */
	public function __construct(Guard $auth, PasswordBroker $passwords)
	{
		$this->auth = $auth;
		$this->passwords = $passwords;
		$this->subject = 'Your Password Reset Link';
		$this->middleware('guest');
	}


	/**
	 * @param \App\Http\Requests\Auth\ResetRequest $request
	 *
	 * @return mixed
     */
    public function postReset(ResetRequest $request)
	{
		$credentials = $request->only(
			'email', 'password', 'password_confirmation', 'token', 'username'
		);
		$response = $this->passwords->reset($credentials, function($user, $password)
		{
			$user->password = bcrypt($password);
			$user->save();

			Mail::send('emails.reset', ['user' => $user], function ($m) use ($user) {
				$m->to($user->email, $user->name)->subject('Password has been successfully reset!');
			});
			//$this->auth->login($user);
		});

		switch ($response)
		{
			case PasswordBroker::PASSWORD_RESET:
				return redirect('/')->with('success','Your Password has been successfully reset.');
			default:
				return redirect()->back()
					->withInput($request->only('email'))
					->withErrors(['email' => trans($response)]);
		}
	}
}