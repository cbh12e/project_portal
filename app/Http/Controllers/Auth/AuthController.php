<?php namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Http\Requests\Auth\LoginRequest;
use App\Duo\LaravelDuo;
use App\User;
use Crypt;
use Validator;
use GrahamCampbell\Binput\Facades\Binput;
use Illuminate\Foundation\Auth\ThrottlesLogins;
use Illuminate\Foundation\Auth\AuthenticatesAndRegistersUsers;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Lang;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Request;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\View;
use Illuminate\Support\Facades\Input;

/**
 * Class AuthController
 *
 * @package App\Http\Controllers\Auth
 */
class AuthController extends Controller {

    /*
    |--------------------------------------------------------------------------
    | Registration & Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users, as well as the
    | authentication of existing users. By default, this controller uses
    | a simple trait to add these behaviors. Why don't you explore it?

    */


    use AuthenticatesAndRegistersUsers, ThrottlesLogins;

    /**
     * @var
     */
    protected $code;
    /**
     * @var
     */
    protected $userId;
    /**
     * @var
     */
    protected $loginData;
    /**
     * @var
     */
    protected $factor;
    /**
     * @var
     */
    protected $insert;
    /**
     * @var
     */
    protected $keyGen;
    /**
     * @var
     */
    protected $value;
    /**
     * @var
     */
    protected $duoInfo;
    /**
     *
     */

    private $_laravelDuo;

    /**
     * AuthController constructor.
     * @param LaravelDuo $laravelDuo
     */
    public function __construct(LaravelDuo $laravelDuo)
    {
        $this->middleware('guest', ['except' => 'getLogout']);
        $this->_laravelDuo = $laravelDuo;
    }


    /**
     * @param LoginRequest $request
     * @return View
     */
    public function postLogin(LoginRequest $request)
    {

        $throttles = $this->isUsingThrottlesLoginsTrait();

        if ($throttles && $this->hasTooManyLoginAttempts($request)) {
            return $this->sendLockoutResponse($request);
        }

        $user = Binput::only(['username', 'password']);


        /**
         * Validate the user details, but don't log the user in
         */
       if(Auth::validate($user)) {
           if ($throttles) {
               $this->clearLoginAttempts($request);
           }


           $factor = User::where('username', $request->username)->firstOrFail();

           /**
            * Check to see if the user is active. If true -> proceed to login.  For false -> logout and display message
            */

           switch ($factor->active) {
               case 'Inactive':
                   return redirect($this->loginPath())->with('flash_message', 'Your user account is not active.');
                   break;
               case 'Suspended':
                   return redirect($this->loginPath())->with('flash_message', 'Your user account is currently suspended.');
                   break;
               case 'Active':
                   $duoInfo = [
                       'HOST' => $this->_laravelDuo->get_host(),
                       'POST' => twoFactorSigned(asset('auth/duo')),
                       'USER' => $factor->username,
                       'SIG' => $this->_laravelDuo->signRequest($this->_laravelDuo->get_ikey(), $this->_laravelDuo->get_skey(), $this->_laravelDuo->get_akey(), $factor->username)
                   ];
                   return view('auth.duo', ['duoInfo' => $duoInfo]);
                   break;
           }
       }
        else{
            /**
             * Login failed, so the server will record this failed login attempt.
             */

            $this->incrementLoginAttempts($request);
        }

        return back()->withInput(Binput::except('password'))->withErrors([
            $this->loginUsername() => $this->getFailedLoginMessage(),
        ]);
    }

    /**
     * Determine if the class is using the ThrottlesLogins trait.
     *
     * @return bool
     */
    protected function isUsingThrottlesLoginsTrait()
    {
        return in_array(
            ThrottlesLogins::class, class_uses_recursive(get_class($this))
        );
    }

    /**
     * Get the path to the login route.
     *
     * @return string
     */
    public function loginPath()
    {
        return property_exists($this, 'loginPath') ? $this->loginPath : '/auth/login';
    }

    /**
     * Get the failed login message.
     *
     * @return string
     */
    protected function getFailedLoginMessage()
    {
        return Lang::has('auth.failed')
            ? Lang::get('auth.failed')
            : 'These credentials do not match our records.';
    }

    /**
     * Logs the user out, deleting their session etc.
     *
     * @return \Illuminate\Http\RedirectResponse
     */
    public function logoutAction()
    {
        /**
         * Logs the user out, deletes their session cookie and redirects to the home page
         */
        Auth::logout();
        segment_track('Logged Out');
        return redirect($this->loginPath())->with('affirmative','You have successfully been logged out');
    }

    /**
     * Stage Three - After Duo Auth Form
     */
    public function postDuoLogin()
    {
        /**
         * Sent back from Duo
         */
        $response = $_POST['sig_response'];
        $attempt = $this->_laravelDuo->verifyResponse($this->_laravelDuo->get_ikey(), $this->_laravelDuo->get_skey(), $this->_laravelDuo->get_akey(), $response);
        /**
         * Duo response returns USER field from Stage Two
         */
        if($attempt){
            /**
             * Get the id of the authenticated user from their username
             */

            $id = User::getIdFromUsername($attempt);

            /**
             * Log the user in by their ID
             */
            Auth::loginUsingId($id);

            /**
             * Check Auth worked, redirect to homepage if so
             */

            if(Auth::check())
            {
                return redirect()->intended($this->redirectPath())->with('affirmative','You have been successfully logged in!');
            }
        }

        /**
         * Otherwise, Auth failed, redirect to homepage with message
         */

        return redirect($this->loginPath())->with('failure','You have been logged out because you were not able to be authenticated!');

    }

    /**
     * Get the needed authorization credentials from the request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    protected function getCredentials(Request $request)
    {
        return $request->only($this->loginUsername(), 'password');
    }

    /**
     * Get the login username to be used by the controller.
     *
     * @return string
     */
    public function loginUsername()
    {
        return property_exists($this, 'username') ? $this->username : 'username';
    }

    /**
     * Send the response after the user was authenticated.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  bool  $throttles
     * @return \Illuminate\Http\Response
     */
    protected function handleUserWasAuthenticated(LoginRequest $request, $throttles)
    {
        if ($throttles) {
            $this->clearLoginAttempts($request);
        }

        if (method_exists($this, 'authenticated')) {
             $this->authenticated($request, Auth::user());
        }

        return redirect()->intended($this->redirectPath());
    }

    /**
     * Get the login lockout error message.
     *
     * @param  int  $seconds
     * @return string
     */
    protected function getLockoutErrorMessage($seconds)
    {
        return Lang::has('auth.throttle')
            ? Lang::get('auth.throttle', ['seconds' => $seconds])
            : 'You have attempted too many unsuccessful login attempts. Try again in '.$seconds.' seconds.';
    }

    /**
     * The number of seconds to delay further login attempts.
     *
     * @return int
     */
    protected function lockoutTime()
    {
        return property_exists($this, 'lockoutTime') ? $this->lockoutTime : 1200;
    }
}