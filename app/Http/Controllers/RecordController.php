<?php namespace App\Http\Controllers;

use App;
use App\Booking;
use App\Category;
use App\Customer;
use App\Services\Duplicate;
use App\Http\Requests\Customer\DetachCustomerRequest;
use App\Http\Requests\Customer\ExistingBookingNewCustomerRequest;
use App\Http\Requests\Customer\AddNewCustomerRequest as RecordFormRequest;
use App\Http\Requests\Booking\CustomerBookingAddRequest;
use App\Http\Requests\Booking\BookingStoreRequest as BookingStore;
use AWS;
use App\Services\CustomerLookup;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Support\Facades\Input;
use Carbon\Carbon;
use Dflydev\ApacheMimeTypes\PhpRepository;
use Gate;
use GrahamCampbell\Binput\Facades\Binput;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use Request;
/**
 * Class RecordController
 *
 * @package App\Http\Controllers
 */
class RecordController extends Controller {

    /**
     * Create a new controller instance.
     *
     * @return void
     */

    protected $id;

    protected $term;

    protected $customer;

    protected $usc;

    protected $booking;

    protected $search;

    protected $s3;

    protected $images;

    protected $files;

    protected $agent;

    protected $record;

    protected $request;

    protected $result;

    protected $check;

    protected $checker;

    protected $upload;

    protected $bucket;

    protected $query;

    protected $res;

    protected $uc1;

    protected $uc2;

    protected $detach;

    /**
     * @var
     */
    protected $data;

    protected $add;
    protected $image;
    /**
     * Show the application welcome screen to the user.
     *
     * @return Response
     */
    protected $case;
    protected $count;

    /**
     * This makes it only available to logged in users
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * @return \Illuminate\View\View
     */
    public function addCustomer()
    {
        /**
         * This function is used to load up the page to add a new customer to the database.
         * The aso active feature is there so that in the future when an associate
         * stops working that their bookings will not be erased and cannot be added
         * when the number switches to 2.
         */

        return view('customer.add');
    }

    /**
     * @param $id
     *
     * @return $this
     */
    public function viewCustomer($id)
    {
        try {

            /**
             * Returns all of the bookings for the selected customer if the customer exists in the database.
             */

            switch(auth()->user()->id)
            {
                case 99:
                    $query = new CustomerLookup();
                    $customer = $query->testLookup($id);
                    break;
                default:
                    $query = new CustomerLookup();
                    $customer = $query->defaultLookup($id);
                    break;
            }

            if(count($customer) === 0)
            {
                return abort(404);
            }

        }
        catch (\Exception $e) {

            /**
             * Throws an exception and a 404 of the search fails.
             */

            Session::flash('404','The customer selected was not found in the database');
            return abort(404);
        }

        return view('customer.customer',['customer' => $customer]);
    }

    /**
     * @param $id
     * @return mixed
     */
    public function showCustomerTable($id)
    {
        switch(auth()->user()->id)
        {
            case 99:

                $query = new CustomerLookup();

                $customer = DB::connection('mysql3')
                    ->table('customer')
                    ->distinct()
                    ->select('booking.bok_id','booking.bok_selector', 'customer.cus_id AS cust', 'customer.cus_selector as cus',
                        customerFullNameDB(),'cus_fname', 'cus_lname', associateQuery(), 'img_name',
                        'bok_ship', 'bok_date', 'associate.aso_id', 'img_path', 'img_url', 'cus_notes', 'bok_reservation')
                    ->leftJoin('customer_booking', 'customer.cus_id', '=', 'customer_booking.cus_id')
                    ->leftJoin('booking', 'customer_booking.bok_id', '=', 'booking.bok_id')
                    ->leftJoin('associate', 'customer.aso_id', '=', 'associate.aso_id')
                    ->leftJoin('image', 'booking.bok_id', '=', 'image.bok_id')
                    ->where('customer.cus_selector', '=', $id)
                    ->where('associate.aso_id', 99)
                    ->orderBy('bok_date', 'desc')
                    ->groupBy('booking.bok_id')
                    ->get();

                break;
            default:
                $customer = DB::connection('mysql3')
                    ->table('customer')
                    ->distinct()
                    ->select('booking.bok_id', 'booking.bok_selector', 'customer.cus_id AS cust', 'customer.cus_selector as cus',
                        customerFullNameDB(),'cus_fname', 'cus_lname', associateQuery(), 'img_name',
                        'bok_ship', 'bok_date', 'associate.aso_id', 'img_path', 'img_url', 'cus_notes', 'bok_reservation')
                    ->leftJoin('customer_booking', 'customer.cus_id', '=', 'customer_booking.cus_id')
                    ->leftJoin('booking', 'customer_booking.bok_id', '=', 'booking.bok_id')
                    ->leftJoin('associate', 'customer.aso_id', '=', 'associate.aso_id')
                    ->leftJoin('image', 'booking.bok_id', '=', 'image.bok_id')
                    ->where('customer.cus_selector', '=', $id)
                    ->orderBy('bok_date', 'desc')
                    ->groupBy('booking.bok_id')
                    ->get();
        }

        return view('table.customer-booking-table', ['customer' => $customer]);
    }

    /**
     * This returns the booking for the specified customer.
     *
     * @param mixed $id
     *
     * @return mixed
     */
    public function viewBooking($id)
    {
        /**
         * If the booking exists in the database, the user will be able to see the booking.
         * If the booking does not exist, it returns the user to the dashboard with an error message.
         *
         */
        $books = Booking::with('associate','land' ,'ship','customers','groups','images.category')
            ->where('bok_selector', '=', $id)
            ->get();
        return view('booking.booking', ['books' => $books]);
    }

    /**
     * @param $id
     *
     * @return $this
     */
    public function booking($id)
    {
        /**
         * This function is used to find a booking that is associated with a customer.
         */

        $new = Customer::findOrFail($id);
            return view('booking.new',compact('new'));
    }

    /**
     * This page is where the user will add the image for the booking.
     *
     * @param  int $id = Booking Selector
     *
     * @return mixed
     */

    public function addImage($id)
    {
        $booking = Booking::where('bok_selector','=', $id)->firstOrFail();
        $record = Category::all();
        return view('booking.filedrop', ['id' => Booking::findOrFail($booking->bok_id)],compact('record'));
    }

    /**
     * @param \App\Http\Requests\RecordFormRequest $data Form Data
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(RecordFormRequest $data)
    {

        /**
         * This function is used to store a new customer into the database.
         * First it will validate the data entered, If it is successfully
         * stored in the database, it will redirect the user to enter a
         * new booking for the customer.
         */

        /** @var Customer $record */

        $uc1 = titleCase($data['firstName']);
        $uc2 = titleCase($data['lastName']);

        /**
         * Checks the database to see if there is a matching record.
         *
         * If a record matches, it will return the user to the page telling them
         * there is a record for the customer already in the database.
         */
        $checker = new Duplicate();
        $result = $checker->checkCustomer($uc1,$uc2,$data['associate']);

        if($result > 0)
        {
            return $checker->customerDuplicate($uc1, $uc2);
        }

        $record = new Customer;
        $record->aso_id         = $data['associate'];
        $record->cus_id         = Binput::get('id');
        $record->cus_selector   = encrypt();
        $record->cus_fname      = $uc1;
        $record->cus_lname      = $uc2;
        if (!empty($data['notes']))
        {
            $record->cus_notes = $data['notes'];
        }
        else
        {
            $record->cus_notes = null;
        }
        $record->created_by = username();
        $record->save();

        /**
         * Checks to see if the field ship is empty.
         */


        if (!empty($data['ship']) && empty($data['land']) || !empty($data['land']) && empty($data['ship'])) {
                $booking = new Booking;

                $booking->bok_selector =  encrypt();
                $booking->bok_reservation =  strtoupper($data['reservation']);
                $booking->aso_id = $data['associate'];
                    if (!empty($data['ship']) && empty($data['land'])){
                        $booking->bok_ship =  $data['ship'];
                        $ship = new CustomerLookup();
                        $result = $ship->shipIdentity($data['ship']);
                        $booking->ship_id = $result;
                        $booking->bok_type = "Sea";
                    }
                    elseif(!empty($data['land'])&& empty($data['ship']))
                    {
                        $booking->bok_ship =  $data['land'];
                        $land = new CustomerLookup();
                        $result = $land->landIdentity($data['land']);
                        $booking->land_id = $result;
                        $booking->bok_type = "Land";
                    }
                    else{
                        return abort(500);
                    }
            $booking->bok_date = unix_date($data['date']);
            if (!empty($data['notesB'])) {
                    $booking->bok_notes = $data['notesB'];
                }
                else {
                    $booking->bok_notes = null;
                }
                $booking->created_by = username();

                $booking->save();

            /**
             * Attaches the booking and customer information to the pivot table.
             */

            $booking->customers()->attach(1,['bok_id' => $booking->bok_id ,'cus_id' => $record->cus_id ]);


            /**
             * Redirects the user to booking page if the booking information is present.
             */

            Session::flash('affirmative','Record for '.$record->cus_fname.' '.$record->cus_lname.' and the booking information has been added successfully into the database');
            return redirect(expire(url('search/booking/'.$booking->bok_selector)));
        }
        else{
            /**
             * Redirects the user to the customer page if the booking information is not present.
             */
            //var_dump($record,$data);
            Session::flash('affirmative','Record for '.$record->cus_fname.' '.$record->cus_lname.' has been added successfully into the database');
            return redirect('search/customer/'.$record->cus_selector);
        }
    }

    /**
     * This stores the booking request for a new booking.
     *
     * @param \App\Http\Requests\BookingStoreRequest $request
     *
     * @var int     $request['id']          Booking ID
     * @var int     $request['associate']   Associate ID
     * @var mixed   $request['ship']        Ship Name
     * @var int     $request['date']        Sail Date
     * @var mixed   $request['notes']       Notes
     *
     * @return \Illuminate\Support\Facades\Redirect
     */

    public function bookingStoreRequest(BookingStore $request)
    {

        /**
         * This function is used to store the booking information.
         * First the information submitted will be validated.
         * If validation is successful, it will then be entered
         * into the database. If that is successful, it will redirect
         * the user to the customer page that the booking belongs to.
         *
         */

        $booking = new Booking;
        $booking->bok_selector = encrypt();
        $booking->aso_id = $request['associate'];
        $booking->bok_reservation = strtoupper($request['reservation']);

        if (!empty($request['ship']) && empty($request['land']))
        {
            $booking->bok_ship = $request['ship'];
            $ship = new CustomerLookup();
            $result = $ship->shipIdentity($request['ship']);
            $booking->ship_id = $result;
            $booking->bok_type = "Sea";
        }
        elseif(empty($request['ship']) && !empty($request['land']))
        {
            $booking->bok_ship = $request['land'];
            $land = new CustomerLookup();
            $result = $land->landIdentity($request['land']);
            $booking->land_id = $result;
            $booking->bok_type = "Land";
        }
        else
        {
          return abort(500);
        }

        $booking->bok_date = unix_date($request['date']);
        if (!empty($request['notes'])) {
            $booking->bok_notes = $request['notes'];
        }
        else{
            $booking->bok_notes = null;
        }
        $booking->created_by = username();

     $booking->save();


        $customer = Customer::where('cus_selector','=' ,$request['id'])->first();
            /**
             * Attaches the booking and customer information to the pivot table.
             */

            $booking->customers()->attach(1,['bok_id' => $booking->bok_id ,'cus_id' => $customer->cus_id ]);

            return redirect('search/customer/' . $customer->cus_selector)->with('affirmative', 'New Booking has been added successfully');

    }

    /**
     * @param \App\Http\Requests\Booking\CustomerBookingAddRequest $result
     *
     * @return mixed
     */
    public function addToExistingBooking(CustomerBookingAddRequest $result)
    {

        /**
         * Does the reservation number exist?
         * if so proceed, else fail.
         */

        $booking = Booking::where('bok_reservation', '=' ,$result['reservation'])->firstOrFail();

        /**
         * Checks to see if the customer already exists in the booking.
         *
         */

        $checker = new Duplicate();
        $check = $checker->checkBooking($result['id'], $booking->bok_id);

        if($check > 0)
        {
            return $checker->customerBookingDuplicate();
        }

        /**
         * Counts number of customers for a specific booking to not allow there to be more than five bookings at a time.
         */

        $count = count($booking->customers);

        switch($count)
        {
            case $count <= 4:
                /**
                 * Occurs when there are five customers or less attached to a booking
                 */
                $booking->customers()->attach(1, ['bok_id' => $booking->bok_id, 'cus_id' => $result['id']]);
                return back()->with('affirmative', 'Customer has been successfully added to Reservation #' . $result['reservation'] . '!');
                break;
            case $count > 4:
                return $checker->customerLimit();
                break;
            default:
                return abort(500);
            break;
        }
    }


    /**
     * This detaches the customer from the booking.
     *
     * @param \App\Http\Requests\Booking\DetachCustomerRequest $request
     *
     * @return mixed
     */
    public function detachCustomer(DetachCustomerRequest $request)
    {

        $detach = Booking::findOrFail($request['booking']);
        $detach->customers()->detach($request['customer']);

            return back()->with('affirmative','Customer has been detached successfully from the booking.');
    }

    /**
     * This adds the customer to an existing booking.
     *
     * @param \App\Http\Requests\Booking\ExistingBookingNewCustomerRequest $data
     *
     * @param string $uc1 = First Name Converted to Title Case
     * @param string $uc2 = Last Name Converted to Title Case
     *
     * @param int       $data['associate'] = Associate ID
     * @param string    $data['firstName'] = Customer First Name
     * @param string    $data['lastName']  = Customer Last Name
     * @param mixed     $data ['notes']    = Customer Notes
     *
     * @return mixed
     */
    public function newCustomerExistingBooking(ExistingBookingNewCustomerRequest $data)
    {
        $uc1 = titleCase($data['firstName']);
        $uc2 = titleCase($data['lastName']);

        $checker = new Duplicate();
        $check = $checker->checkCustomer($uc1, $uc2, $data['associate']);

        if ($check > 0)
        {
            return $checker->customerDuplicate($uc1, $uc2);
        }

        $record = new Customer;
        $record->aso_id         = $data['associate'];
        $record->cus_selector   = encrypt();
        $record->cus_fname      = $uc1;
        $record->cus_lname      = $uc2;
        $record->cus_notes      = $data['notes'];
        $record->created_by     = username();
        $record->save();

        $booking = Booking::where('bok_selector','=',$data['booking'])->firstOrFail();
        $booking->customers()->attach(1,['bok_id' => $booking->bok_id ,'cus_id' => $record->cus_id ]);

        return back()->with('affirmative', 'Success!');
    }

    public function getImages($id)
    {
        $book = Booking::with('associate','land' ,'ship','customers','groups','images.category')
            ->where('bok_selector', '=', $id)
            ->get();
        return view('table.image-table', ['book' => $book->images]);
    }
}