<?php
/**
 * Created by PhpStorm.
 * User: Casey Hargarther
 * Date: 2/29/2016
 * Time: 7:16 PM
 */

namespace App\Http\Controllers;

use App\Http\Requests\CreateProviderRequest;
use App\Http\Requests\DeleteProviderRequest;
use App\Http\Requests\EditProviderRequest;
use App\Land;
use GrahamCampbell\Binput\Facades\Binput;
use Illuminate\Support\Facades\Auth;
/**
 * Class LandController
 * @package App\Http\Controllers
 */
class LandController extends Controller
{
    protected $provider;

    protected $query;

    protected $land;

    protected $data;

    /**
     * LandController constructor.
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * @return mixed
     */
    public function viewProviders()
    {
        $provider = Land::where('land_active', 0)->orderBy('land_name')->get();

        return view('land.index',['provider' => $provider]);
    }

    /**
     * @return mixed
     */
    public function search()
    {
        $query = Binput::get('land');
        $data = array();

        /**
         * QUERY USED TO SEARCH FOR VACATION PROVIDERS FOR AUTOCOMPLETE.
         * LAND_ACTIVE IS A CONSTRAINT USED TO PREVENT INACTIVE PROVIDERS.
         */

        $provider = Land::where('land_name', 'LIKE', '%' . $query . '%')
            ->where('land_active', '=', 0)
            //->select('land_name')
            ->get();

        foreach ($provider as $index=>$result):
            $data[$index] = [
                'vacation_pvr'    => $result->land_name,
            ];
        endforeach;
        return response()->json($data);
    }

    /**
     * @return mixed
     */
    public function createProvider()
    {
        return view('land.create');
    }

    /**
     * @param CreateProviderRequest $request
     * @return mixed
     */
    public function postProvider(CreateProviderRequest $request)
    {
        $land = new Land();
        $land->land_name = titleCase($request->provider);
        $land->land_notes = $request->notes;
        $land->created_by = username();
        $land->save();

        return back()->with('affirmative','Provider has been successfully added to the database');
    }

    public function editGet($id)
    {
       $land =  Land::findOrFail($id);

        return view('land.edit',['land' => $land]);
    }

    /**
     * @param EditProviderRequest $request
     * @return mixed
     */
    public function editPost(EditProviderRequest $request)
    {
        try {
            /**
             * This function updates the booking for a selected customer.
             *
             */
            $land = Land::findOrFail($request->id);
            $land->land_name = titleCase($request->provider);
            $land->land_notes = $request->notes;
            $land->save();

            return redirect(expire(url('land/list')))->with('affirmative', 'Land Provider has been updated successfully');

        }
        catch(ModelNotFoundException $e)
        {
            return abort(500);
        }
    }

    /**
     * @param $id
     * @return mixed
     */
    public function deleteGet($id)
    {
        try {

            $land = Land::findOrFail($id);
        }
        catch(ModelNotFoundException $e)
        {
            return abort(500);
        }

        return view('land.delete', ['land' => $land]);
    }

    /**
     * @param DeleteProviderRequest $request
     * @return mixed
     */
    public function deletePost(DeleteProviderRequest $request)
    {

        $land = Land::findOrFail($request->id);
        $land->land_active = 1;
        $land->save();

        return redirect(expire(url('land/list')))->with('affirmative', 'Land Provider has been successfully deleted');
    }
}