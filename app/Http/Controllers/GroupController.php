<?php
/**
 * Created by PhpStorm.
 * User: hargcb
 * Date: 6/24/2015
 * Time: 7:19 PM
 */

namespace App\Http\Controllers;

use App;
use App\Associate;
use App\Booking;
use App\Group;
use App\Image;
use App\Customer;
use App\Ship;
use App\Http\Requests\Groups\NewGroupRequest as NewGroup;
use App\Http\Requests\Groups\UpdateGroupRequest as Update;
use App\Http\Requests\Groups\NewCustomerGroupRequest;
use App\Http\Requests\Search\GroupLookupRequest;
use App\Http\Requests\Groups\AttachCustomerRequest as Attach;
use GrahamCampbell\Binput\Facades\Binput;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;
use DB;
use App\Services\Duplicate;
use Illuminate\Support\Facades\Response;
use Carbon\Carbon;


/**
 * Class GroupController
 *
 * @package App\Http\Controllers
 */

class GroupController extends Controller{


    /**
     * GroupController constructor.
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * @var
     */
    protected $associate;

    /**
     * @var
     */
    protected $request;

    protected $customer;


    /**
     * @return \Illuminate\View\View
     */
    public function addGroup()
    {
        /**
         * This function is used to send the user to the page to enter the group information
         */

        $associate = Associate::where('aso_active','=', 1)
            ->get();
        return view('groups.newgroup',['associate' => $associate ]);

    }

    protected $new;
    /**
     * @param \App\Http\Requests\Groups\NewGroupRequest $request
     *
     * @return \Illuminate\Http\RedirectResponse
     */
    public function groupAdd(NewGroup $request)
    {
        /**
         * This function is used to store a new group into the database
         */

        $new = new Group();
        $new->aso_id         = $request->associate;
        $new->group_name     = $request->groupName;
        $new->group_selector = encrypt();
        $new->group_number   = $request->groupNum;
        $new->group_ship     = $request->ship;

        $identity = new App\Services\CustomerLookup();
        $ship = $identity->shipIdentity($request->ship);
        $new->ship_id = $ship;

        $new->group_date     = unix_date($request->date);
        $new->group_notes    = $request->notes;
        $new->created_by     = username();
        $new->save();

        return redirect('groups/active')->with('affirmative','New Group has been added successfully into the database');
    }

    protected $result;

    /**
     * Group List View
     * This function is used to sort the multiple group list
     * and allow changes without entering it into the controller 4 times.
     *
     *
     * @param $result
     * @return mixed
     */
    public function getView($group)
    {
        $result = $group->sortable(['group_date' => 'desc'])->paginate(5);
        return view('groups.search',['result' => $result]);
    }

    /**
     * @return \Illuminate\View\View
     */
    public function showAllFutureSailings()
    {
        /**
         * This function is fired when a user wants to search for future sailing groups.
         */

        $record = Group::has('ship')
            ->whereHas('associate', function ($query) {
                $query->where('associate.aso_active',1);
                $query->where('group_active',1);
            });


        Session::flash('group_status','Active');

        $result = $record->sortable(['group_date' => 'asc'])->paginate(5);
        return view('groups.search',['result' => $result]);

    }

    /**
     * @return mixed
     */
    public function showAllPastSailings()
    {
        /**
         * This function is fired when a user wants to search for all groups.
         */

        $result = Group::has('ship')
            ->whereHas('associate', function ($query) {
            $query->where('associate.aso_active', '=', 1);
            $query->where('associate.aso_id', '<>', 99);
            $query->where('group.group_active','=','S');});

        Session::flash('group_status','Sailed');
        return $this->getView($result);
    }    /**
     * @return mixed
     */

    public function showAllCancelledSailings()
    {
        /**
         * This function is fired when a user wants to search for all groups.
         */

        $result = Group::has('ship')
            ->whereHas('associate', function ($query) {
                $query->where('associate.aso_active', '=', 1);
                $query->where('associate.aso_id', '<>', 99);
                $query->where('group.group_active','=','C');});
/*            ->orderBy('group.group_date','desc')
            ->paginate(5);*/

        Session::flash('group_status','Cancelled');
        return $this->getView($result);
    }

    protected $id;

    /**
     * @return \Illuminate\View\View
     */
    public function associateSearch()
    {
        /**
         * This function is fired when a user searches for groups by associate.
         */

        $id = Binput::get('a');
       $associate = Associate::where('aso_selector','=',$id)->first();

        if(count($associate) === 0)
        {
            return abort(404);
        }

        $group = Group::has('associate')
            ->where('aso_id','=',$associate->aso_id)
            ->where('group_active', '=', 1);

        Session::flash('group_status','Associate');
        $result = $group->sortable(['group_date' => 'asc'])->paginate(5);
        return view('groups.search',['result' => $result]);
    }

    protected $agent;

    protected $resultQuery;

    /**
     * @param $id
     *
     * @return \Illuminate\View\View
     */

    public function showGroup($id)
    {
        /**
         * Group and Image Page
         *
         * This page shows the user the page with all the customer's bookings  and images associated with the group.
         *
         * @param $id = the ID used for the group number located in the address bar
         * @mixed $resultQuery = This is the MySQL function that returns the query for the specific group.
         *
         */

        /**
         * Valid Query - Do Not Modify.
         */

       /* $resultQuery = Group::with('associate','images.category','bookings.customer')
            ->where('group_selector','=',$id)
            ->get();*/

        $resultQuery = Group::with('ship','associate','images.category','bookings.customer')
            ->where('group_selector','=',$id)
            ->get();

        return view('groups.info',['resultQuery' => $resultQuery]);
    }

    protected $group;

    /**
     * @param \App\Http\Requests\Groups\UpdateGroupRequest $request
     *
     * @return \Illuminate\Http\RedirectResponse
     */
    public function updateGroup(Update $request)
    {
        /**
         * This function updates a group in the database
         */

        $group = Group::find($request->group);
        $group->group_ship      = $request->ship;
        $identity               = new App\Services\CustomerLookup();
        $group->ship_id         = $identity->shipIdentity($request->ship);
        $group->aso_id          = $request->associate;
        $group->group_number    = $request->number;
        $group->group_name      = $request->name;
        $group->group_notes     = $request->notes;
        $group->group_date      = unix_date($request->date);
        $group->save();

            return back()->with('affirmative','Group information has been successfully updated!');
    }

    protected $delete;

    /**
     * @return \Illuminate\Http\RedirectResponse
     */
    public function deleteGroup()
    {
        /**
         * This function deletes a group from the database
         */


        try{
            $id = Binput::get('delete');
            $delete = Group::findOrFail($id);
            $delete->group_active = 'C';
            $delete->save();
        }
        catch(\Exception $e)
        {
            return abort(500);
        }

        return redirect('home')->with('affirmative', 'Group has been deleted successfully!');
    }

    protected $person;

    protected $attach;

    /**
     * @return \Illuminate\Http\RedirectResponse
     */
    public function newGroupBooking(Attach $request)
    {
        try
        {
            /**
             * Checks to see if the group exists
             */
            $group = Group::where('group_number','=',Binput::get('group'))
                ->where('group_active', 1)
                ->first();

            /**
             *  This function adds a customer to a group booking without filling out the name of ship, agent or even a sail date.
             */

            $person = Customer::find(Binput::get('id'))->first();

            $record = new Booking;
            $record->bok_reservation = Binput::get('reservation');
            $record->bok_selector    = encrypt();
            $record->aso_id          = $person->aso_id;
            $record->bok_ship        = $group->group_ship;
            $record->ship_id         = $group->ship_id;
            $record->bok_date        = $group->group_date;
            $record->group_id        = $group->group_id;
            $record->created_by      = username();
            $record->save();

            /**
             * Attaches the customer to the booking
             */

            $attach = Booking::find($record->bok_id)->first();

            $attach->customers()->attach(1,['bok_id' => $record->bok_id ,'cus_id' => Binput::get('id') ]);


            return back()->with('affirmative','Customer has been successfully added to Group #'.Binput::get('group').'.');

        }
        catch (\Exception $e)
        {
            /**
             * Returns the user back to the customer page if the group number is not valid.
             */

            return back()->withInput();
        }
    }

    protected $res;

    protected $data;

    protected $index;
    /**
     * @return mixed
     */
    public function groupSearch()
    {

        $query = Binput::get('group');
        $data = array();

        /**
         * QUERY USED TO SEARCH FOR GROUPS FOR AUTOCOMPLETE.
         * GROUP_ACTIVE IS A CONSTRAINT USED TO PREVENT
         * GROUPS THAT HAVE ALREADY SAILED FROM BEING RETURNED
         * AS A SUGGESTION.
         */
        $user_id = Auth::user()->id;

        switch ($user_id)
        {
            case 1:
                $res = Group::where('group_number', 'LIKE', '%' . $query . '%')
                    ->where('group_active', 1)
                    ->orWhere('group_name', 'LIKE', '%' . $query . '%')
                    ->where('group_active', 1)
                    ->orWhere('group_ship', 'LIKE', '%' . $query . '%')
                    ->where('group_active', 1)
                    //->select('group_number','group_ship','group_name','group_date')
                    ->get();
                 break;
            case 99:
                $res = Group::where('group_number', 'LIKE', '%' . $query . '%')
                    ->where('group_active', 1)
                    ->where('aso_id', 99)
                    ->orWhere('group_name', 'LIKE', '%' . $query . '%')
                    ->where('group_active', 1)
                    ->where('aso_id', 99)
                    ->orWhere('group_ship', 'LIKE', '%' . $query . '%')
                    ->where('group_active', 1)
                    ->where('aso_id', 99)
                    //->select('group_number','group_ship','group_name','group_date')
                    ->get();
                break;
            default:
                $res = Group::where('group_number', 'LIKE', '%' . $query . '%')
                    ->where('group_active', 1)
                    ->where('aso_id','<>', 99)
                    ->orWhere('group_name', 'LIKE', '%' . $query . '%')
                    ->where('group_active', 1)
                    ->where('aso_id','<>', 99)
                    ->orWhere('group_ship', 'LIKE', '%' . $query . '%')
                    ->where('group_active', 1)
                    ->where('aso_id','<>', 99)
                    //->select('group_number','group_ship','group_name','group_date')
                    ->get();
                break;
        }

        foreach ($res as $index=>$result):
            $data[$index] = [
                'group_number'  => $result->group_number,
                'group_ship'    => $result->group_ship,
                'group_name'    => $result->group_name,
                'group_date'    => human_date($result->group_date)
            ];
        endforeach;

        return response()->json($data);
}

    /**
     * @return mixed
     */
    public function newCustomer()
{
    return view('groups.add');
}

    /**
     * @param \app\Http\Requests\Groups\NewCustomerGroupRequest $request
     */
    public function addCustomer(NewCustomerGroupRequest $request)
    {
        $group = Group::has('ship')
        ->where('group_number', '=', $request->group)
        ->where('group_active', 1)
        ->firstOrFail();

        $firstName = titleCase($request->firstName);
        $lastName = titleCase($request->lastName);

        $checker = new Duplicate();
        $check = $checker->checkCustomer($firstName,$lastName,$group->aso_id);

        if($check > 0)
        {
            return $checker->customerDuplicate1($firstName, $lastName);
        }

        try {
            $customer = new Customer();
            $customer->aso_id = $group->aso_id;
            $customer->cus_selector = encrypt();
            $customer->cus_fname = $firstName;
            $customer->cus_lname = $lastName;

            if (!empty($request->notes)) {
                $customer->cus_notes = $request->notes;
            }
            else {
                $customer->cus_notes = null;
            }

            $customer->created_by = Auth::user()->username;

            if ($customer->save() == true) {
                $booking = new Booking();
                $booking->bok_reservation = $request->reservation;
                $booking->bok_selector = encrypt();
                $booking->aso_id = $group->aso_id;
                $booking->group_id = $group->group_id;
                $booking->ship_id = $group->ship_id;
                $booking->bok_ship = $group->group_ship;
                $booking->bok_date = $group->group_date;

                if (!empty($request->notesB)) {
                    $booking->bok_notes = $request->notesB;
                } else {
                    $booking->bok_notes = null;
                }
                $booking->created_by = username();
                $booking->save();

                /**
                 * Attach Customer to Booking.
                 */

                $attach = Booking::find($booking->bok_id);
                $attach->customers()->attach(1, ['bok_id' => $booking->bok_id, 'cus_id' => $customer->cus_id]);
                Session::flash('affirmative', 'Record for ' . $customer->cus_fname . ' ' . $customer->cus_lname . ' and the booking information has been added successfully into the database');

                return redirect(expire(url('search/booking/' . $booking->bok_selector)));
            }
        }
        catch (\Exception $e)
        {
            /**
             * Returns the user back to the customer page if the group number is not valid.
             */

            Session::flash('exception', Binput::get('group').' is either not a valid group number, the Sail Date of the group was more than 10 days ago, or has been set to cancelled.');

            //  return redirect('search/customer/'.$person->cus_id);
            return back()->withInput();
        }

        return abort(500);
    }
}