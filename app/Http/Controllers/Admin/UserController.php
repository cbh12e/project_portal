<?php namespace App\Http\Controllers\Admin;
use App\Http\Controllers\AdminController;
use App\User;
use App\Http\Requests\Admin\UserRequest;
use App\Http\Requests\Admin\UserEditRequest;
use App\Http\Requests\Admin\DeleteRequest;
use Illuminate\Support\Facades\Auth;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Brotzka\DotenvEditor\DotenvEditor;
use Brotzka\DotenvEditor\Exceptions\DotEnvException;

/**
 * Class UserController
 *
 * @package App\Http\Controllers\Admin
 */
class UserController extends AdminController  {

    /*
    * Display a listing of the resource.
    *
    * @return Response
    */

    /**
     * @var string
     */
    protected $connection = 'mysql2';

    /**
     * @return \Illuminate\View\View
     */
    public function index()
    {

    $users = User::where('active','=','Active')->get();
        // Show the page
        return view('admin.users.index',compact('users'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function getCreate() {
        return view('admin.users.create_edit');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \App\Http\Requests\Admin\UserRequest $request
     *
     * @return \App\Http\Controllers\Admin\Response
     */
    public function postCreate(UserRequest $request) {

        $user = new User();
        $user->setConnection('mysql2');
        $user -> name = $request->name;
		$user -> username = $request->username;
        $user -> email = $request->email;
        $user -> password = bcrypt($request->password);
        $user -> confirmation_code = str_random(32);
        $user -> confirmed = $request->confirmed;
        $user -> save();

        return back()->with('flash_message','The User: '. $user->username.' for '. $user->name. ' has been successfully created!');

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param $user
     * @return Response
     */
    public function getEdit($id) {

        $user = User::findOrFail($id);
        return view('admin.users.create_edit', compact('user'));
    }

    /**
     * @param $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\View\View
     */
    public function getProfile($id) {

        try {
           $user =  User::findOrFail($id);
        }
        catch (ModelNotFoundException $e) {
            return redirect('home')->with('not_found','The User of the number '.$id.' does not exist');
        }

       // $user = User::find($id);
        return view('admin.users.profile', compact('user'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param $user
     * @return Response
     */
    public function postEdit(UserEditRequest $request, $id) {

        $user = User::findOrFail($id);
        $user -> name = $request->name;
        $user -> confirmed = $request->confirmed;
        $password = $request->password;
        $passwordConfirmation = $request->password_confirmation;

        if (!empty($password)) {
            if ($password === $passwordConfirmation) {
                $user -> password = bcrypt($password);
            }
        }
        $user -> save();
    }
    /**
     * Remove the specified resource from storage.
     *
     * @param $user
     * @return Response
     */

    public function getDelete($id)
    {

        $user = User::findOrFail($id);
        // Show the page
        return view('admin.users.delete', compact('user'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param $user
     * @return Response
     */
    public function postDelete(DeleteRequest $request)
    {

        $user = User::findOrFail($request['id']);
        $user->setConnection('mysql2');

        if ($user->admin !== 'R') {
            // $user = User::find();
            // Checks to see if the user is attempting to deleting the root account
            if ($request['id'] == Auth::User()->id) {
                return back()->with('error', 'You cannot delete your own user account.');
            } // Deletes the user account
            else {
                $user->active = 'Inactive';
                $user->delete();
                return back()->with('flash_message', 'The selected user has been successfully deleted.');
            }
    }
            else{
                return back()->with('error', 'You cannot delete the root user account.');
            }
        }
    public function getConfiguration()
    {
        $backups = new DotenvEditor();
        //$backups->getBackupPath();
        return view('vendor.dotenv-editor.overview',compact('backups'));
    }
}
