<?php namespace App\Http\Controllers\Admin;

use App\Http\Controllers\AdminController;
use App\User;
use App\Booking;
use App\Image;
use App\Customer;
use App\Group;

class DashboardController extends AdminController {

    public function __construct()
    {
        parent::__construct();
    }

	public function index()
	{
        $title = "Dashboard";
        $users = User::all()->count();
        $bookings = Booking::all()->count();
        $images = Image::all()->count();
        $records = Customer::all()->count();
        $groups = Group::all()->count();

        // Memory Usage
        $mem_usage = memory_get_usage(true);

        if ($mem_usage < 1024) {
            $mem_use =   $mem_usage." B";
        }
        elseif ($mem_usage < 1048576) {
            $mem_use = round($mem_usage/1024,2)." KB";
        }
        else{
            $mem_use =  round($mem_usage/1048576,2)." MB";
        }
        return view('admin.dashboard.index', compact('bookings','images','records','mem_use','users','title','groups'));

	}
}