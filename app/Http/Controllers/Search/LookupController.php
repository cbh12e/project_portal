<?php
/**
 * Created by PhpStorm.
 * User: hargcb
 * Date: 12/19/2015
 * Time: 1:49 PM
 */
namespace App\Http\Controllers\Search;
use App;
use App\Associate;
use App\Customer;
use App\Booking;
use App\Image;
use DB;
use App\Http\Controllers\Controller;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Support\Facades\Auth;
use GrahamCampbell\Binput\Facades\Binput;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Request;
use Illuminate\Support\Facades\Storage;
use App\Http\Requests\Search\CustomerSearchRequest;
use App\Http\Requests\Search\CustomerQueryRequest;
use App\Http\Requests\Search\ReservationLookupRequest;
use App\Http\Requests\Search\ReservationQueryRequest;
use App\Http\Requests\Search\ShipLookupRequest;
use App\Services\CustomerLookup;

/**
 * Class LookupController
 *
 * @package app\Http\Controllers\Search
 */
class LookupController extends Controller
{
    protected $agent;

    protected $books;

    protected $customer;

    protected $data;

    protected $search;

    protected $sql;

    protected $term;

    protected $user_id;

    protected $query;

    protected $image;

    protected $res;

    /**
     * This makes it only available to logged in users
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * @param \App\Http\Requests\Search\CustomerSearchRequest $request
     *
     * @return mixed
     */
    public function searchCustomer(CustomerSearchRequest $request)
    {

        /**
         * This function is used when a user searches for a customer in the search bar.
         * As of right now, only last names are accepted as a search method.
         *
         */

        $term = Binput::get('q');
        /** @var param $count */

        $search = Customer::has('associate')->where('cus_lname', 'like', '%'.$term.'%')
            ->orWhere('cus_fname', 'like', '%'.$term.'%')
            ->get();

        if (count($search) > 0) {
            return view('customer.search')->with('search', $search);
        }
        else {
            Session::flash('404','The customer selected was not found in the database');
            return abort(404);
        }
    }


    /**
     * @param \App\Http\Requests\Search\CustomerQueryRequest $customerQueryRequest
     *
     * @return mixed
     */
    public function customerQuery(CustomerQueryRequest $customerQueryRequest)
    {

        $query = Binput::get('term');
        switch($query)
        {
            case $sql = Customer::where('cus_lname', 'LIKE', '%' . $query . '%')
                ->orWhere('cus_fname', 'LIKE', '%' . $query . '%')
                //->select('cus_id','cus_selector','cus_fname','cus_lname')
                ->get():
                $data = array();
                foreach ($sql as $index=>$result):
                    $data[$index] = [
                        'identifier'  => $result->cus_selector,
                        'customer'    => $result->CustomerFullName
                    ];
                endforeach;
            break;
            default:
                $sql = Customer::where('cus_lname', 'LIKE', '%' . $query . '%')
                    ->orWhere('cus_fname', 'LIKE', '%' . $query . '%')
                    //->select('cus_selector','cus_id','cus_fname','cus_lname')
                    ->get();
                $data = array();
                foreach ($sql as $index=>$result):
                    $data[$index] = [
                        'identifier'  => $result->cus_selector,
                        'customer'    => $result->CustomerFullName
                    ];
                endforeach;
                break;

        }
        return response()->json($data);
    }


    /**
     * @param CustomerSearchRequest $request
     * @return mixed
     */
    public function customerLookup(CustomerSearchRequest $request)
    {
        $term = Binput::get('q');
        try{

            /**
             * Returns all of the bookings for the selected customer if the customer exists in the database.
             */

            $query = new CustomerLookup();

            $customer = $query->defaultLookup($term);

            if(count($customer) === 0)
            {
                /**
                 * If count is zero, then return 404
                 */
                return abort(404);
            }
        }
        catch (\Exception $e) {

            /**
             * Throws an exception and a 404 of the search fails.
             */

            return abort(404);
        }
        return view('customer.customer', ['customer' => $customer]);
    }

    /**
     * @return $this
     */
    public function agent()
    {
        $associate = Associate::where('aso_selector','=',Binput::get('a'))->first();

        if(count($associate) === 0)
        {
            /**
             * If query returns zero, then return 404.
             */

            return abort(404);
        }


        /**
         * This function returns all customers from a selected agent that are entered into the database.
         */

             $result = Customer::has('associate')
                    ->where('aso_id', '=', $associate->aso_id);
                   // ->orderBy('cus_lname')

                $agent = $result->sortable(['cus_lname'])->paginate(10);


        $count = Customer::has('associate')
            ->where('aso_id', '=', $associate->aso_id)
            ->count();

        return view('customer.agent',compact('agent','count'));
    }

    /**
     * @return mixed
     */
    public function all(Customer $customer)
    {
        switch(auth()->user()->id)
        {
            case 1:
                /**
                 * Root User
                 */
                $customer  = Customer::has('associate');
                break;
            case 99:
                /**
                 * Test User
                 */
                // Search Records for Agent Jane Doe
                $customer  = Customer::whereHas('associate', function ($query) {
                    $query->where('associate.aso_id', 99);});
                break;
            default:
                /**
                 * Default Setting
                 */
                $customer  = Customer::whereHas('associate', function ($query) {
                    $query->where('associate.aso_id', '<>', 99);});
                break;
        }

        Session::flash('search','all');
        $search = $customer->sortable(['cus_lname'])->paginate(10);
        return view('customer.search',['search' => $search]);
    }

    /**
     * @return mixed
     */
    public function reservationQuery(ReservationQueryRequest $term)
    {

        $searchQuery = Binput::get('term');
        //$user_id = Auth::user()->id;

       $sql = Booking::where('bok_reservation', 'LIKE', '%' .$searchQuery . '%')->get();
        $data = [];
            foreach ($sql as $index => $result):
                if(count($result->land_id)> 0 && count($result->ship_id) === 0)
                {
                    $data[$index] = [
                        'bok_id' => $result->bok_selector,
                        'reservation' => $result->bok_reservation,
                        'date1' => select_date($result->bok_date),
                        'name1' => $result->land->land_name
                    ];
                }
                else
                {
                    $data[$index] = [
                        'bok_id' => $result->bok_selector,
                        'reservation' => $result->bok_reservation,
                        'date1' => select_date($result->bok_date),
                        'name1' => $result->ship->ship_name
                    ];
                }
            endforeach;

        return response()->json($data);
    }

    /**
     * @return mixed
     */
    public function reservationSearch()
    {

        $query = Binput::get('reservation');
        $data = array();

        /*
         * QUERY USED TO SEARCH FOR GROUPS FOR AUTOCOMPLETE.
         * GROUP_ACTIVE IS A CONSTRAINT USED TO PREVENT
         * GROUPS THAT HAVE ALREADY SAILED FROM BEING RETURNED
         * AS A SUGGESTION.
         */

        switch (auth()->user()->id)
        {
            case 1:
                $res = Booking::has('customers')
                    ->where('bok_reservation', 'LIKE', '%' . $query . '%')
                    ->orWhere('bok_id', 'LIKE', '%' . $query . '%')
                    ->get();
                break;
            case 99:
                $res = Booking::has('customers')
                    ->where('bok_reservation', 'LIKE', '%' . $query . '%')
                    ->where('aso_id', 99)
                    ->orWhere('bok_id', 'LIKE', '%' . $query . '%')
                    ->where('aso_id', 99)
                    ->get();
                break;
            default:
                $res = Booking::has('customers')
                    ->where('bok_reservation', 'LIKE', '%' . $query . '%')
                    ->where('aso_id','<>', 99)
                    ->get();
                break;
        }
        foreach ($res as $index=>$result):

            if(count($result->land_id) > 0)
            {
                $data[$index] = [
                    'count'  => count($result->customers),
                    'reset'  => $result->bok_reservation,
                    'name'   => $result->land->land_name,
                    'date'   => human_date($result->bok_date)
                ];
            }
            else
            {
                $data[$index] = [
                    'count'  => count($result->customers),
                    'reset'  => $result->bok_reservation,
                    'name'   => $result->ship->ship_name,
                    'date'   => human_date($result->bok_date)
                ];
            }

        endforeach;

        return response()->json($data);
    }

    /**
     * @param \App\Http\Requests\Search\ReservationLookupRequest $r
     *
     * @return mixed
     */
    public function reservationLookup(ReservationLookupRequest $r)
    {
        $term = Binput::get('r');

        $books = Booking::with('associate','customers','groups','images.category')
            ->where('booking.bok_selector', '=', $term)
            ->get();

        if (count($books) === 0)
        {
            return abort(404);
        }

        return view('booking.booking', ['books' => $books]);

    }

    /**
     * @param $id
     * @return mixed
     */
    public function listImage($id)
    {
        $image = DB::connection('mysql3')->table('image')->select('img_id', 'bok_ship', 'bok_reservation',
            'booking.bok_id', 'customer.cus_id', 'image.created_at', 'img_path','group.group_id',
            'img_size','img_mime', 'img_ext', 'cat_name', 'img_name', 'img_notes', customerFullNameDB(),
            'cus_fname', 'cus_lname', 'bok_date','group_number','group_name')
            ->join('booking', 'booking.bok_id', '=', 'image.bok_id')
            ->join('customer_booking', 'customer_booking.bok_id', '=', 'booking.bok_id')
            ->join('customer', 'customer.cus_id', '=', 'customer_booking.cus_id')
            ->leftJoin('group', 'group.group_id', '=', 'booking.group_id')
            ->join('category', 'category.cat_id', '=', 'image.cat_id')
            ->where('booking.bok_selector', '=', $id)
            ->orderBy('image.cat_id')
            ->paginate(5);

        return view('table.image-table',['image' => $image])->render();
    }
}