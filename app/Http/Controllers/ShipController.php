<?php
/**
 * Created by PhpStorm.
 * User: hargcb
 * Date: 9/10/2015
 * Time: 2:57 PM
 */

namespace App\Http\Controllers;
use App\CruiseLine;
use App\Ship;
use App\Land;
use DB;
use App\Http\Requests\CreateNewShipRequest;
use App\Http\Requests\CreateProviderRequest;
use GrahamCampbell\Binput\Facades\Binput;

/**
 * Class ShipController
 *
 * @package App\Http\Controllers
 */
class ShipController extends Controller
{

    protected $cruiseLine;

    protected $ship;

    protected $provider;

    protected $land;


    /**
     * ShipController constructor.
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * @return mixed
     */
    public function cruiseLineView()
    {
        $cruiseLine = CruiseLine::orderBy('cl_name')->paginate(10);

        return $this->cl_view($cruiseLine);


    }

    public function cruiseLineAsc()
    {
        $cruiseLine = CruiseLine::orderBy('cl_name','asc')->paginate(10);

        return $this->cl_view($cruiseLine);


    }


    public function cl_view($cruiseLine)
    {
        return view('ship.cl_view', compact('cruiseLine'));

    }

    /**
     * @param $id
     * @return mixed
     */
    public function viewShips($id)
    {
        $ship = Ship::has('cruiseLine')
            ->where('cl_id', $id)
            ->orderBy('ship_name')
            ->paginate(10);

       return view('ship.cruiseline', compact('ship'));
    }

    public function setNewShip(CreateNewShipRequest $request)
    {
        $ship = new Ship();

        $ship->cl_id = $request['a'];
        $ship->ship_name = $request['shipName'];
        $ship->ship_year = $request['year'];
        $ship->ship_tonnage = $request['tonnage'];
        $ship->ship_notes = $request['notes'];

        if ($ship->save() == true)
        {
            return redirect('ship/line/view/'.$ship->cl_id)->with('affirmative','New Ship information has been successfully added.');
        }

        return back()
            ->withInput()
            ->with('failure','Ship Information was not entered. ');
    }

    /**
     * @return mixed
     */
    public function getNewShip()
    {

        $cruiseLine = CruiseLine::all();
        return view('ship.ship-add', ['cruiseLine' => $cruiseLine]);
    }


    /**
     *
     */
    public function inactiveShip()
    {
        $ship = Ship::find(Binput::get('confirm'));
        $ship->ship_active = 'Y';
        $ship->save();
    }


    /**
     * @return mixed
     */
    public function shipSearch()
    {
        $query = Binput::get('ship');
        $data = array();

        /**
         * QUERY USED TO SEARCH FOR CRUISE SHIPS FOR AUTOCOMPLETE.
         * SHIP_ACTIVE IS A CONSTRAINT USED TO PREVENT SHIPS THAT
         * HAS LEFT THE FLEET OR SHIPS LIKE THE COSTA ATLANTICA THAT
         * IS MARKETED ONLY TO CHINA. IT IS THERE SINCE IT COULD RETURN
         * TO EUROPE AND BE MARKETED WORLDWIDE AGAIN AT ANYTIME.
         */

        $res = Ship::has('cruiseLine')
            ->where('ship_name', 'LIKE', '%' . $query . '%')
            ->where('ship_active', '=', 'Y')
            ->orderBy('ship_name','asc')
            //->select('ship_name','cl_id')
            ->get();

        foreach ($res as $index=>$result):
            $data[$index] = [
                'logo'  => asset('/logos/'.$result->cruiseLine->cl_logo),
                'ship'    => $result->ship_name,
            ];
        endforeach;
        return response()->json($data);
    }

    /**
     * @return mixed
     */

}