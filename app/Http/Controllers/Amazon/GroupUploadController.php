<?php
/**
 * Created by PhpStorm.
 * User: hargcb
 * Date: 6/24/2015
 * Time: 7:19 PM
 */

namespace App\Http\Controllers\Amazon;
use App\Http\Controllers\Controller;
use App;
use App\Http\Requests\S3StoreRequest;
use App\Group;
use App\Image;
use App\Category;
use App\Services\AmazonS3;
use GrahamCampbell\Binput\Facades\Binput;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;
use DB;
use Illuminate\Support\Facades\Response;
use Carbon\Carbon;
use Dflydev\ApacheMimeTypes\PhpRepository;
/**
 * Class GroupController
 *
 * @package app\Http\Controllers
 */

class GroupUploadController extends Controller{

    /**
     * @return \Illuminate\View\View
     */

    /**
     *
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * @param \App\Http\Requests\S3StoreRequest $groupRequest
     */
    public function groupUpload(S3StoreRequest $groupRequest)
{
    $files = Binput::file('file');

    $upload = Group::has('associate')
        ->where('group_id', '=', Binput::get('id'))
        ->firstOrFail();

        // start count how many uploaded
        $count = count($files);

    foreach ($files as $file) {
        /**
         * This renames the file before uploading to Amazon S3
         */

        $count++;

        $extension = $file->getClientOriginalExtension();

       // $category = Category::findOrFail($request['category']);
        $fileName =  uniqid('group_'). '.' . $extension;
        $filePath = 'Groups/' . studly_case($upload->associate->AssociateName) . '/' . studly_case($upload->group_name)   .'/' . $fileName; // renaming image

        /**
         * Uploading file to Amazon S3 Storage Service
         *
         * @var  mixed $s3     Calls the Connection to Amazon S3 using supplied credentials
         * @var  array $result Uploads the given file into Amazon S3 Storage Service.
         *
         */
        $s3 = new AmazonS3();
        $result = $s3->s3Upload($file, $filePath, $extension);

        $image = new Image;
        $image->group_id        = Binput::get('id');
        $image->cat_id          = Binput::get('category');
        $image->img_name        = $fileName;
        $image->img_path        = $filePath;
        $image->img_ext         = $file->getClientOriginalExtension();
        $image->img_mime        = $file->getMimeType();
        $image->img_enc         = $result['ServerSideEncryption'];
        $image->img_size        = $file->getSize();
        $image->img_url         = $result['ObjectURL'];
        $image->img_notes       = Binput::get('notes');
        $image->uploaded_by     = username();
        $image->save();
        Session::flash('affirmative', 'New Image has successfully been added for Group # ' . $upload->group_id);
    }
}

    /**
     * @param $id
     *
     * @return mixed
     */
    public function newImagePage($id)
    {
        $record = Category::all();

        $groups = Group::where('group_selector','=',$id)->firstOrFail();

        return view('groups.group-file', ['id' => Group::findOrFail($groups->group_id) ],compact('record'));
    }
}
