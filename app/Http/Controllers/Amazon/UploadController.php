<?php
/**
 * Created by PhpStorm.
 * User: hargcb
 * Date: 12/18/2015
 * Time: 8:28 PM
 */

namespace App\Http\Controllers\Amazon;

use App\Http\Controllers\Controller;
use App;
use AWS;
use DB;
use App\Image;
use App\Category;
use App\Group;
use App\Http\Requests\S3StoreRequest as S3Upload;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Request;
use Illuminate\Support\Facades\Storage;
use Dflydev\ApacheMimeTypes\PhpRepository;
use GrahamCampbell\Binput\Facades\Binput;
use App\Services\AmazonS3;

/**
 * Class UploadController
 *
 * @package app\Http\Controllers\Amazon
 */
class UploadController extends Controller
{

    protected $files;
    protected $count;
    protected $upload;
    protected $image;
    protected $bucket;
    protected $groupInfo;
    protected $result;
    protected $s3;

    /**
     * This makes it only available to logged in users
     */
    public function __construct()
    {
        // Allows only authorized users to access this controller
        $this->middleware('auth');
    }

    /**
     * @param \App\Http\Requests\S3StoreRequest $request
     *
     * @return \Illuminate\Support\Facades\Redirect
     */
    public function s3ImageRequest(S3Upload $request)
    {

        /**
         * This function is used to upload files
         * to Amazon S3 Storage Service. If successful,
         * it will add the links to access the file in the
         * database to associate it with the booking.
         * If the upload fails, it will then return an 500 error.
         */

        /**
         * Getting file information to prepare for upload to Amazon S3
         *
         * @var mixed   $files       File Name of the new file
         * @var mixed   $extension   File Extension for new file
         * @var string  $fileName    File Name for new file
         * @var string  $filePath    File Path of new file
         * @var int     $count       Counts the number of files uploaded
         * @var mixed   $path        Gets the temporary file path of file
         *
         */

        if ($files = Binput::hasFile('file') == true)
        {
            /**
             * Getting information to insert into Filename and Path boxes for Amazon S3
             */
            $files = Binput::file('file');
            $upload = DB::connection('mysql3')
                ->table('booking')
                ->select('customer.cus_id', 'customer.aso_id', associateQuery(),'aso_lname', 'cus_fname','land_name','ship_name',
                    'cus_lname', 'booking.bok_id', 'bok_selector', 'bok_date', 'bok_ship', 'group_id','bok_type AS Type')
                ->join('customer_booking', 'customer_booking.bok_id', '=', 'booking.bok_id')
                ->leftJoin('cruiseship', 'booking.ship_id', '=', 'cruiseship.ship_id')
                ->leftJoin('land', 'booking.land_id', '=', 'land.land_id')
                ->join('customer', 'customer.cus_id', '=', 'customer_booking.cus_id')
                ->join('associate', 'associate.aso_id', '=', 'booking.aso_id')
                ->where('booking.bok_id', '=', Binput::get('id'))
                ->first();

            // start count how many uploaded
            $count = count($files);

            if ($upload->group_id !== null) {

                $groupInfo = Group::findOrFail($upload->group_id);

                foreach ($files as $file) {
                    /**
                     * This renames the file before uploading to Amazon S3
                     */
                    $extension = $file->getClientOriginalExtension();

                    $category = Category::findOrFail($request['category']);
                    $fileName = uniqid('file_') . '.' . $extension;
                    $filePath = 'Groups/'.$upload->aso_name. '/'. studly_case($groupInfo->group_name). '/' . $fileName; // renaming image

                    $count++;

                    /**
                     * Uploading file to Amazon S3 Storage Service
                     *
                     * @var  mixed $s3     Calls the Connection to Amazon S3 using supplied credentials
                     * @var  array $result Uploads the given file into Amazon S3 Storage Service.
                     *
                     */

                    // Call out Amazon S3 Upload Class
                    $s3 = new AmazonS3();
                    // Upload File with filename, path, and file type extension
                    $result = $s3->s3Upload($file, $filePath, $extension);

                    // Call Image Class
                    $image = new Image;
                    // Booking ID
                    $image->bok_id = $upload->bok_id;
                    // Category ID
                    $image->cat_id = $request['category'];
                    // File Name
                    $image->img_name = $fileName;
                    // File Path
                    $image->img_path = $filePath;
                    // File Extension
                    $image->img_ext = $file->getClientOriginalExtension();
                    // File Extension Type
                    $image->img_mime = $file->getMimeType();
                    // File Encryption Type
                    $image->img_enc = $result['ServerSideEncryption'];
                    // File Size
                    $image->img_size = $file->getSize();
                    // File URL
                    $image->img_url = $result['ObjectURL'];
                    // File Notes
                    $image->img_notes = $request['notes'];
                    // File Uploaded By
                    $image->uploaded_by = username();
                    // Save Image
                    $image->save();

                    Session::flash('affirmative', 'New Image has successfully been added for Booking # ' . $upload->bok_id);
                }
            }
            else {
                foreach ($files as $file) {
                    /**
                     * This renames the file before uploading to Amazon S3
                     */
                    $extension = $file->getClientOriginalExtension();
                    $category = App\Category::findOrFail($request['category']);

                    $fileName = uniqid('file_') . '.' . $extension;

                    if($upload->Type === "Land")
                    {
                        $filePath = studly_case($upload->aso_name) . '/' . studly_case($upload->land_name) . '/' . unix_date($upload->bok_date) . '/' . $fileName; // renaming image
                    }
                    else
                    {
                        $filePath = studly_case($upload->aso_name) . '/' . studly_case($upload->ship_name) . '/' . unix_date($upload->bok_date) . '/' . $fileName; // renaming image
                    }

                    $count++;

                    /**
                     * Uploading file to Amazon S3 Storage Service
                     *
                     * @var  mixed $s3     Calls the Connection to Amazon S3 using supplied credentials
                     * @var  array $result Uploads the given file into Amazon S3 Storage Service.
                     *
                     */
                    $s3 = new AmazonS3();
                    // Uploading File to Amazon S3
                    $result = $s3->s3Upload($file, $filePath, $extension);

                    // Creates Database record for Image
                    $image = new Image;
                    // Booking ID
                    $image->bok_id = Binput::get('id');
                    // Category ID
                    $image->cat_id = $request['category'];
                    // Image Name
                    $image->img_name = $fileName;
                    // Image Path
                    $image->img_path = $filePath;
                    // Image Extension
                    $image->img_ext = $file->getClientOriginalExtension();
                    // Image MIME Type
                    $image->img_mime = $file->getMimeType();
                    // Image Encryption
                    $image->img_enc = $result['ServerSideEncryption'];
                    // Image Size
                    $image->img_size = $file->getSize();
                    // Image S3 URL
                    $image->img_url = $result['ObjectURL'];
                    // Image Notes
                    $image->img_notes = $request['notes'];
                    // Image Uploaded By
                    $image->uploaded_by = username();
                    // Save Data Record to Database
                    $image->save();

                    Session::flash('affirmative', 'New Image has successfully been uploaded for Booking # ' . $upload->bok_id);
                }
            }
        }
        else
        {
            // Abort Upload if Fails
            return abort(500);
        }
    }
}