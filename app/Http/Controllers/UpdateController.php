<?php namespace App\Http\Controllers;

/**
 * Created by PhpStorm.
 * User: hargcb
 * Date: 4/26/2015
 * Time: 5:08 PM
 */

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Support\Facades\Input;
use GrahamCampbell\Binput\Facades\Binput;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\DB;
use App;
use App\Customer;
use App\Booking;
use App\Image;
use Request;
use App\Http\Requests\Booking\BookingUpdateRequest;
use App\Http\Requests\Booking\LandBookingUpdateRequest;

/**
 * Class UpdateController
 *
 * @package App\Http\Controllers
 */
class UpdateController extends Controller
{

    /**
     *
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    protected $cusupdate;

    /**
     * @param $id
     *
     * @return $this
     */
    public function customerUpdate($id)
    {
        $cusupdate = Customer::findOrFail($id);
        return view('cusupdate', ['cus_update', $cusupdate]);
    }

    protected  $record;

    /**
     * @param \App\Http\Requests\CustomerUpdateRequest $request
     *
     * @return \Illuminate\Support\Facades\Redirect
     */
    public function storeUpdate(App\Http\Requests\CustomerUpdateRequest $request)
    {
        try {
            $record = Customer::where('cus_selector','=',Binput::get('customer'))->first();
            $record->cus_fname = titleCase(Binput::get('firstName')) ;
            $record->cus_lname = titleCase(Binput::get('lastName'));
            $record->aso_id = Binput::get('associate');
            $record->cus_notes = Binput::get('notesEdit');
            $record->save();
        }
        catch(\Exception $e)
        {
            return abort(500);
        }
        return back()->with('affirmative','Customer Information has been updated successfully');
    }

        //return true;

    protected $booking;

    protected $request;

    protected $identity;

    protected $ship;

    protected $land;

    /**
     * @param \App\Http\Requests\BookingUpdateRequest $request
     *
     * @return
     * @internal param \App\Http\Controllers\BookingUpdateRequest|\App\Http\Requests\BookingUpdateRequest $bookingUpdateRequest
     */
    public function updateBooking(BookingUpdateRequest $request)
    {

        try {
          /**
           * This function updates the booking for a selected customer.
           *
           */
            $booking = Booking::where('bok_selector','=',$request['booking'])->firstOrFail();
            $booking->bok_reservation = strtoupper($request['reservation']);
            $booking->aso_id = $request['associate'];
            $booking->bok_ship = $request['ship'];

            $identity = new App\Services\CustomerLookup();
            $ship = $identity->shipIdentity($request['ship']);
            $booking->ship_id = $ship;

            $booking->bok_date = unix_date($request['date']);
            $booking->bok_notes = $request['notes'];
            $booking->save();
        }
        catch(ModelNotFoundException $e)
        {
            return abort(500);
        }
        return Session::flash('affirmative', 'Booking has been updated successfully');
    }



    /**
     * @param \App\Http\Requests\BookingUpdateRequest $request
     *
     * @return
     * @internal param \App\Http\Controllers\BookingUpdateRequest|\App\Http\Requests\BookingUpdateRequest $bookingUpdateRequest
     */
    public function updateLandBooking(LandBookingUpdateRequest $request)
    {

        try {
          /**
           * This function updates the booking for a selected customer.
           *
           */
            $booking = Booking::where('bok_selector','=',$request['booking'])->firstOrFail();
            $booking->bok_reservation = strtoupper($request['reservation']);
            $booking->bok_ship = $request['ship'];


            /**
             * Retrieve Land Identity to insert into the Booking table.
             */

            $identity = new App\Services\CustomerLookup();
            $land = $identity->landIdentity($request['ship']);
            $booking->land_id = $land;

            $booking->aso_id = $request['associate'];
            $booking->bok_date = unix_date($request['date']);
            $booking->bok_notes = $request['notes'];
            $booking->save();
        }
        catch(ModelNotFoundException $e)
        {
            return abort(500);
        }
        return Session::flash('affirmative', 'Booking has been updated successfully');
    }

    public function viewChangePassword()
    {

        return view('admin.changePassword');

    }
}