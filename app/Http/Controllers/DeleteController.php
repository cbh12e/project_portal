<?php
/**
 * Created by PhpStorm.
 * User: hargcb
 * Date: 5/4/2015
 * Time: 1:41 PM
 */

namespace App\Http\Controllers;
use Input;
use App;
use App\Image;
use App\Customer;
use App\Booking;
use App\Services\AmazonS3;
use GrahamCampbell\Binput\Facades\Binput;
use Illuminate\Support\Facades\DB;
use Illuminate\Database\Eloquent;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Session;
use Aws\S3Client;
use Aws\S3\Exception\S3Exception;

use App\Http\Requests\Booking\DeleteBookingRequest;
use App\Http\Requests\Booking\DeleteImageRequest;
use App\Http\Requests\Booking\DeleteCustomerRequest;
/**
 * Class DeleteController
 *
 * @package App\Http\Controllers
 */
class DeleteController extends Controller
{

    /**
     * @var
     */
    protected $s3;
    /**
     * @var
     */
    protected $s2;
    /**
     * @var
     */
    protected $s1;
    /**
     * @var
     */
    protected $del;
    /**
     * @var
     */
    protected $id;
    /**
     * @var
     */
    protected $result;
    /**
     * @var
     */
    protected $booking;
    /**
     * @var
     */
    protected $delete;
    /**
     * @var
     */
    protected $image;
    /**
     * @var
     */
    protected $book;
    /**
     * @var
     */
    protected $count;
    /**
     * @var
     */
    protected $lax;

    protected $request;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * @return \Illuminate\Http\RedirectResponse
     */
    public function customerDelete(DeleteCustomerRequest $request)
    {
        /*
         * This Deletes the record of the customer
         * in the database. This does not return anything
         * right now, since this is executed using AJAX.
         */


        $id = Binput::get('delete');

        $delete = Customer::where('cus_selector','=',$id)->firstOrFail();
        $delete->delete();

            return redirect('home')->with('affirmative', 'Customer has been deleted successfully!');
    }


    /**
     * @return \Illuminate\View\View
     */
    public function deleteImage(DeleteImageRequest $request)
    {
        $id = Binput::get('image');
        $image = Image::findOrFail($id);

        $s3 = new AmazonS3();
        $s3->s3Delete($image->img_path);

        $image->delete();

        return back()->with('affirmative','File has been deleted successfully');

    }

    /**
     * @return \Illuminate\View\View
     */
    public function deleteGroupImage(DeleteImageRequest $request)
    {
        $id = Binput::get('image');
        $image = Image::findOrFail($id);

        $s3 = new AmazonS3();
        $s3->s3Delete($image->img_path);

        $image->delete();

        return back()->with('affirmative','File has been deleted successfully');

    }

    /**
     * This deletes the booking from the customer in the database.
     *
     * @param \App\Http\Requests\Booking\DeleteBookingRequest $request
     *
     * @return mixed
     */

    public function deleteBook(DeleteBookingRequest $request)
    {
        $id = Binput::get('booking');

        $del = Booking::where('bok_selector','=',$id)->firstOrFail();


        if (count($del->customers) === 1)
        {
        //$del->customers()->detach(1, ['bok_id'=> $id, 'cus_id' => $del->cus_id]);

        if (count($s1= Image::where('bok_id', '=', $del->bok_id)->get()) > 0)
        {
            foreach($s1 as $s2)
            {
                $s3 = new AmazonS3();
                $s3->s3Delete($s2->img_path);
            }

            $del->delete();
            Session::flash('affirmative','Booking # '.$del->bok_id.' and all images associated with the booking has been deleted successfully');
        }
        else
        {
            $del = Booking::where('bok_selector','=',$id)->firstOrFail();
            $del->delete();

            Session::flash('affirmative','Booking #'.$del->bok_id.' has been deleted successfully');
        }
        //Session::flash('affirmative','Booking has been deleted successfully');
       // return redirect('search');
    }
        else
        {
            return response()->json([
                'message' => ['Bookings with more than one customer cannot be deleted. Detach all customers but one and then try again.']
            ], 422);

           // return back()->with('exception','There is currently more than one customer in this booking. You must detach all but one before you can perform this action');
        }
       // return back()->with('exception','There is currently more than one customer in this booking. You must detach all but one before you can perform this action');

        }

}