<?php namespace App\Http\Controllers;


use App\User;
use App\Booking;
use App\Image;
use App\Customer;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Support\Facades\Input;
use Illuminate\Filesystem;
use App\Http\Requests\PhotoUpdateRequest;
use Auth;
use Torann\GeoIP\GeoIP;
use LaravelAnalytics;

/**
 * Class HomeController
 *
 * @package App\Http\Controllers
 */
class HomeController extends Controller {

	/*
	|--------------------------------------------------------------------------
	| Home Controller
	|--------------------------------------------------------------------------
	|
	| This controller renders your application's "dashboard" for users that
	| are authenticated. Of course, you are free to change or remove the
	| controller as you wish. It is just here to get your app started!
	|
	*/

	/**
	 * Create a new controller instance.
	 *
	 * @return void
	 */
	public function __construct()
    {
        $this->middleware('auth');
    }

	/**
	 * Show the application dashboard to the user.
	 *
	 * @return Response
	 */

    protected $bookings;

    protected $images;

    protected $mem_use;

	public function index()
	{

        /**
         * Count all Bookings, Records, and Images
         */
        $bookings = Booking::where('aso_id','<>',99)->count();
        $images = Image::all()->count();
        //$analytics = LaravelAnalytics::getVisitorsAndPageViews(7);
        /**
         * Server Memory Usage
         */
        $mem_use = memory_get_usage(true);

        $analytics =  $this->getGoogleData();
        return view('home', compact('bookings','images','mem_use'));

	}

    /**
     * @return \Illuminate\View\View
     */
    public function add()
    {
        return view('add');
    }

    /**
     * @return \Illuminate\View\View
     */
    public function profile()
    {
        return view('admin.profile');
    }

    /**
     * @return \Illuminate\View\View
     */
    public function calendar()
    {
        return view('calendar');
    }

    /**
     * @param $id
     *
     * @return $this
     */
    public function changeImage()
    {

        return view('admin.image');
    }

    /**
     * @return mixed
     */
    public function getGoogleData()
    {
        $analytics = LaravelAnalytics::getVisitorsAndPageViews(60);
        $data = array();
        foreach ($analytics as $index=>$result):
            $data[$index] = [
                'date'  => unix_date($result['date']),
                'visitors'    => $result['visitors'],
                'usDate'  => human_date($result['date']),
                'value' => $result['visitors'],
                'label'  => human_date($result['date'])
            ];
        endforeach;
       // return json_encode($data);



return response()->json($data);

            //return response()->json($analytics);
    }

    /**
     * @return mixed
     */
    public function getGoogleBrowserData()
    {
        $analytics = LaravelAnalytics::getTopBrowsers(60,6);

        $data = array();
        foreach ($analytics as $index=>$result):
            $data[$index] = [
                'label'  => $result['browser'],
                'value'    => $result['sessions'],
            ];
        endforeach;

        return response()->json($data);
    }

    /**
     *  getMostVisitedPages($numberOfDays = 365, $maxResults = 20)
     * @return mixed
     */
    public function getGooglePagesData()
    {
        $analytics = LaravelAnalytics::getMostVisitedPages(60,12);
/*        dd($analytics);
        die();*/
        $data = array();
        foreach ($analytics as $index=>$result):
            $data[$index] = [
                'label'  => $result['url'],
                'value'    => $result['pageViews'],
            ];
        endforeach;

        return response()->json($data);
    }

    protected $user;
    protected $request;
    /**
     * @param \App\Http\Requests\PhotoUpdateRequest $photoUpdateRequest
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector|string
     */
    public function updateImage(PhotoUpdateRequest $request)
    {
        try{
            $user = User::findOrFail(Auth::user()->id);
            $destinationPath = 'profile'; // upload path
            $extension = Input::file('photo')->getClientOriginalExtension(); // getting image extension
            $fileName = rand(11111, 99999) . '.' . $extension; // renameing image
            Input::file('photo')->move($destinationPath, $fileName); // uploading file to given path
            $file1 = ''.$destinationPath.'/'.$fileName.'';
            $user->photo = $file1;
            $user->save();
            return redirect('home')->with('affirmative','Profile image has been successfully updated.');
        }
        catch(ModelNotFoundException $e){
            return App::abort(500);
        }
    }
}
