<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

/**
 * Login Routes
 *
 * This is here should the login  break in Laravel 5.2
 */

/**
 *  This is disabled in favor of Two Factor Authentication
 * Route::get('auth/login', 'Auth\AuthController@getLogin');
 * Route::post('auth/login', 'Auth\AuthController@postLogin');
 * Route::get('auth/logout', 'Auth\AuthController@getLogout');
 */

/**
 * Two Factor Authorization Routes
 */
Route::get('2fa{id})', [
    'as'   => 'factor',
    'uses' => 'Auth\AuthController@showTwoFactorAuth',
]);

Route::post('auth/duo', 'Auth\AuthController@postDuoLogin');
Route::get('auth/postLogin/{id}', 'Auth\AuthController@showTwoFactor');


Route::group([ 'middleware' => 'signedurl', ], /**
 *
 */
    function() {
        Route::post('2factor', 'Auth\AuthController@postTwoFactor');
    });

/**
 * Two-Factor Enable and Disable
 */
Route::get('qr', 'Auth\TwoFactorController@getQR');
Route::post('enable2factor', 'Auth\TwoFactorController@enable2Fa');
Route::post('disable2factor', 'Auth\TwoFactorController@disable2Fa');

/**
 * User Account Routes
 */
Route::get('/', 'WelcomeController@login');
Route::get('home', 'HomeController@index');
Route::get('calendar', 'HomeController@calendar');



/**
 * Search Routes
 */
Route::get('search', 'Search\LookupController@all');
Route::get('search/', 'Search\LookupController@all');
Route::get('search/associate', 'Search\LookupController@agent');
Route::get('search/agent/{term?}', 'Search\LookupController@agent');
Route::get('booking/{id}', 'RecordController@viewBooking');
Route::get('booking/images/{id}', 'RecordController@getImages');
Route::get('listImage/{id}', 'Search\LookupController@listImage');

Route::get('leon', 'HomeController@getGoogleData');
Route::get('browser', 'HomeController@getGoogleBrowserData');
Route::get('pages', 'HomeController@getGooglePagesData');

Route::group([ 'middleware' => 'signedurl', ], /**
 *
 */
    function() {

        /**
         * Individual Booking Search
         */
        Route::get('user/profile', 'HomeController@profile');
        //Route::get('search/agent/{term?}', 'RecordController@agent');
        Route::get('search/booking/{id}', 'RecordController@viewBooking');
        Route::get('image/create/{id}', 'RecordController@addImage');
        Route::get('group/image/create/{id}', 'Amazon\GroupUploadController@newImagePage');

        /**
         * User Profile
         */
        Route::post('profile/image', 'HomeController@updateImage');
        Route::get('profile/image', 'HomeController@changeImage');
        Route::get('profile/password', 'UpdateController@viewChangePassword');
        Route::post('profile/password', 'UpdateController@changePassword');

        /**
         * Group Booking Search
         */
        //  Route::get('groups/search/agent/{id?}','GroupController@associateSearch');
    });

//Route::get('search/booking/{id}', 'RecordController@viewBooking');

/**
 * Customer View Pages
 */
Route::get('search/customer/{id}', 'RecordController@viewCustomer');
Route::get('search/customer/page/{id}', 'RecordController@showCustomerTable');
Route::get('search/customer{q?}', 'Search\LookupController@searchCustomer');
Route::get('search/customer/fail','RecordController@searchFailed');

/**
 * Twitter Typeahead and Select2 queries
 */
Route::get('search/select/query' , 'Search\LookupController@customerQuery'); // SELECT2 SEARCH QUERY
//Route::get('search/select/reservation/query' , 'Search\LookupController@reservationQuery'); // SELECT2 RESERVATION SEARCH QUERY
Route::get('search/reservations/q' , 'Search\LookupController@reservationQuery'); // SELECT2 RESERVATION SEARCH QUERY
Route::get('reservation' , 'Search\LookupController@reservationLookup'); // SELECT2 RESERVATION SEARCH QUERY
Route::get('customer/lookup', 'Search\LookupController@customerLookup');

/**
 * Add Routes
 */
Route::get('add', 'RecordController@addCustomer');
Route::get('search/new/{id}', 'RecordController@booking');
Route::post('search/customer/booking/image', 'Amazon\UploadController@s3ImageRequest');
Route::post('search/new/{id}', 'RecordController@bookingStoreRequest');
Route::post('search/customer/add', 'RecordController@bookingStoreRequest');
Route::post('booking/customer/add', 'RecordController@addToExistingBooking');
Route::post('add', 'RecordController@store');
Route::post('booking/customer/existing/add', 'RecordController@newCustomerExistingBooking');

/**
 * Update Routes
 */
Route::get('customer/update/{id}', 'UpdateController@customerUpdate');
//Route::get('profile/image/{id}', 'HomeController@ChangeImage');
Route::post('search/customer/{id}', 'UpdateController@storeUpdate');
//Route::post('profile/image', 'HomeController@updateImage');
Route::post('customer/update', 'UpdateController@storeUpdate');
Route::post('customer/booking/update', 'UpdateController@updateBooking');
Route::post('customer/bookingL/update', 'UpdateController@updateLandBooking');


/**
 * Inserts Records into null values that were added into the database.
 *
 *  Keep these routes commented out in production environment.
 *
 * Route::get('null1', 'RecordController@customerRowInsert');
 * Route::get('null2', 'RecordController@bookingRowInsert');
 * Route::get('null3', 'RecordController@groupRowInsert');
 * Route::get('null4', 'RecordController@agentRowInsert');
 */

/**
 * Delete Routes
 */
Route::post('customer/booking/image/delete', 'DeleteController@deleteImage');
Route::post('group/image/delete','DeleteController@deleteGroupImage');
Route::post('customer/booking/delete', 'DeleteController@deleteBook');
Route::post('customer/delete','DeleteController@customerDelete');
Route::post('customer/detach','RecordController@detachCustomer');

/**
 * Group Routes
 */
Route::group(['prefix' => 'groups'], function () {
    Route::get('add', 'GroupController@addGroup');
    Route::get('new', 'GroupController@newCustomer');
    Route::get('active', 'GroupController@showAllFutureSailings');
    Route::get('past', 'GroupController@showAllPastSailings');
    Route::get('cancelled', 'GroupController@showAllCancelledSailings');
    Route::get('search/associate', 'GroupController@associateSearch');
    //Route::get('search/agent/{id}', 'GroupController@associateSearch');
    Route::get('image/add', 'Amazon\GroupUploadController@groupUpload');
    Route::post('image/add', 'Amazon\GroupUploadController@groupUpload');
    Route::get('list/{id}', 'GroupController@showGroup');

    Route::group(['middleware' => 'signedurl'], function () {
        Route::get('{id}', 'GroupController@showGroup');
    });

    Route::post('new', 'GroupController@addCustomer');
    Route::post('add', 'GroupController@groupAdd');
    Route::post('update', 'GroupController@updateGroup');
    Route::post('delete', 'GroupController@deleteGroup');
});

/**
 * Ship Name Routes
 */
Route::group(['prefix' => 'ship' ], function () {

    Route::get('', 'ShipController@cruiseLineView');
    Route::get('line/view/{id}', 'ShipController@viewShips');
    //Route::post('ship/update', 'ShipController@updateShip')
    Route::get('create', 'ShipController@getNewShip');
    Route::post('create', 'ShipController@setNewShip');
    Route::get('create/cl', 'ShipController@createNewCl');

    //Route::post('update', 'ShipController@updateShip');
    //Route::post('inactive', 'ShipController@inactiveShip');
});

Route::group(['prefix' => 'land' , 'middleware' => 'signedurl'], function () {
    Route::get('list', 'LandController@viewProviders');
    Route::get('edit/{id}', 'LandController@editGet');
    Route::post('edit', 'LandController@editPost');
    Route::get('create', 'LandController@createProvider');
    Route::post('create', 'LandController@postProvider');
    Route::get('delete/{id}', 'LandController@deleteGet');
    Route::post('delete/{id}', 'LandController@deletePost');

    });

Route::post('search/customer/group/add','GroupController@newGroupBooking');

/**
 * Twitter Typeahead Queries
 *
 * These use towns in the Florida Western Panhandle aka the developer's alumnus college: Florida State University located in Tallahassee, Florida.
 * These are used to prevent someone from just typing reservations/query to get all information.
 */
Route::get('monticello/query',   'LandController@search'); // TYPEAHEAD BLOODHOUND SEARCH QUERY
Route::get('tallahassee/query',  'ShipController@shipSearch'); // TYPEAHEAD BLOODHOUND SEARCH QUERY
Route::get('blountstown/query',  'GroupController@groupSearch'); // TYPEAHEAD BLOODHOUND SEARCH QUERY
Route::get('quincy/query',       'Search\LookupController@reservationSearch'); // TYPEAHEAD BLOODHOUND SEARCH QUERY

/**
 * Controller Routes
 */

Route::controllers([
    'auth' => 'Auth\AuthController',
    'password' => 'Auth\PasswordController',
]);

/**
 * Administration Routes
 */

Route::group(['prefix' => 'admin', 'middleware' => 'auth', 'namespace' => 'Admin'], /**
 *
 */
    function() {
        Route::pattern('id', '[0-9]+');
        Route::pattern('id2', '[0-9]+');

        # Admin Dashboard
        Route::get('dashboard', 'DashboardController@index');

        # Users
        Route::get('users/', 'UserController@index');
        Route::get('users/profile/{id}', 'UserController@getProfile');
        Route::post('users/create', 'UserController@postCreate');
        Route::get('users/{id}/edit', 'UserController@getEdit');
        Route::post('users/{id}/edit', 'UserController@postEdit');
        Route::get('users/{id}/delete', 'UserController@getDelete');
        Route::post('users/delete', 'UserController@postDelete');
        Route::get('users/data', 'UserController@data');
    });

/**
 * Laravel Messenger Routes
 */
Route::group(['prefix' => 'messages'], function () {
    Route::get('/', ['as' => 'messages', 'uses' => 'MessagesController@index']);
    Route::get('create', ['as' => 'messages.create', 'uses' => 'MessagesController@create']);
    Route::post('/', ['as' => 'messages.store', 'uses' => 'MessagesController@store']);
    Route::group(['middleware' => 'signedurl'], function () {
        Route::get('{id}', ['as' => 'messages.show', 'uses' => 'MessagesController@show']);
    });
    Route::put('{id}', ['as' => 'messages.update', 'uses' => 'MessagesController@update']);
});

/**
 * File Manager Routes
 */

Route::group(['prefix' => 'upload', 'middleware' => 'auth'], /**
 *
 */
    function() {
        get('', 'UploadController@index');
//post('upload/file', 'UploadController@uploadFile');
//delete('upload/file', 'UploadController@deleteFile');
//post('upload/folder', 'UploadController@createFolder');
//delete('upload/folder', 'UploadController@deleteFolder');
    });
