<?php
/**
 * Created by PhpStorm.
 * User: hargcb
 * Date: 5/7/2015
 * Time: 1:48 PM
 */

namespace App\Http\Requests;

use App\Http\Requests\Request;
use Illuminate\Validation\Validator;

/**
 * Class PhotoUpdateRequest
 *
 * @package App\Http\Requests
 */
class PhotoUpdateRequest {

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'photo' => 'required|image',
        ];
    }


}