<?php

namespace App\Http\Requests;



/**
 * Class DeleteProviderRequest
 *
 * @package App\Http\Requests
 */
class DeleteProviderRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'id' => 'required|exists:mysql3.land,land_id'
        ];
    }
}
