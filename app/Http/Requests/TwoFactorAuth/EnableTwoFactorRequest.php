<?php
/**
 * Created by PhpStorm.
 * User: hargcb
 * Date: 12/31/2015
 * Time: 1:51 PM
 */

namespace App\Http\Requests\TwoFactorAuth;

use Illuminate\Foundation\Http\FormRequest;

/**
 * Class EnableTwoFactorRequest
 *
 * @package app\Http\Requests\TwoFactorAuth
 */
class EnableTwoFactorRequest extends FormRequest
{

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'g-recaptcha-response' => 'required|recaptcha',
        ];
    }

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

}