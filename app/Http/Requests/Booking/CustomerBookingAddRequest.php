<?php

namespace App\Http\Requests\Booking;
use App\Http\Requests\Request;

/**
 * Class CustomerBookingAddRequest
 *
 * @package App\Http\Requests
 */
class CustomerBookingAddRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'id' => 'required|integer|exists:mysql3.customer,cus_id',
            'reservation' => 'required|string|max:15|exists:mysql3.booking,bok_reservation'
        ];
    }
}