<?php
/**
 * Created by PhpStorm.
 * User: hargcb
 * Date: 4/23/2015
 * Time: 11:33 AM
 */

namespace App\Http\Requests\Booking;

use App\Http\Requests\Request;
use Illuminate\Validation\Validator;

/**
 * Class bookingStoreRequest
 *
 * @package App\Http\Requests
 */
class BookingStoreRequest extends Request {

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'provider' => 'required',
            'reservation' => 'min:3|max:15|required|unique:mysql3.booking,bok_reservation',
            'ship' => 'required_unless:provider,Land|max:30|exists:mysql3.cruiseship,ship_name',
            'land' => 'required_unless:provider,Sea|max:30|exists:mysql3.land,land_name',
            'date' => 'required|date',
            'notes'=> 'max:500',
        ];
    }

    /**
     * @return array
     */
    public function messages()
    {
        return [
            'ship.exists' => 'The name of the cruise ship entered does not exist in the database.',
            'land.exists' => 'The name of The name of the provider entered does not exist in the database.',
            'reservation.exists' => 'The reservation number entered is already taken.'
        ];
    }
}
