<?php

namespace App\Http\Requests\Booking;

use App\Http\Requests\Request;

class ExistingBookingNewCustomerRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'booking' => 'required',
            'associate' => 'required|integer',
            'firstName' => 'required|max:20|string',
            'lastName' => 'required|min:2|max:30|string',
            'notes' => 'max:255',
        ];
    }
}
