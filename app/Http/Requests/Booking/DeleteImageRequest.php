<?php

namespace App\Http\Requests\Booking;

use App\Http\Requests\Request;

/**
 * Class DeleteImageRequest
 *
 * @package App\Http\Requests\Booking
 */
class DeleteImageRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'image' => 'required|integer|exists:mysql3.image,img_id'
        ];
    }
}
