<?php
/**
 * Created by PhpStorm.
 * User: hargcb
 * Date: 4/23/2015
 * Time: 11:33 AM
 */

namespace App\Http\Requests\Booking;

use App\Http\Requests\Request;
use Illuminate\Validation\Validator;

/**
 * Class BookingUpdateRequest
 *
 * @package App\Http\Requests
 */
class BookingUpdateRequest extends Request {

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [

            'booking' => 'required',
            'associate' => 'required|integer',
            'reservation' => 'min:3|unique:mysql3.booking,bok_reservation,'.$this->get('booking').',bok_selector',
            'ship' => 'required|max:35||exists:mysql3.cruiseship,ship_name',
            'date' => 'date|required',
        ];
    }

    public function messages()
    {
        return [
            'ship.exists' => 'The name of the cruise ship entered does not exist in the the database.',
            'reservation.exists' => 'The reservation number that was entered already exists in the database.'
        ];
    }
}