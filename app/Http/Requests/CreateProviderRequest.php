<?php

namespace App\Http\Requests;

/**
 * Class CreateProviderRequest
 * @package App\Http\Requests
 */
class CreateProviderRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'provider' => 'required|unique:mysql3.land,land_name',
            'notes' => 'max:300'
        ];
    }
}
