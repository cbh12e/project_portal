<?php
/**
 * Created by PhpStorm.
 * User: hargcb
 * Date: 12/31/2015
 * Time: 1:51 PM
 */

namespace App\Http\Requests\Search;

use Illuminate\Foundation\Http\FormRequest;

/**
 * Class EnableTwoFactorRequest
 *
 * @package app\Http\Requests\TwoFactorAuth
 */
class CustomerSearchRequest extends FormRequest
{

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'q' => 'required',
        ];
    }

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    public function messages()
    {
        return [
          'q.required' => 'The customer search field is required to perform a customer search and it cannot be empty.'
        ];
    }

}