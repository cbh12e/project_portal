<?php
/**
 * Created by PhpStorm.
 * User: hargcb
 * Date: 12/31/2015
 * Time: 1:51 PM
 */

namespace App\Http\Requests\Search;

use Illuminate\Foundation\Http\FormRequest;


/**
 * Class GroupLookupRequest
 *
 * @package App\Http\Requests\Search
 */
class GroupLookupRequest extends FormRequest
{

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
 /*       return [
            'q' => 'required',
        ];*/
    }

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

}