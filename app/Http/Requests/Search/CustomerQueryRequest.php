<?php
/**
 * Created by PhpStorm.
 * User: hargcb
 * Date: 12/31/2015
 * Time: 1:51 PM
 */

namespace App\Http\Requests\Search;

use Illuminate\Foundation\Http\FormRequest;


/**
 * Class CustomerQueryRequest
 *
 * @package App\Http\Requests\Search
 */
class CustomerQueryRequest extends FormRequest
{

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'term' => 'required',
        ];
    }

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Returns a message when validation fails.
     *
     * @return array
     */
    public function messages()
    {
        return [
            'term.required' => 'The search customer field cannot be empty.'
        ];
    }


}