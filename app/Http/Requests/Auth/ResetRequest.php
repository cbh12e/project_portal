<?php namespace App\Http\Requests\Auth;

use App\Http\Requests\Request;
use Illuminate\Foundation\Http\FormRequest;

/**
 * Class ResetRequest
 *
 * @package App\Http\Requests
 */
class ResetRequest extends Request {

	/**
	 * Determine if the user is authorized to make this request.
	 *
	 * @return bool
	 */
	public function authorize()
	{
		return true;
	}

	/**
	 * Get the validation rules that apply to the request.
	 *
	 * @return array
	 */
	public function rules()
	{
		return [
			'token' => 'required',
			'email' => 'required|email',
			'username' => 'required',
			'password' => 'required|confirmed|min:8|case_diff|numbers|letters|symbols',
			'g-recaptcha-response' => 'required|recaptcha',
		];
	}
}
