<?php

namespace App\Http\Requests\Customer;

use App\Http\Requests\Request;

/**
 * Class DeleteCustomerRequest
 *
 * @package App\Http\Requests\Booking
 */
class DeleteCustomerRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'delete' => 'required|string|exists:mysql3.customer,cus_selector'
        ];
    }
}
