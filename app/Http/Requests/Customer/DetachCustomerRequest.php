<?php

namespace App\Http\Requests\Customer;

use App\Http\Requests\Request;

/**
 * Class DetachCustomerRequest
 *
 * @package App\Http\Requests\Booking
 */
class DetachCustomerRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'booking' => 'required|exists:mysql3.booking,bok_id',
            'customer' => 'required|exists:mysql3.customer,cus_id'
        ];
    }
}
