<?php
/**
 * Created by PhpStorm.
 * User: hargcb
 * Date: 4/26/2015
 * Time: 5:41 PM
 */

namespace App\Http\Requests\Customer;

use App\Http\Requests\Request;
use Illuminate\Validation\Validator;

/**
 * Class CustomerUpdateRequest
 *
 * @package App\Http\Requests
 */
class CustomerUpdateRequest extends Request{


    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'customer' => 'required',
            'associate' => 'required|integer',
            'firstName' => 'required|max:20|string',
            'lastName' => 'required|min:2|max:30|string',
            'notesEdit'=> 'max:255',
        ];
    }
}