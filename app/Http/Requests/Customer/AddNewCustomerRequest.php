<?php namespace App\Http\Requests\Customer;

use App\Http\Requests\Request;
use Illuminate\Validation\Validator;

/**
 * Class AddNewCustomerRequest
 * @package App\Http\Requests\Customer
 */
class AddNewCustomerRequest extends Request {

	/**
	 * Determine if the user is authorized to make this request.
	 *
	 * @return bool
	 */
	public function authorize()
	{
		return true;
	}

	/**
	 * Get the validation rules that apply to the request.
	 *
	 * @return array
	 */
	public function rules()
	{
		return [
            'firstName' 	=> 'required|max:20}string',
            'lastName' 		=> 'required|min:2|max:30|string',
			'associate' 	=> 'required|integer',
            'notes'			=> 'max:255',
			'provider'		=> 'required',
            'reservation' 	=> 'required_with:date|max:15|unique:mysql3.booking,bok_reservation',
            'ship' 			=> 'max:30|exists:mysql3.cruiseship,ship_name',
            'land' 			=> 'max:30|exists:mysql3.land,land_name',
            'date' 			=> 'required_with:ship|required_with:land|date'
		];
	}

	public function messages()
	{
		return [
			'ship.exists' => 'The name of the ship entered does not exist in the database.',
			'land.exists' => 'The name of the provider entered does not exist in the database.',
			'reservation.unique' => 'The reservation number that was entered already exists in the database.'
		];
	}
}