<?php
/**
 * Created by PhpStorm.
 * User: hargcb
 * Date: 6/24/2015
 * Time: 9:34 PM
 */

namespace App\Http\Requests\Groups;

use Illuminate\Foundation\Http\FormRequest;
/**
 * Class NewGroupRequest
 *
 * @package app\Http\Requests
 */
class NewGroupRequest extends FormRequest
{


    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'associate' => 'required|integer',
            'groupName' => 'max:30|required|string',
            'groupNum' => 'max:100|required|unique:mysql3.group,group_number',
            'ship' => 'required|max:30|exists:mysql3.cruiseship,ship_name',
            'date' => 'required|date',
            'notes' => 'max:300',
        ];


    }

    /**
     * Get the custom validation messages that apply to the request.
     *
     * @return array
     */
    public function messages()
    {
        return [
            'groupNum.unique' => 'The group number entered already exists.',
            'groupNum.required' => 'The group number field is required.',
            'ship.exists' => 'The name of the cruise ship entered is not valid.'
        ];
    }
}
