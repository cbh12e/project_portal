<?php
/**
 * Created by PhpStorm.
 * User: hargcb
 * Date: 6/25/2015
 * Time: 1:43 PM
 */

namespace App\Http\Requests\Groups;

use Illuminate\Foundation\Http\FormRequest;

/**
 * Class UpdateGroupRequest
 *
 * @package app\Http\Requests\Groups
 */
class UpdateGroupRequest extends FormRequest{


    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'associate' => 'required|integer',
            //'group' => 'required',
            'name' => 'required|max:50',
            'number' =>  'max:100|required',
	        'ship' => 'required|max:25|exists:mysql3.cruiseship,ship_name',
            'date' => 'required|date',
            'notes'=> 'max:255',
        ];
    }
    public function messages()
    {
        return [
            'ship.exists' => 'The name of the cruise ship entered is invalid.'
        ];
    }

}
