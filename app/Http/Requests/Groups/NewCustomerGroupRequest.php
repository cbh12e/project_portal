<?php
/**
 * Created by PhpStorm.
 * User: hargcb
 * Date: 8/31/2015
 * Time: 3:33 PM
 */

namespace App\Http\Requests\Groups;

use Illuminate\Foundation\Http\FormRequest;

/**
 * Class NewCustomerGroupRequest
 * @package App\Http\Requests\Groups
 */
class NewCustomerGroupRequest extends FormRequest
{

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'group' => 'max:30|required|exists:mysql3.group,group_number',
            'reservation' => 'max:30|required|unique:mysql3.booking,bok_reservation',
            'firstName' => 'required|string|max:20',
            'lastName' => 'required|string|max:30',
            'notes' => 'max:300',
            'notesB' => 'max:300',
        ];
    }

    /**
     * @return array
     */
    public function messages()
    {
        return [

        ];
    }
}
