<?php

namespace App\Http\Requests\Groups;

use App\Http\Requests\Request;

/**
 * Class AttachCustomerRequest
 *
 * @package App\Http\Requests\Groups
 */
class AttachCustomerRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'group' => 'required|max:30|exists:mysql3.group,group_number',
            'reservation' => 'min:3|max:15|required|unique:mysql3.booking,bok_reservation',
        ];
    }
}
