<?php namespace App\Http\Requests;

use App\Http\Requests\Request;
use Illuminate\Validation\Validator;

/**
 * Class S3StoreRequest
 *
 * @package App\Http\Requests
 */
class S3StoreRequest extends Request {

	/**
	 * Determine if the user is authorized to make this request.
	 *
	 * @return bool
	 */
	public function authorize()
	{
		return true;
	}

	/**
	 * Get the validation rules that apply to the request.
	 *
	 * @return array
	 */
	public function rules()
	{
		return [
            'id' => 'required',
            'file' => 'required',
			'category'=> 'required',
            'notes'=> 'max:255',

		];
	}
}
