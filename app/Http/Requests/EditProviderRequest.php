<?php

namespace App\Http\Requests;

/**
 * Class EditProviderRequest
 * @package App\Http\Requests
 */
class EditProviderRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'id' => 'required|integer|exists:mysql3.land,land_id',
            'provider' => 'required|max:50|unique:mysql3.land,land_name,'.$this->provider.',land_name',
            'notes' => 'max:300'
        ];
    }
}
