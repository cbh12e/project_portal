<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class CreateNewShipRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [

            'a' => 'required',
            'shipName' => 'required|max:40|unique:cruiseship,ship_name',
            'notes' => 'max:300',
            //
        ];
    }
}
