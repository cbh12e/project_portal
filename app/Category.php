<?php namespace App;
/**
 * Created by PhpStorm.
 * User: hargcb
 * Date: 4/20/2015
 * Time: 9:41 AM
 */
use Illuminate\Database\Eloquent\Model;

/**
 * Class Category
 *
 * @package App
 */
class Category extends Model
{
    /**
     * @var string
     */
    protected $connection = 'mysql3';

    /**
     * @var string
     */
    protected $table = 'category';
    /**
     * @var string
     */

    protected $primaryKey = 'cat_id';

    protected $fillable = ['cat_id','cat_name','cat_desc','cat_type'];

}