<?php namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Kyslik\ColumnSortable\Sortable;
/**
 * Class Associate
 *
 * @package App
 */
class Associate extends Model
{

    use Sortable;

    public function getAssociateNameAttribute()
    {
        return $this->attributes['aso_fname'] .' '. $this->attributes['aso_lname'];
    }

    public function customers()
    {
        return $this->hasMany('App\Customer','aso_id','aso_id');

    }

    //use SoftDeletes;
    /**
     * @var string
     */
    protected $primaryKey = 'aso_id';

    /**
     * @var string
     */
    protected $connection = 'mysql3';

    /**
     * @var string
     */
    protected $table = 'associate';
    /**
     * @var string
     */
    public $sortable = ['aso_lname'];

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['deleted_at'];

    protected $guarded = ['aso_active'];

    protected $fillable = ['aso_id','aso_selector','aso_fname','aso_lname','aso_role','aso_street','aso_city','aso_state', 'aso_zip','aso_notes'];



}