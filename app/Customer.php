<?php namespace App;
/**
 * Created by PhpStorm.
 * User: hargcb
 * Date: 4/19/2015
 * Time: 10:41 PM
 */

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Kyslik\ColumnSortable\Sortable;
/**
 * Class Customer
 *
 * @package App
 */
class Customer extends Model
{
    //use SoftDeletes;

    use Sortable;
    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function associate()
    {
        return $this->hasOne('App\Associate', 'aso_id', 'aso_id');
    }

    public function booking()
    {
        //return $this->hasOne('App\Booking','cus_id','cus_id');

        //return $this->hasManyThrough('App\Booking','App\Customer_Booking', 'cus_id', 'cus_id');

        return $this->belongsToMany('App\Booking', 'customer_booking')->with('bok_id','bok_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function customer()
    {
        return $this->hasMany('App\Booking','cus_id','cus_id');
    }

    /**
     * @return string
     */
    public function getCustomerFullNameAttribute()
    {
        return $this->attributes['cus_fname'] .' '. $this->attributes['cus_lname'];
    }

    protected $sortable = ['cus_lname','cus_fname','aso_id'];

    protected $primaryKey = 'cus_id';

    /**
     * @var string
     */
    protected $connection = 'mysql3';

    /**
     * @var string
     */
    protected $table = 'customer';

    /**
     * @var string
     */
    protected $fillable = ['cus_id','aso_id','cus_selector','cus_fname','cus_lname','cus_notes'];

}