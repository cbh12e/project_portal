<?php namespace App;
/**
 * Created by PhpStorm.
 * User: hargcb
 * Date: 4/19/2015
 * Time: 10:41 PM
 */
use Illuminate\Database\Eloquent\Model;

/**
 * Class Booking
 *
 * @package App
 */
class Land extends Model
{

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function provider()
    {
        return $this->hasOne('App\CruiseLine', 'cl_id', 'cl_id');
    }

    protected $primaryKey = 'land_id';

    /**
     * @var string
     */
    protected $connection = 'mysql3';

    /**
     * @var string
     */
    protected $table = 'land';
    /**
     * @var string
     */

    protected $fillable = ['land_id','cl_id','land_name','land_notes'];

}