<?php namespace App;
/**
 * Created by PhpStorm.
 * User: hargcb
 * Date: 4/19/2015
 * Time: 10:41 PM
 */
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
/**
 * Class Booking
 *
 * @package App
 */
class Booking extends Model
{

    //use SoftDeletes;
    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function customer()
    {
        /**
         * DO NOT MODIFY
         */
        return $this->belongsToMany('App\Customer','customer_booking','bok_id','cus_id')
            ->withPivot('bok_id', 'cus_id')
            ->withTimestamps();
    }

    /**
     * @return mixed
     */
    public function customers()
    {
        /**
         * DO NOT MODIFY
         */
        return $this->belongsToMany('App\Customer','customer_booking','bok_id','cus_id')
            ->withPivot('bok_id', 'cus_id')
            ->withTimestamps();
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function associate()
    {
        return $this->hasOne('App\Associate', 'aso_id', 'aso_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function ship()
    {
        return $this->hasOne('App\Ship', 'ship_id', 'ship_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function land()
    {
        return $this->hasOne('App\Land', 'land_id', 'land_id');
    }

    /**
     * @return mixed
     */
    public function images()
    {
        return $this->hasMany('App\Image','bok_id', 'bok_id');
    }

    /**
     * @return mixed
     */
    public function groups()
    {
        return $this->hasOne('App\Group', 'group_id', 'group_id');
    }

    protected $primaryKey = 'bok_id';

    /**
     * @var string
     */
    protected $connection = 'mysql3';

    /**
     * @var string
     */
    protected $table = 'booking';

    /**
     * @var string
     */
    protected $fillable = ['bok_id','ship_id','land_id' ,'bok_type','bok_selector' ,'bok_reservation','cus_id','aso_id','group_id','bok_ship','bok_date','bok_notes','created_by'];

}