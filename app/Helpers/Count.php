<?php
function numBookings($id)
{
    $count =  App\Booking::where('cus_id','=',$id )->count();
    return $count;
}


/**
 * @param $universal
 *
 * @return string
 */


function agent_count_dash()
{

}

/**
 * @return mixed
 */
function agents()
{

    $dash = App\Associate::where('aso_active', 1)->get();

    return $dash;

}

/**
 * @return mixed
 */
function countAgents()
{

    $dash = App\Associate::where('aso_active', 1)->count();

    return $dash;

}

/**
 * @return mixed
 */
function all_agents()
{
    switch (Auth::user()->associate_assignment) {
        case 'R':
            $a = App\Associate::all();
                //->select('aso_id','aso_selector','aso_fname','aso_lname');
            break;
        case 1:
            $a = App\Associate::where('aso_active', 1)
                //->select('aso_id','aso_selector','aso_fname','aso_lname')
                ->get();
            break;
        case 2:
            $ids = [1,3,4,5,6,7,8,9];

            $ids_ordered = implode(',', $ids);

            $a = App\Associate::where('aso_active', 1)
                ->orderByRaw(DB::raw("FIELD(aso_id, $ids_ordered)"))
                //->select('aso_id','aso_selector','aso_fname','aso_lname')
                ->get();
            break;
        case 3:
            $ids = [1, 2];

            $ids_ordered = implode(',', $ids);

            $a = App\Associate::where('aso_active', 1)
                ->orderByRaw(DB::raw("field(aso_id, $ids_ordered)"))
                //->select('aso_id','aso_selector','aso_fname','aso_lname')
                ->get();
            break;
        case 9:
            $ids = [8];

            $ids_ordered = implode(',', $ids);

            $a = App\Associate::where('aso_active', 1)
                ->orderByRaw(DB::raw("field(aso_id, $ids_ordered)"))
                //->select('aso_id','aso_selector','aso_fname','aso_lname')
                ->get();
            break;

        case 99:
            $a = App\Associate::where('aso_id', 99)
                //->select('aso_id','aso_fname','aso_lname')
                ->get();
            break;
        default:
            $a = App\Associate::where('aso_active', 1)
                //->select('aso_id','aso_selector','aso_fname','aso_lname')
                ->get();
            break;
    }
    return $a;
}

function all_agents_dashboard()
{
    switch (Auth::user()->associate_assignment) {
        case 'R':
            $a = App\Associate::all();
            break;
        case 1:
            $a = App\Associate::has('customers')
                ->where('aso_active', 1)->get();
            break;
        case 2:
            $ids = [1,3,4,5,6,7,8,9];

            $ids_ordered = implode(',', $ids);

            $a = App\Associate::has('customers')
                ->where('aso_active', 1)
                ->orderByRaw(DB::raw("FIELD(aso_id, $ids_ordered)"))
                ->get();
            break;
        case 3:
            $ids = [1, 2];

            $ids_ordered = implode(',', $ids);

            $a = App\Associate::where('aso_active', 1)
                ->orderByRaw(DB::raw("field(aso_id, $ids_ordered)"))
                ->get();
            break;
        case 99:
            $a = App\Associate::where('aso_id', 99)->get();
            break;
        default:
            $a = App\Associate::where('aso_active', 1)->get();
            break;
    }
    return $a;
}

/**
 * @return mixed
 */
function customer_count()
{
    /**
     * Excludes customers made under Jane Doe with id of 99 from the count
     */

    if (Auth::user()->id == 99)
    {
        return App\Customer::where('aso_id',99)->count();
    }
    else
    {

        return App\Customer::where('aso_id','<>',99)->count();
    }

}

/**
 * @return mixed
 */
function group_count()
{
    if (Auth::user()->id == 99)
    {
        /**
         * Includes groups only made under Jane Doe with aso_id of 99 on the count
         */

        return App\Group::where('aso_id',99)
            ->count();
    }
    else
    {
        /**
         * Excludes groups made under Jane Doe with aso_id of 99 from the count
         */

        return App\Group::where('aso_id','<>',99)
            ->where('group_active',1)
            ->count();
    }
}

/**
 * @return mixed
 */
function group_cancel_count()
{
    if (Auth::user()->id == 99) {
        /**
         * Includes groups only made under Jane Doe with aso_id of 99 on the count
         */

        $count = App\Group::where('aso_id', 99)
            ->where('group_active', '=', 'C')
            ->count();
    } else {
        $count = App\Group::where('aso_id','<>', 99)
            ->where('group_active', '=', 'C')
            ->count();
    }

    return $count;
}

function group_sailed_count()
{
    if (Auth::user()->id == 99) {
        /**
         * Includes groups only made under Jane Doe with aso_id of 99 on the count
         */

        $count = App\Group::where('aso_id', 99)
            ->where('group_active', '=', 'S')
            ->count();
    } else {
        $count = App\Group::where('aso_id','<>', 99)
            ->where('group_active', '=', 'S')
            ->count();
    }

    return $count;
}
