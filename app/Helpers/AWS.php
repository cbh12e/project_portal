<?php
/**
 * Created by PhpStorm.
 * User: hargcb
 * Date: 9/12/2015
 * Time: 1:42 PM
 */

/**
 * Amazon S3 Presigned Links
 * @param $file
 * @param $s3
 *
 * @return string
 */

function S3($file)
{
    /**
     * Creates a Pre-signed Link for Amazon S3 to Retrieve Files.
     */
    $s3 = App::make('aws')->createClient('s3');
    $command = $s3->getCommand('GetObject', [ 'Bucket' => env('AWS_BUCKET'), 'Key' => $file ]);
    $sdk = $s3->createPresignedRequest($command, '+10 minutes');
    $signedUrl = (string) $sdk->getUri();

    return $signedUrl;
}

/**
 * @return mixed
 */
function S3_Connect()
{
    $s3 = App::make('aws')->createClient('s3');
    return $s3;
}
