<?php
/**
 * Created by PhpStorm.
 * User: Casey Hargarther
 * Date: 2/6/2016
 * Time: 4:55 PM
 */

/**
 * Date Converter Helper
 * This converts the Unix Date to a readable date.
 * @param $date
 * @return bool|string
 */
function human_date($date)
{
    return date("m/d/Y", strtotime($date));
}

/**
 * @param $date
 * @return bool|string
 */
function json_date($date)
{
    return date("m-d-Y", strtotime($date));
}

/**
 * @param $date
 * @return bool|string
 */
function unix_date($date)
{
   return date("Y-m-d", strtotime($date));
}

/**
 * @param $date
 * @return bool|string
 */
function select_date($date)
{
    return date("m-d-y", strtotime($date));
}

/**
 * This helper returns a shorter date for use for mobile devices.
 *
 * @param $date
 *
 * @return bool|string
 */
function human_date_mobile($date)
{
    return date("m/d/y", strtotime($date));
}

/**
 * @param $date
 * @return bool|string
 */
function human_date2($date)
{
    return date("m/d/Y", strtotime($date));
}