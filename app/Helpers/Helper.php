<?php
/**
 * Created by PhpStorm.
 * User: hargcb
 * Date: 9/12/2015
 * Time: 1:46 PM
 */

use Cmgmyr\Messenger\Models\Thread;
use Cmgmyr\Messenger\Models\Message;
use Cmgmyr\Messenger\Models\Participant;

/**
 * Returns default string 'active' if route is active.
 *
 * @param $route
 * @param string $str
 * @return string
 */
function active($route, $str = 'active') {

    return call_user_func_array('Request::is', (array)$route) ? $str : '';

}

/**
 * @param $aId
 * @param $rId
 * @param $aName
 * @return string
 */
function associateSelector($aId, $rId, $aName)
{

    return '<option value="'. $aId .'"  '.$rId == $aId ? 'selected="selected"' : '' .'>'. $aName .'</option>';
}


/**
 * Return sizes readable by humans
 */
function human_filesize($bytes, $decimals = 2)
{
    $size = [' B', ' KB', ' MB', ' GB', ' TB', ' PB'];
    $factor = floor((strlen($bytes) - 1) / 3);

    return sprintf("%.{$decimals}f", $bytes / pow(1024, $factor)) .
    @$size[$factor];
}

/**
 * @return mixed
 */
function fileLimit()
{
  //  $max_upload = (int)(ini_get('upload_max_filesize'));
  //  $max_post = (int)(ini_get('post_max_size'));
  //  $memory_limit = (int)(ini_get('memory_limit'));

    $max_upload = (int)(ini_get('upload_max_filesize'));
    $max_post = (int)(ini_get('post_max_size'));
    $max_memory = (int)(ini_get('memory_limit'));
return $file_limit = min($max_upload, $max_post, $max_memory);
}

/**
 * @return mixed
 */
function totalLimit()
{
  //  $max_upload = (int)(ini_get('upload_max_filesize'));
  //  $max_post = (int)(ini_get('post_max_size'));
  //  $memory_limit = (int)(ini_get('memory_limit'));

    $max_upload = (int)(ini_get('upload_max_filesize'));
    $max_post = (int)(ini_get('post_max_size'));
    $max_memory = (int)(ini_get('memory_limit'));

return min($max_post, $max_memory);


}

/**
 * @return string
 */
function recaptica()
{
    $recaptica = "<script type='text/javascript' src='https://www.google.com/recaptcha/api.js'></script>";

    return $recaptica;
}

/**
 * @return string
 */
function bootBox()
{

    return '<script type="text/javascript" src="https://oss.maxcdn.com/bootbox/4.2.0/bootbox.min.js"></script>';

}

/**
 * @param $currentUserId
 * @return mixed
 */
function messages($currentUserId)
{
// All threads, ignore deleted/archived participants
//$threads = Thread::getAllLatest()->paginate(5);



// All threads that user is participating in
$threads = Thread::forUser($currentUserId)->latest('updated_at')->take(5)->get();

// All threads that user is participating in, with new messages
// $threads = Thread::forUserWithNewMessages($currentUserId)->latest('updated_at')->get();

return $threads ;
    }


/**
 * @return string
 */
function isTwoFactorEnabled()
{
    if (Auth::user()->google_2fa_secret == null)
    {

        return '<div class="alert alert-danger">' .
                    '<p> <strong><i class="fa fa-exclamation-circle"></i></strong>'.
                    ' Two Factor Authentication is not enabled for your account! Please go to your user profile to enable it.'.
                    '</p>'.
                '</div>';
    }

}

/**
 * @return mixed
 */
function geoIP()
{
   $location = GeoIP::getLocation();

    return $location;

}


/**
 * @return string
 */
function phpVersion1()
{
    // PHP_VERSION_ID is available as of PHP 5.2.7, if our
// version is lower than that, then emulate it
    if (!defined('PHP_VERSION_ID')) {
        $version = explode('.', PHP_VERSION);

       return define('PHP_VERSION_ID', ($version[0] * 10000 + $version[1] * 100 + $version[2]));
    }
    else{
        return phpversion();
    }
}

/**
 * @return string
 */
function encrypt()
{
    if (version_compare(PHP_VERSION, '7.0.0') >= 0) {

        return strtr(
            base64_encode(
                random_bytes(9)
            ),
            '+/',
            '-_'
        );
    }
    else{
        return str_random(9);
    }
}

/**
 * @return mixed
 */
function username()
{
    return auth()->user()->username;
}

function associateQuery()
{
    return DB::raw('CONCAT(aso_fname, " ", aso_lname) AS aso_name');
}

function customerFullNameDB()
{
   return DB::raw('CONCAT(cus_fname, " ", cus_lname) AS FullName');
}
