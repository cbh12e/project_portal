<?php
/**
 * Created by PhpStorm.
 * User: hargcb
 * Date: 9/12/2015
 * Time: 1:44 PM
 */

use Carbon\Carbon;

/**
 * @param $universal
 *
 * @return mixed
 */

function time_zone($universal)
{

    /**
     * Converts time from Universal Time Zone (UTC) to Eastern Time Zone
     */

    $dateTime = new DateTime($universal);
    $eastern = Carbon::createFromTimestamp($dateTime->getTimestamp(),'America/New_York');

    return $eastern->format('M j, Y g:i a T');
}


