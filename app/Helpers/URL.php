<?php
/**
 * Created by PhpStorm.
 * User: hargcb
 * Date: 9/12/2015
 * Time: 1:42 PM
 */

use Carbon\Carbon;

/**
 * @param $url
 *
 * @return mixed
 */
function expire($url){

    return UrlSigner::sign($url, Carbon::now()->addHours(2));
}

/**
 * @param $url
 *
 * @return mixed
 */
function twoFactorSigned($url){

    return UrlSigner::sign($url, Carbon::now()->addMinutes(5));
}


function exp_url($url)
{
    $sign = expire(url($url));
    return $sign;
}

function expire_url_param($url, $id)

{
    return expire(url($url, [$id]));
}

// global CDN link helper function
/**
 * @param $asset
 * @return string
 */
function cdn1($asset ){

    // Verify if KeyCDN URLs are present in the config file
    if( !Config::get('app.cdn') )
        return asset( $asset );

    // Get file name incl extension and CDN URLs
    $cdns = Config::get('app.cdn');
    $assetName = basename( $asset );

    // Remove query string
    $assetName = explode("?", $assetName);
    $assetName = $assetName[0];

    // Select the CDN URL based on the extension
    foreach( $cdns as $cdn => $types ) {
        if( preg_match('/^.*\.(' . $types . ')$/i', $assetName) )
            return cdnPath($cdn, $asset);
    }

    // In case of no match use the last in the array
    end($cdns);
    return cdnPath( key( $cdns ) , $asset);

}

/**
 * @param $cdn
 * @param $asset
 * @return string
 */
function cdnPath($cdn, $asset) {
    return  "//" . rtrim($cdn, "/") . "/" . ltrim( $asset, "/");
}

/**
 * Generates a token based URL for files attached to the CDN.
 *
 * @param $path = File Path Location
 * @return string Signed URL
 */
function cdn($path)
{
   if (App::environment('production'))
   {
    /**
     * Generates token for CDN Files
     */
    // Expiration in seconds (e.g. 60 seconds)
       $expire = time() + 180 ;

    // Generates a one time expiration token
       $md5 = md5($path.env('CDN_KEY').$expire, true);
       $md5 = base64_encode($md5);
       $md5 = strtr($md5, '+/', '-_');
       $md5 = str_replace('=', '', $md5);

       return '//'.env('CDN_URL')."{$path}?token={$md5}&expire={$expire}";
   }
    else
    {
        /**
         * Bypasses the CDN for local and stage environments.
         */
        return cdn1($path);
    }

}

/**
 * Typeahead URL String
 *
 * This helper returns the query url string used for Bloodhound in Twitter Typeahead.
 * @param $string
 * @return string
 */
function typeahead($string)
{
    return url().'/'.$string;
}

