<?php
/**
 * Is the mime type an image
 */
function is_image($mimeType)
{
    return starts_with($mimeType, 'image/');
}

/**
 * @param $mimeType
 *
 * @return mixed
 */
function is_msOffice($mimeType)
{
    return starts_with($mimeType, 'application/vnd.ms-office');
}

/**
 * Is the mime type a pdf
 */
function is_pdf($mimeType)
{
    return starts_with($mimeType, 'application/pdf');
}

/**
 * @param $mimeType
 *
 * @return mixed
 */
function is_text($mimeType)
{
    return starts_with($mimeType, 'text/');
}

/**
 * @param $mimeType
 *
 * @return mixed
 */
function is_message($mimeType)
{
    return starts_with($mimeType, 'msg');
}
