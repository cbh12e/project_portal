<?php namespace App;
/**
 * Created by PhpStorm.
 * User: hargcb
 * Date: 4/19/2015
 * Time: 10:41 PM
 */
use Illuminate\Database\Eloquent\Model;

/**
 * Class Booking
 *
 * @package App
 */
class Ship extends Model
{

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function cruiseLine()
    {
        return $this->hasOne('App\CruiseLine', 'cl_id', 'cl_id');
    }

    /**
     * @return mixed
     */
    public function bookings()
    {
        return $this->hasMany('App\Booking','ship_id', 'ship_id');
    }

    protected $primaryKey = 'ship_id';

    /**
     * @var string
     */
    protected $connection = 'mysql3';

    /**
     * @var string
     */
    protected $table = 'cruiseship';
    /**
     * @var string
     */

    protected $fillable = ['cl_id','ship_name','ship_year','ship_tonnage','ship_notes'];

}