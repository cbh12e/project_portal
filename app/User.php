<?php namespace App;

use Illuminate\Auth\Authenticatable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Auth\Passwords\CanResetPassword;
use Illuminate\Foundation\Auth\Access\Authorizable;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Contracts\Auth\Access\Authorizable as AuthorizableContract;
use Illuminate\Contracts\Auth\CanResetPassword as CanResetPasswordContract;
use DB;
//use Illuminate\Database\Eloquent\SoftDeletes;
/**
 * Class User
 * @package App
 */
class User extends Model implements AuthenticatableContract,  AuthorizableContract, CanResetPasswordContract {

	use Authenticatable, CanResetPassword, Authorizable;
	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'users';

    /**
     * This is the guarded attributes
     * @var string
     */
    protected $guarded = ['type'];

	/**
	 * The attributes that are mass assignable.
	 *
	 * @var array
	 */
	protected $fillable = ['name', 'email', 'password','username','confirmed' ,'confirmation_code','active',];

	/**
	 * The attributes excluded from the model's JSON form.
	 *
	 * @var array
	 */
	protected $hidden = ['password', 'remember_token','confirmation_code','google_f2a_secret','active','associate_assignment'];

	/**
     * Returns if a user has enabled two factor authentication.
     *
     * @return bool
     */
    public function getHasTwoFactorAttribute()
    {
        return trim($this->google_2fa_secret) !== '';
    }

	/**
	 * Get a users ID from their username for Duo authentication purposes
	 *
	 * @param $username
	 * @return mixed
	 */

	public static function getIdFromUsername($username)
	{
		$query = User::where('username', '=', $username)
			->first();
		return $query->id;
	}

	/**
	 * @param $username
	 * @return mixed
     */
	public static function twoFactorCheck($username)
	{
		$query = DB::table('users')
			->where('username', '=', $username)
			->first();
		return $query->id;

	}
}
