<?php namespace App;
/**
 * Created by PhpStorm.
 * User: hargcb
 * Date: 4/19/2015
 * Time: 10:41 PM
 */
use Illuminate\Database\Eloquent\Model;
use Kyslik\ColumnSortable\Sortable;

/**
 * Class Booking
 *
 * @package App
 */
class Group extends Model
{

    use Sortable;
    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function customer()
    {
        //return $this->hasOne('App\Customer','cus_id', 'cus_id');

        return $this->hasManyThrough('App\Booking', 'App\Customer','bok_id','cus_id');


    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function associate()
    {
        return $this->hasOne('App\Associate', 'aso_id', 'aso_id');
    }


/*    public function associateName()
    {
        return $this->belongsTo('App\Associate', 'aso_id', 'aso_id');
    }*/

    /**
     * @return mixed
     */
    public function images()
    {
        return $this->hasMany('App\Image','group_id', 'group_id');
    }

    /**
     * @return mixed
     */
    public function bookings()
    {
        return $this->hasMany('App\Booking','group_id', 'group_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function ship()
    {
        return $this->hasOne('App\Ship', 'ship_id', 'ship_id');
    }

    protected $primaryKey = 'group_id';

    /**
     * @var string
     */
    protected $connection = 'mysql3';

    /**
     * @var string
     */
    protected $table = 'group';
    /**
     * @var string
     */

    protected $sortable = ['group_number','group_name','group_ship','group_date'];

    protected $fillable = ['group_id','group_selector','group_number','group_name','aso_id','group_ship','group_date','group_notes','created_by'];

   // protected $hidden = ['group_active'];

}