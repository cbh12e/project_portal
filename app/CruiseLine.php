<?php namespace App;
/**
 * Created by PhpStorm.
 * User: hargcb
 * Date: 4/19/2015
 * Time: 10:41 PM
 */
use Illuminate\Database\Eloquent\Model;

/**
 * Class CruiseLine
 *
 * @package App
 */
class CruiseLine extends Model
{


    protected $primaryKey = 'cl_id';

    /**
     * @var string
     */
    protected $connection = 'mysql3';

    /**
     * @var string
     */
    protected $table = 'cruiseline';
    /**
     * @var string
     */

    protected $fillable = ['cl_name','cl_type','cl_logo','cl_notes','created_by'];

}