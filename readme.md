## Sea and Land Web Form Image Application

This web form allows travel agents to save client documents to a database and files to Amazon S3 to make it easier to show a specific client's documents.
Currently this build's codename is Tallahassee using version 1.

## Laravel PHP Framework

[![Build Status](https://travis-ci.org/laravel/framework.svg)](https://travis-ci.org/laravel/framework)
[![Total Downloads](https://poser.pugx.org/laravel/framework/downloads.svg)](https://packagist.org/packages/laravel/framework)
[![Latest Stable Version](https://poser.pugx.org/laravel/framework/v/stable.svg)](https://packagist.org/packages/laravel/framework)
[![Latest Unstable Version](https://poser.pugx.org/laravel/framework/v/unstable.svg)](https://packagist.org/packages/laravel/framework)
[![License](https://poser.pugx.org/laravel/framework/license.svg)](https://packagist.org/packages/laravel/framework)

Laravel is a web application framework with expressive, elegant syntax. We believe development must be an enjoyable, creative experience to be truly fulfilling. Laravel attempts to take the pain out of development by easing common tasks used in the majority of web projects, such as authentication, routing, sessions, queueing, and caching.

Laravel is accessible, yet powerful, providing powerful tools needed for large, robust applications. A superb inversion of control container, expressive migration system, and tightly integrated unit testing support give you the tools you need to build any application with which you are tasked.

## Official Documentation

Documentation for the framework can be found on the [Laravel website](http://laravel.com/docs).


## Symbolic Link Commands
In order for the CSS Links to work correctly in a production environment, you will have to create a symbolic link for the Bootstrap and AdminLTE folders. This is to update the CSS and JS of the stylesheet when there is a update to the AdminLTE and Bootstrap frameworks.

 ln -sfr vendor/almasaeed2010/adminlte/bootstrap    public/vendor/bootstrap
 ln -sfr vendor/almasaeed2010/adminlte/dist/dist    public/vendor/adminlte
 ln -sfr vendor/almasaeed2010/adminlte/plugins      public/vendor/plugins

## Contributing

Thank you for considering contributing to the Laravel framework! The contribution guide can be found in the [Laravel documentation](http://laravel.com/docs/contributions).

### License

The Laravel framework is open-sourced software licensed under the [MIT license](http://opensource.org/licenses/MIT)
