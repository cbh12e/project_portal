<script type="text/javascript">
    $(document).ready(function(){
        $("#landBased").css("display","none");
        $(".provider").click(function(){
            if ($('input[name=provider]:checked').val() == "Sea" )
            {
                $('#vacationType').empty().append("Sail");
                $("#sea").show();
                $("#landBased").hide();

            }
            else
            {
                $("#sea").hide();
                $("#landBased").show();
                $("#vacationType").empty().append("Vacation");
            }
        });
    });
</script>