<div class="modal fade" id="progressModal" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">File Upload Progress</h4>
            </div>
            <div class="modal-body">
                <div id="answer"></div>
                <div class="progress text-center">
                    <div id="progressbar" role="progressbar" class="progress-bar progress-bar-primary progress-bar-striped active">
                        <div id="percent">0%</div>
                    </div>
                    </div>

                <div class="modal-footer">
                    <div id="action"></div>
                </div><!-- /.modal-content -->
            </div><!-- /.modal-dialog -->
        </div>
    </div>
</div>


