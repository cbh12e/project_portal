@extends('app')
@section('title','Cruise Line Ship List')
@section('content')
    <section class="content-header">
        <h1><i class="fa fa-ship"></i> Cruise Line Ship List</h1>
        <ol class="breadcrumb">
            <li><a href="{{url('/')}}"><i class="fa fa-dashboard"></i> Home</a></li>
            <li><a href="{{asset('ship/')}}"> Cruise Line List</a></li>
            <li class="active"><i class="fa fa-ship"></i> Cruise Line Ship List</li>
        </ol>
    </section>
    <hr>
    <div class="container">
        <div class="col-md-11 col-xs-10">
            <a href="{{url('ship/create')}}" class="btn btn-primary text-right">Create New Ship</a>
            <hr>
            <table class="table table-striped table-bordered">
                <thead>
                <tr>
                    <th> Ship Status</th>
                    <th> Ship Name</th>
                    <th> Cruise Line</th>
                    <th> Ship Year</th>
                    <th> Ship Tonnage</th>
                </tr>
                </thead>
                <tbody>
                @foreach($ship as $record)
                    <tr>
                        <td>{{$record->ship_active }}</td>
                        <td>{{$record->ship_name }}</td>
                        <td>{{$record->cruiseLine->cl_name }}</td>
                        <td>{{$record->ship_year }}</td>
                        <td>{{$record->ship_tonnage }}</td>
                    </tr>
                @endforeach
                </tbody>
            </table>
            <div class="text-right">
                {!! $ship->render() !!}
            </div>
        </div>
    </div>
    @endsection
    @section('javascript')
    <script type="text/javascript">
        bootbox.confirm("Are you sure?", function(result) {
            Example.show("Confirm result: "+result);
        });
    </script>
@endsection