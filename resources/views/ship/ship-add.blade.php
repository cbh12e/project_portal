@extends('app')
@section('title')
   Create New Cruise Ship
@endsection
@section('content')

    <section class="content-header">
        <h1><i class="fa fa-search"></i>Create New Ship</h1>
        <ol class="breadcrumb">
            <li><a href="{{url('/')}}"><i class="fa fa-dashboard"></i> Home</a></li>
            <li><a href="{{url('ships')}}">Cruise Line List</a></li>
            <li class="active">Create New Ship</li>
        </ol>
    </section>

    <hr>
    <div class="container">
        <div class="col-md-12">
                <small> All fields with an asterisk * are required. </small>
                <form class="form-horizontal" method="post">
                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                    <fieldset>
                        <div class="form-group">
                            <label class="col-md-2 control-label" for="cruiseLine"> Cruise Line: * </label>
                            <div class="col-md-5">
                                <select name="a" id="cruiseLine" class="form-control">
                                    @foreach($cruiseLine as $cruiseLines)
                                        <option value="{{$cruiseLines->cl_id}}">{{$cruiseLines->cl_name}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <!-- Name of Ship-->
                        <div class="form-group">
                            <label class="col-md-2 control-label" for="ship">Ship: *</label>
                            <div class="col-md-6">
                                <div class="input-group">
                                    <span class="input-group-addon"><i class="fa fa-ship"></i></span>
                                    <input type="text" name="shipName" id="shipName" class="form-control" placeholder="Ship Name"  @if(count(old('shipName')) > 0)value="{{old('shipName')}}" @endif  autocomplete="off">
                                </div>
                            </div>
                        </div>
                    </fieldset>
                    {{-- Notes --}}
                    <div class="form-group">
                        <label class="col-md-2 control-label" for="notes">Ship Notes:</label>
                        <div class="col-md-7">
                            <div class="input-group">
                                <span class="input-group-addon"><i class="fa fa-sticky-note-o"></i></span>
                                @if(old('notes'))
                                    <textarea name="notes" id="notes" class="form-control" rows="5">{{old('notes')}}</textarea>
                                @else
                                    <textarea name="notes" id="notes" class="form-control" rows="5"></textarea>
                                @endif
                            </div>
                        </div>
                    </div>
                    <!-- Submit Button -->
                    <div class="form-group">
                        <div class="col-md-8">
                            <button type="submit" class="btn btn-primary" id="submit1"><i class="fa fa-plus-circle"></i> Submit</button>
                            <button type="reset" class="btn btn-warning">Reset</button>
                        </div>
                    </div>
                </form>
        </div>
    </div>
    @endsection