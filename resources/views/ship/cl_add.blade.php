@extends('app')
@section('title')
    Create New Cruise Line
@endsection
@section('content')

    <section class="content-header">
        <h1><i class="fa fa-search"></i>Create Vacation Provider</h1>
        <ol class="breadcrumb">
            <li><a href="{{url('/')}}"><i class="fa fa-dashboard"></i> Home</a></li>
            <li><a href="{{url('ships')}}">Cruise Line List</a></li>
            <li class="active">Create Vacation Provider</li>

        </ol>
    </section>

    <hr>
    <div class="container">
        <div class="col-md-12">
            <small> All fields with an asterisk * are required. </small>
            <form class="form-horizontal" method="post">

                    {{-- CSRF TOKEN--}}
                    <input type="hidden" name="_token" value="{{ csrf_token() }}">

                <div class="form-group">
                    <label class="col-md-2 control-label" for="provider">Provider Type: *</label>
                    <div class="col-md-8">
                        <div class="input-group">
                            <input type="radio" name="provider" value="Sea" class="provider minimal-blue" checked /> Cruise Line
                            <input type="radio" name="provider" value="Land" class="provider minimal-blue" /> Land Provider
                        </div>
                    </div>
                </div>


                    <div id = "sea">
                        <fieldset>
                            <legend>Sea Based Vacation</legend>
                    <!-- Name of Ship-->
                    <div class="form-group">
                        <label class="col-md-2 control-label" for="ship">Cruise Line Name: *</label>
                        <div class="col-md-8">
                            <div class="input-group">
                                <span class="input-group-addon"><i class="fa fa-ship"></i></span>
                                <input type="text" name="clName" id="clName" class="form-control"  @if(count(old('clName')) > 0)value="{{old('clName')}}" @endif  autocomplete="off">
                            </div>
                        </div>
                    </div>
                        </fieldset>
                    </div>

                    <div id="land">

                        <fieldset>
                            <legend> Land Based Vacation</legend>
                        <!-- Name of Ship-->
                        <div class="form-group">
                            <label class="col-md-2 control-label" for="landName">Vacation Provider Name: *</label>
                            <div class="col-md-8">
                                <div class="input-group">
                                    <span class="input-group-addon"><i class="fa fa-ship"></i></span>
                                    <input type="text" name="landName" id="landName" class="form-control"  @if(count(old('landName')) > 0)value="{{old('landName')}}" @endif  autocomplete="off">
                                </div>
                            </div>
                        </div>
                        </fieldset>
                    </div>

                {{-- Notes --}}
                            <div class="form-group">
                                <label class="col-md-2 control-label" for="notes">Provider Notes:</label>
                                <div class="col-md-8">
                                    <div class="input-group">
                                        <span class="input-group-addon"><i class="fa fa-sticky-note-o"></i></span>
                                        @if(old('notes'))
                                            <textarea name="notes" id="notes" class="form-control" rows="5">{{old('notes')}}</textarea>
                                        @else
                                            <textarea name="notes" id="notes" class="form-control" rows="5"></textarea>
                                        @endif
                                    </div>
                                </div>
                            </div>

                <!-- Submit Button -->
                <div class="form-group">
                    <div class="col-md-8">

                        <button type="button" class="btn btn-default pull-left" data-dismiss="modal" >Close</button>
                        <button type="reset" class="btn btn-warning">Reset</button>
                        <button type="submit" class="btn btn-primary" id="submit1"><i class="fa fa-plus-circle"></i> Submit</button>
                    </div>
                </div>
            </form>
            <!-- /.modal-content -->
        </div>
    </div>
@endsection

@section('javascript')
    <script type="text/javascript">
        $(document).ready(function(){
            $("#land").css("display","none");
            $(".provider").click(function(){
                if ($('input[name=provider]:checked').val() == "Sea" ) {
               // if ($('input[name=provider]:checked').val() == "sea" ) {
                  //  $("#land").slideDown("fast"); //Slide Down Effect
                    $("#land").hide();
                    $("#sea").show();

                } else{
                    //$("#sea").slideUp("fast");  //Slide Up Effect
                   // $("#newShip").slideUp("fast");  //Slide Up Effect
                    $("#sea").hide();
                    $("#land").show();
                }
            });
        });
    </script>
    @endsection

@section('icheck')
    @overwrite