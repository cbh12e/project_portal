<!-- Modal -->
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">Update Group Information</h4>
            </div>

            <div class="modal-body">
                <form class="form-horizontal" method="post" action="{{url('groups/update')}}" id="update">
                    <fieldset>

                        <!-- Group ID Modal Update-->
                        <input type="hidden" name="group" id="group" value="@if( old('group') == true){{old('group')}}@else{{$result->group_id}}@endif">

                        <input type="hidden" name="_token" value="{{ csrf_token() }}">

                        <!-- Group Number Modal Update-->
                        <div class="form-group">
                            <label class="col-md-3 control-label" for="name"> Group Number:</label>
                            <div class="col-md-5">
                                <input type="text" class="form-control" name="number" id="number"  autocomplete="off" value="@if( old('number') == true){{old('number')}}@else{{$result->group_number}}@endif"/>
                            </div>
                        </div>

                        <!-- Group Name Modal Update-->
                        <div class="form-group">
                            <label class="col-md-3 control-label" for="name"> Group Name:</label>
                            <div class="col-md-8">
                                <input type="text" class="form-control" name="name" id="name"  autocomplete="off" value="@if( old('name') == true){{old('name')}}@else{{$result->group_name}}@endif"/>
                            </div>
                        </div>

                        <!-- Ship Name Modal Update-->
                        <div class="form-group">
                            <label class="col-md-3 control-label" for="ship"> Ship Name:</label>
                            <div class="col-md-8">
                                <div class="input-group">
                                    <span class="input-group-addon"><i class="fa fa-ship"></i></span>
                                    <input type="text" class="form-control" name="ship" id="ship" autocomplete="off" value="@if( old('ship') == true){{old('ship')}}@else{{$result->group_ship}}@endif"/>
                                </div>
                            </div>
                        </div>

                        <!-- Sail Date Modal Update-->
                        <div class="form-group">
                            <label class="col-md-3 control-label" for="date"> Sail Date</label>
                            <div class="col-md-4">
                                <div class="input-group">
                                    <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                                    <input type="text" class="form-control" name="date" id="bDate" autocomplete="off" value="@if( old('date') == true){{old('date')}}@else{{date("m/d/Y", strtotime($result->group_date))}}@endif"/>
                                </div>
                            </div>
                        </div>


                        <!-- Notes Modal Update -->
                        <div class="form-group">
                            <label class="col-md-3 control-label" for="notes">Notes: </label>
                            <div class="col-md-7">
                                @if(count(old('notes'))==1)
                                    <textarea name="notes" id="notes" class="form-control" rows="5">{{old('notes')}}</textarea>
                                @elseif(count($result->group_notes) == 1)
                                    <textarea name="notes" id="notes" class="form-control" rows="5">{{$result->group_notes}}</textarea>
                                @else
                                    <textarea name="notes" id="notes" class="form-control" rows="5"></textarea>
                                @endif
                            </div>
                        </div>
                    </fieldset>

                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                        <button type="submit" class="btn btn-primary" id="209A">Save changes</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>