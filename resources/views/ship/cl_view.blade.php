@extends('app')
@section('title','Cruise Line List')
@section('content')
    <section class="content-header">
        <h1><i class="fa fa-search"></i> Cruise Line List</h1>
        <ol class="breadcrumb">
            <li><a href="{{url('/')}}"><i class="fa fa-dashboard"></i> Home</a></li>
            <li class="active">Cruise Line List</li>
        </ol>
    </section>
    <hr>
    <div class="container">
        <div class="col-md-11 col-xs-10">
            <table class="table table-striped table-bordered">
                <thead>
                <tr>
                    <th> Logo </th>
                    <th> Cruise Line Name</th>
                    <th></th>
                </tr>
                </thead>
                <tbody>
                @foreach($cruiseLine as $record)
                    <tr>
                        <td><img class="img-responsive" src="{{asset('/logos/'.$record->cl_logo)}}" alt="{{$record->cl_name}}"/></td>
                        <td>{{$record->cl_name }}</td>
                        <td><a href="{{ expire(action('ShipController@viewShips', ['id' => $record->cl_id])) }}" class="btn btn-info"> View </a> </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
            <div class="text-right">
                {!! $cruiseLine->render() !!}
            </div>
        </div>
    </div>
@endsection