@extends('app')
@section('title','Associate Search')
@section('content')
    <section class="content-header">
        <h1><i class="fa fa-search"></i> Search Results by Associate <span class="badge">{{$count}}</span></h1>
        <ol class="breadcrumb">
            <li><a href="{{url('/')}}"><i class="fa fa-dashboard"></i> Home</a></li>
            <li class="active"><i class="fa fa-search"></i> Search Results <span class="badge">{{$count}}</span></li>
        </ol>
    </section>

    <hr>
    <div class="container">
        <div class="col-md-11 col-xs-10">
    <table class="table table-striped table-bordered" id="agent">
        <thead>
        <tr>
            <th> Associate</th>
            <th>@sortablelink('cus_lname','Customer Name')</th>
            <th> Action</th>
        </tr>
        </thead>

        <tbody>

        @foreach($agent as $record)
            <tr>
                <td>{{$record->associate->AssociateName }}</td>
                <td>{{$record->CustomerFullName }}</td>
                <td><a href="{{ url('search/customer',[$record->cus_selector])}}" class="btn btn-info"> View</a></td>
            </tr>
        @endforeach
        </tbody>
    </table>
            <div class="text-right">
                {!! $agent->appends(\Input::except('page'))->render() !!}
            </div>
    </div></div>
@endsection