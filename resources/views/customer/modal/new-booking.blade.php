<div class="modal fade" id="bookingModal" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Add New Booking for <small>{{$menu->cus_fname}} {{$menu->cus_lname}}</small></h4>
            </div>
            <div class="modal-body"> 
			<small> All fields with an asterisk * are required. Do not store credit card information since it violates PCI compliance standards.</small>
                <form class="form-horizontal" method="post" action="{{asset('search/customer/add')}}" role="form">
                    <fieldset>

                        <legend> Booking Information</legend>
                        {{-- CSRF TOKEN--}}
                        <input type="hidden" name="_token" value="{{ csrf_token() }}">

                        <!-- Customer ID -->
                        <input type="hidden" name="id" id="id" value="{{ $menu->cus }}">


                        <!-- Booking Type -->
                        <div class="form-group">
                            <label class="col-md-3 control-label" for="provider">Provider Type: *</label>
                            <div class="col-md-8">
                                <div class="input-group">
                                    <input type="radio" name="provider" value="Sea" class="provider iradio_minimal-red" checked="checked" id="850"/>
                                    <label for="850">&nbsp;<i class="fa fa-ship"></i> Cruise</label>&nbsp;

                                    <input type="radio" name="provider" value="Land" class="provider iradio_minimal-red" id="644"/>
                                    <label for="644">&nbsp;<i class="fa fa-fort-awesome"></i> &nbsp;Land </label>

                                </div>
                            </div>
                        </div>

                        <!-- Associate ID -->
                        <input type="hidden" name="associate" id="associate" value="{{ $menu->aso_id }}" >

                        <!-- Reservation Number-->
                        <div class="form-group">
                            <label class="col-md-3 control-label" for="ship">Reservation Number: *</label>
                            <div class="col-md-8">
                                <div class="input-group">
                                    <span class="input-group-addon"><i class="fa fa-list-ol"></i></span>
                                    <input type="text" name="reservation" id="reservation" class="form-control" placeholder="Reservation Number" value="{{old('ship')}}" autocomplete="off">
                                </div>
                            </div>
                        </div>

                        <div id ="sea">
                        <!-- Name of Ship-->
                        <div class="form-group">
                            <label class="col-md-3 control-label" for="ship">Ship: *</label>
                            <div class="col-md-8">
                                <div class="input-group">
                                    <span class="input-group-addon"><i class="fa fa-ship"></i></span>
                                    <input type="text" name="ship" id="ship" class="form-control" placeholder="Ship Name" value="{{old('ship')}}" title="Enter the Name of the Cruise Ship" autocomplete="off">
                                </div>
                            </div>
                        </div>
                        </div>

                        <div id ="landBased">
                        <!-- Name of Ship-->
                        <div class="form-group">
                            <label class="col-md-3 control-label" for="land">Land: *</label>
                            <div class="col-md-8">
                                <div class="input-group">
                                    <span class="input-group-addon"><i class="fa fa-hotel"></i></span>
                                    <input type="text" name="land" id="landT" class="form-control" placeholder="Vacation Name" value="{{old('ship')}}"  autocomplete="off">
                                </div>
                            </div>
                        </div>
                        </div>

                        <!-- Date of Sailing -->
                        <div class="form-group">
                            <label class="col-md-3 control-label" for="date"> <span id ="vacationType">Sail</span> Date: *</label>
                            <div class="col-md-4">
                                <div class="input-group">
                                    <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                                    <input type="text" name="date" id="bDate" class="form-control" value="{{old('date')}}" placeholder="Date" autocomplete="off" >
                                </div>
                            </div>
                        </div>
            </fieldset>

                    {{-- Notes --}}
                    <div class="form-group">
                        <label class="col-md-3 control-label" for="notes">Notes:</label>
                        <div class="col-md-8">
                            <div class="input-group">
                                <span class="input-group-addon"><i class="fa fa-sticky-note-o"></i></span>
                            @if(old('notes'))
                                <textarea name="notes" id="notes" class="form-control" rows="5">{{old('notes')}}</textarea>
                            @else
                                <textarea name="notes" id="notes" class="form-control" rows="5"></textarea>
                            @endif
                            </div>
                        </div>
                    </div>

                    <!-- Submit Button -->
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default pull-left" data-dismiss="modal" >Close</button>
                        <button type="reset" class="btn btn-warning">Reset</button>
                        <button type="submit" class="btn btn-primary" id="submit5"><i class="fa fa-plus-circle"></i> Submit</button>
                    </div>
                </form>
                <!-- /.modal-content -->
            </div><!-- /.modal-dialog -->
        </div>
    </div>
</div>