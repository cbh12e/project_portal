<div class="modal" id="editModal" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content ">
            <div class="modal-header">

                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" onclick='UpdateRequest();'>Edit Customer Information </h4>
            </div>


            <form class="form-horizontal" method="post" name="update" role="form">
                <fieldset>

                    <div class="modal-body">
                        <!-- CSRF TOKEN -->
                        <input type="hidden" name="_token" value="{{ csrf_token() }}">

                        <!-- Customer ID EDIT-->
                        <input type="hidden" name="customer" id="customer" value="{{$record->cus}}">

                        {{-- Associate --}}
                        <div class="form-group">
                            <label class="col-md-4 control-label" for="associate">Associate Name: *</label>
                            <div class="col-md-7">
                                <div class="input-group">
                                    <span class="input-group-addon"><i class="fa fa-users"></i></span>
                                    <select name="associate" id="associate" class="form-control selectpicker">
                                        @if (all_agents())
                                            @foreach(all_agents() as $associate)
                                                <option value="{{ $associate->aso_id }}" {{ $record->aso_id == $associate->aso_id ? 'selected="selected"' : '' }}>{{ $associate->AssociateName }}</option>
                                            @endforeach
                                            @endif
                                    </select>
                                </div>
                            </div>
                        </div>

                        <!-- First Name EDIT-->
                        <div class="form-group">
                            <label class="col-md-4 control-label" for="firstName">Customer First Name: *</label>
                            <div class="col-md-7">
                                <div class="input-group">
                                    <span class="input-group-addon"><i class='fa fa-user'></i></span>
                                    <input type="text" name="firstName" id="firstName1" value="@if( old('firstName') == true){{old('firstName')}}@else{{$record->cus_fname}}@endif" placeholder="First Name" class="form-control input-md" autocomplete="off">
                                </div>
                            </div>
                        </div>

                        <!-- Last Name EDIT-->
                        <div class="form-group">
                            <label class="col-md-4 control-label" for="lastName">Customer Last Name: *</label>
                            <div class="col-md-7">
                                <div class="input-group">
                                    <span class="input-group-addon"><i class='fa fa-user'></i></span>
                                    <input type="text" name="lastName" id="lastName1" value="@if( old('lastName') == true){{old('lastName')}}@else{{$record->cus_lname}}@endif"  placeholder="Last Name" class="form-control input-md"  autocomplete="off">
                                </div>
                            </div>
                        </div>

                        <!-- Notes EDIT-->
                        <div class="form-group">
                            <label class="col-md-4 control-label" for="notesEdit">Customer Notes: </label>
                            <div class="col-md-7">
                                @if(count($record->cus_notes)== 1)
                                    <textarea name="notesEdit" id="notesEdit" class="form-control" rows="5">{{$record->cus_notes}}</textarea>
                                @else
                                    <textarea name="notesEdit" id="notesEdit" class="form-control" rows="5"></textarea>
                                @endif
                            </div>
                        </div>

                        <!-- EDIT Submit Button-->
                        <div class="modal-footer">
                            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                            <button type="reset" class="btn btn-warning">Reset</button>
                            <button type="button" id="submit4" class="btn btn-primary" name="submit4"> Save changes</button>
                        </div>
                    </div>
                </fieldset>
            </form>
        </div>
    </div>
</div>
