<div class="modal fade" id="customerDel" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Delete Customer </h4>
            </div>
            <form method="post" action="{{action('DeleteController@customerDelete')}}" role="form">
            <div class="modal-body">
                <p>Are you sure you want to delete this customer? This action cannot be undone.</p>
                <input type="hidden" name="delete"  id="customer" value="{{$menu->cus}}"/>
                <input type="hidden" name="_token" value="{{ csrf_token() }}"/>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-primary" data-dismiss="modal"> Close</button>
                <button type="submit" class="btn btn-danger" id="submitDel"><i class="fa fa-times-circle"></i> Delete</button>
            </div>
            </form>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->
