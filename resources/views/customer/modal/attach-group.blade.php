<div class="modal fade" id="groupModal" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Add Customer to an existing Group Booking</h4>
            </div>
            <div class="modal-body">

                <p> Enter the provided group number given to you by the Cruise Line. You can either type in the name of the ship or the group to get it.</p>
                <form class="form-horizontal" method="post" action="{{asset('search/customer/group/add')}}" role="form">
                    <fieldset>
                        <!-- CSRF Token -->
                        <input type="hidden" name="_token" value="{{ csrf_token() }}">

                        <!-- Customer ID -->
                        <input type="hidden" name="id" id="id" value="{{ $menu->cust }}" ><br />

                        <!-- Group ID-->
                        <div class="form-group">
                            <label class="col-md-3 control-label" for="group">Group Number:</label>
                            <div class="col-md-8">
                                <div class="input-group">
                                    <span class="input-group-addon"><i class="fa fa-pencil"></i></span>
                                    <input type="text" name="group" id="group" class="form-control" placeholder="Group Number" value="{{old('group')}}" autocomplete="off" >
                                </div>
                            </div>
                        </div>

                        <!-- Booking Reservation-->
                        <div class="form-group">
                            <label class="col-md-3 control-label" for="group">Reservation Number:</label>
                            <div class="col-md-8">
                                <div class="input-group">
                                    <span class="input-group-addon"><i class="fa fa-list-ol"></i></span>
                                    <input type="text" name="reservation" id="reservation" class="form-control" placeholder="Reservation Number" value="{{old('reservation')}}" autocomplete="off" >
                                </div>
                            </div>
                        </div>
                    </fieldset>

                    <!-- Submit Button -->
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Close</button>
                        <button type="submit" class="btn btn-primary" id="submit"><i class="fa fa-plus-circle"></i> Add </button>
                    </div>
                </form>
                <!-- /.modal-content -->
            </div><!-- /.modal-dialog -->
        </div>
    </div>
</div>