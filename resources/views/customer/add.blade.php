@extends('app')
@section('title','Create Customer')
@section('errorBar')@overwrite
@section('content')
        {{-- Form Name --}}
    <section class="content-header">
        <h1><i class="fa fa-user"></i> Create Customer <small> Fields with an asterisk <strong>*</strong> are required. <br />
                    Fields with <strong>**</strong> are required only when one is filled, but is optional if neither are filled.</small></h1>
        <ol class="breadcrumb">
            <li><a href="{{url('/')}}"><i class="fa fa-dashboard"></i> Home</a></li>
            <li class="active"><i class="fa fa-user"></i> Create Customer</li>
        </ol>
    </section>
    <br />

        <form class="form-horizontal" method="post" role="form" id="customerForm">
        <fieldset>
            <legend> Customer Information</legend>

            {{-- CSRF Token --}}
            <input type="hidden" name="_token" value="{{ csrf_token() }}">

            {{-- Associate --}}
                <div class="form-group {{ $errors->has('associate') ? ' has-error' : '' }}">
                    <label class="col-md-3 control-label" for="associate">Associate: *</label>
                    <div class="col-md-4">
                        <div class="input-group">
                            <span class="input-group-addon"><i class="fa fa-users"></i></span>
                            <select name="associate" id="associate" class="form-control selectpicker">
                                @foreach(all_agents() as $row)
                                    <option value="{{$row->aso_id}}">{{$row->AssociateName}}</option>
                                @endforeach
                            </select>
                            @if ($errors->has('associate'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('associate') }}</strong>
                                </span>
                            @endif
                        </div>
                    </div>
                </div>

                {{-- First Name --}}
                <div class="form-group {{ $errors->has('firstName') ? ' has-error' : '' }}">
                <label class="col-md-3 control-label" for="firstName">Customer First Name: *</label>
                <div class="col-md-4">
                    <div class="input-group">
                        <span class="input-group-addon"><i class='fa fa-user'></i></span>
                        <input type="text" name="firstName" id="firstName"  placeholder="First Name" value="{{old('firstName')}}" class="form-control input-md" autocomplete="off" >
                    </div>
                    @if ($errors->has('firstName'))
                        <span class="help-block">
                            <strong>{{ $errors->first('firstName') }}</strong>
                        </span>
                    @endif
                </div>
            </div>

            {{-- Last Name --}}
            <div class="form-group {{ $errors->has('lastName') ? ' has-error' : '' }}">
                <label class="col-md-3 control-label" for="lastName">Customer Last Name: *</label>
                <div class="col-md-4">
                    <div class="input-group">
                        <span class="input-group-addon"><i class='fa fa-user'></i></span>
                        <input type="text" name="lastName" id="lastName" placeholder="Last Name" value="{{old('lastName')}}" class="form-control input-md"  autocomplete="off">
                    </div>
                    @if ($errors->has('lastName'))
                        <span class="help-block">
                            <strong>{{ $errors->first('lastName') }}</strong>
                        </span>
                    @endif
                </div>
            </div>

            {{-- Notes --}}
            <div class="form-group" {{ $errors->has('notes') ? ' has-error' : '' }}>
                <label class="col-md-3 control-label" for="notes">Customer Notes:</label>
                <div class="col-md-5">
                    @if(old('notes'))
                        <textarea name="notes" id="notes" class="form-control" rows="5">{{old('notes')}}</textarea>
                    @else
                        <textarea name="notes" id="notes" class="form-control" rows="5"></textarea>
                    @endif
                </div>
                @if ($errors->has('notes'))
                    <span class="help-block">
                            <strong>{{ $errors->first('notes') }}</strong>
                        </span>
                @endif
            </div>
        </fieldset>

        <fieldset>
            <legend> Booking Information <small> (Use for individual bookings only. Skip this step for group bookings)</small></legend>

            <div class="form-group">
                <label class="col-md-3 control-label" for="provider">Booking Type: **</label>
                <div class="col-md-8">
                    <div class="input-group">
                        <input type="radio" name="provider" value="Sea" class="provider iradio_minimal-red" checked="checked" id="850"/>
                        <label for="850">&nbsp;<i class="fa fa-ship"></i> Cruise</label>&nbsp;

                        <input type="radio" name="provider" value="Land" class="provider iradio_minimal-red" id="644"/>
                        <label for="644">&nbsp;<i class="fa fa-fort-awesome"></i> &nbsp;Land </label>
                    </div>
                </div>
            </div>

            {{-- Reservation Number --}}
            <div class="form-group {{ $errors->has('reservation') ? ' has-error' : '' }}">
                <label class="col-md-3 control-label" for="reservation">Reservation Number: **</label>
                <div class="col-md-3">
                    <div class="input-group">
                        <span class="input-group-addon"><i class="fa fa-list-ol"></i></span>
                        <input type="text" name="reservation" id="reservation" class="form-control" placeholder="Reservation Number" value="{{old('reservation')}}" autocomplete="off">
                    </div>
                    @if ($errors->has('reservation'))
                        <span class="help-block">
                            <strong>{{ $errors->first('reservation') }}</strong>
                        </span>
                    @endif
                </div>
            </div>

            <div id="sea">
            {{-- Name of Ship --}}
            <div class="form-group {{ $errors->has('ship') ? ' has-error' : '' }}">
                <label class="col-md-3 control-label" for="ship">Ship: **</label>
                <div class="col-md-4">
                    <div class="input-group">
                        <span class="input-group-addon"><i class="fa fa-ship"></i></span>
                        <input type="text" name="ship" id="ship" class="form-control" placeholder="Ship Name" value="{{old('ship')}}" autocomplete="off">
                    </div>
                    @if ($errors->has('ship'))
                        <span class="help-block">
                            <strong>{{ $errors->first('ship') }}</strong>
                        </span>
                    @endif
                </div>
            </div>
                </div>

            <div id="landBased">
            {{-- Name of Vacation Package --}}
            <div class="form-group {{ $errors->has('land') ? ' has-error' : '' }}">
                <label class="col-md-3 control-label" for="land">Land Provider: **</label>
                <div class="col-md-4">
                    <div class="input-group">
                        <span class="input-group-addon"><i class="fa fa-hotel"></i></span>
                        <input type="text" name="land" id="landT" class="form-control" placeholder="Land Provider" value="{{old('land')}}" autocomplete="off">
                    </div>
                    @if ($errors->has('land'))
                        <span class="help-block">
                            <strong>{{ $errors->first('land') }}</strong>
                        </span>
                    @endif
                </div>
            </div>
                </div>

            {{-- Date of Sailing --}}
            <div class="form-group {{ $errors->has('date') ? ' has-error' : '' }}">
                <label class="col-md-3 control-label" for="date"><span id="vacationType">Sail</span> Date: **</label>
                <div class="col-md-2">
                    <div class="input-group">
                        <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                        <input type="text" name="date" id="bDate" class="form-control" value="{{old('date')}}" placeholder="Date" autocomplete="off">
                    </div>
                    @if ($errors->has('date'))
                        <span class="help-block">
                            <strong>{{ $errors->first('date') }}</strong>
                        </span>
                    @endif
                </div>
            </div>


            {{-- Notes --}}
            <div class="form-group {{ $errors->has('notesB') ? ' has-error' : '' }}">
                <label class="col-md-3 control-label" for="notesB">Booking Notes: </label>
                <div class="col-md-5">
                    @if(old('notesB'))
                        <textarea name="notesB" id="notesB" class="form-control" rows="5">{{old('notesB')}}</textarea>
                    @else
                        <textarea name="notesB" id="notesB" class="form-control" rows="5"></textarea>
                    @endif
                        @if ($errors->has('notesB'))
                            <span class="help-block">
                            <strong>{{ $errors->first('notesB') }}</strong>
                        </span>
                        @endif
                </div>
            </div>
        </fieldset>
        <fieldset>
            {{-- Submit Button --}}
            <div class="form-group">
                <label class="col-md-4 control-label" for="submit"></label>
                <div class="col-md-4">
                    <button type="submit" name="submit" id="submit" class="btn btn-primary"><i class="fa fa-check"></i> Submit</button>
                    <button type="reset" class="btn btn-default"><i class="fa fa-times"></i> Reset </button>
                </div>
            </div>
        </fieldset>
    </form>
@endsection

@section('javascript')
    @include('layouts.ship-typeahead')

    @include('layouts._datepicker')

    @include('layouts.land-sea')
@endsection
@section('icheck') @overwrite