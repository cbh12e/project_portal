@extends('app')
@section('title')
Search Results
@endsection
@section('css')
    @include('layouts._datatables-css')
@endsection
@section('content')
    @if(Session::has('flash_message'))
        <div class="alert alert-success">{{Session::get('flash_message')}}</div>
    @endif

    <div class="container">
        <div class="col-md-10 col-xs-11 col-sm-11 col-lg-10 ">
            <section class="content-header">
                <h1><i class="fa fa-search"></i> Search Results</h1>
                <ol class="breadcrumb">
                    <li><a href="{{url('/')}}"><i class="fa fa-dashboard"></i> Home</a></li>
                    <li class="active">Search Results</li>
                </ol>
            </section>
            <hr>

        <!-- View for Desktops and Tablets -->

            <table class="table table-striped table-bordered" id="manager">
                <thead>
                <tr>@if(session('search') === 'all')
                        <th> @sortablelink ('aso_id','Associate Name')</th>
                        <th> @sortablelink ('cus_lname','Customer Name')</th>
                        <th> Action</th>
                        @else
                    <th> Associate Name</th>
                    <th> Customer Name</th>
                    <th> Action</th>
                        @endif
                </tr>
                </thead>
                <tbody>
                @foreach($search as $record)
                    <tr>
                        <td>{{$record->associate->AssociateName }}</td>
                        <td>{{$record->CustomerFullName }} </td>
                        <td><a href="{{ url('search/customer',[$record->cus_selector])}}" class="btn btn-info"> View</a></td>
                    </tr>
                @endforeach
                </tbody>
            </table>
            @if(session('search') === 'all')
                <div class="text-right">
                    {!! $search->appends(\Input::except('page'))->render() !!}
                </div>
                @endif
            </div>
            </div>
    @endsection

@section('javascript')
    @if(session('search') <> 'all')
        <!-- DataTables -->
    @include('layouts._datatables-js')
    <!-- DataTables JS -->
    <script type="text/javascript">
        $(function () {
            $('#manager').dataTable({
                "bPaginate": true,
                "bSort": true,
                "bInfo": true,
                "bAutoWidth": true,
                "order": [[ 1, "asc" ]],
                "lengthMenu": [[10,15,25,50,75,100, -1], [10,15,25,50,75,100, "All"]]
            });
        });
    </script>
    @endif
@endsection