{{-- Ajax for Pagination - Do not modify this file. --}}
<script type="text/javascript">
    $(document).on('click','.pagination a', function(e) {
        e.preventDefault();
        var page = $(this).attr('href').split('page=')[1];
        getBookings(page);
    });
    function getBookings(page)
    {
        $.ajax({
                    url:'{{asset('')}}{!! $dataType !!}/{!! $id !!}?page=' + page
                })
                .done(function(data){
                    $('.ajaxContent').html(data);
                    location.hash = page;

                });
    }
</script>