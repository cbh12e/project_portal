@extends('app')
@section('css')
    {{-- Bootstrap Datepicker Override CSS for Modals --}}
    <link type="text/css" href="{{ cdn('/css/datepicker-override.css') }}" rel="stylesheet">
    <link type="text/css" href="{{ cdn('/css/dropzone.css') }}" rel="stylesheet">

    @include('layouts._datatables-css')
    {{-- Fix Datepicker Position for Modal Override --}}
    <style type="text/css">
        .datepicker {
            top: 315px !important;
        }

    </style>
@endsection
@section('content')
    @foreach ($customer as $menu)@endforeach
    <section class="content-header">
        <h1><i class="fa fa-user"></i> Customer Profile of  <small>{{$menu->FullName}}</small> </h1>
            <ol class="breadcrumb">
            <li><a href="{{url('/')}}"><i class="fa fa-dashboard"></i> Home</a></li>
            <li><a href="{{url('search')}}"><i class="fa fa-search"></i> Search</a></li>
            <li class="active"><i class="fa fa-user"></i> Customer Profile</li>
        </ol>
    </section>
        <hr>
        {{-- Booking Modal Options --}}
        <button class="btn btn-success" data-toggle="modal" id="bookingOpen" data-id="{{$menu->cus}}" data-target="#bookingModal"> <i class="fa fa-plus-circle"></i> <i class="fa fa-user"></i> New Booking</button>
        <button class="btn btn-success" data-toggle="modal"  data-target="#groupModal"> <i class="fa fa-plus-circle"></i> <i class="fa fa-users"></i> New Group Booking</button>
        <button class="btn btn-primary" data-toggle="modal"  data-target="#aModal"> <i class="fa fa-plus-circle"></i> Attach to Existing Booking</button>
        <button class="btn btn-info" data-toggle="modal" id="modalIdOpen" data-id="{{$menu->cus}}" data-target="#editModal"> <i class="fa fa-pencil-square"></i> Edit Customer</button>


        {{-- Shows Customer Notes if exists--}}
        @if($menu->cus_notes == true)
            <button class="btn btn-info" data-toggle="modal" id="customerNotes" data-notes="{{$menu->cus_notes}}"> <i class="fa fa-sticky-note-o"></i> Notes</button>
        @endif
        {{-- Shows when there is no bookings entered into the database --}}
        @if(count($menu->bok_id) < 1)
        <button class="btn btn-danger" data-toggle="modal" id="deleteOpen" data-id="{{$menu->cus}}" data-target="#customerDel"><i class="fa fa-trash"></i> Delete Customer</button>
        <hr>
        <div class="row">
            <div class="col-md-10 col-md-offset-1">
                <div class="jumbotron">
                    <div class="container">
                        <h1 class="text-red"><i class="fa fa-exclamation-circle"></i> Alert</h1>
                        <p> There are no bookings entered for {{$menu->FullName}} in the database!</p>
                        <p><a class="btn btn-primary btn-lg" data-toggle="modal" id="bookingOpen" data-id="{{$menu->cus}}" data-target="#bookingModal" role="button">Add Booking</a></p>
                    </div>
                </div>
            </div>
        </div>
    @endif


    @if(count($menu->bok_id)> 0)
<hr>
        <div class="ajaxContent">
            @include('table.customer-booking-table')
        </div>

            @endif

    @foreach($customer as $record) @endforeach

    {{-- Customer Edit Modal --}}
    @include('customer.modal.edit-customer')
    {{-- New Booking Modal --}}
    @include('customer.modal.new-booking')
    {{-- New Group Booking Modal --}}
    @include('customer.modal.attach-group')

    {{-- Add to Existing Booking--}}
    @include('customer.modal.attach-booking')

    {{-- Shows when there is no bookings entered into the database --}}
    @if(count($menu->bok_id) < 1)
    {{-- Customer Delete Modal --}}
    @include('customer.modal.delete-customer')
    @endif

    @endsection
    @section('javascript')

        {{-- Ship Name Search Typeahead --}}
        @include('layouts.ship-typeahead')

        {{-- Existing Booking Typeahead--}}
        @include('booking.typeahead')

        @if(count($record->bok_id) > 0)

     {{-- DataTables --}}
    @include('layouts._datatables-js')

        <script type="text/javascript">
                $(document).ready(function() {
                    $('#booking').DataTable( {
                        "bPaginate": true,
                        "bSort": false,
                        "bInfo": true,
                        "searching": false,
                        "bAutoWidth": true,
                        "order": [[ 3, "desc" ]],
                        "lengthMenu": [[5,10,15,25,50,75,100, -1], [5,10,15,25,50,75,100, "All"]]
                    } );
                } );
            </script>
        @endif
    {{-- Dropzone JS --}}
    <script type="text/javascript" src="{{cdn('/js/dropzone.js')}}"></script>
    {{-- Bootbox JS--}}
    <script type="text/javascript" src="https://oss.maxcdn.com/bootbox/4.2.0/bootbox.min.js"></script>
    @if($menu->cus_notes == true)
    {{-- Customer Notes Bootbox Modal --}}
    <script type="text/javascript">
        $(document).on('click','#customerNotes', function() {
            bootbox.dialog({
                title: 'Customer Notes',
                message: $(this).data('notes'),
                show: true,
                buttons: {
                    success: {
                        label: "Close",
                        className: "btn-primary",
                        callback: function () {}
                    }
                }
            });
        });
    </script>
    @endif

    <!-- Booking Option Selector -->
    @include('layouts.land-sea')
    {{--  Javascript Scripts --}}
    @include('customer.javascript')
    {{-- AJAX CSRF Token --}}
    @include('customer.csrf')

@endsection
@section('title')
{{$menu->FullName}}
@endsection

@section('icheck') @overwrite