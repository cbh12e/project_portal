<!-- Add Customer Ajax Value Append -->
<script type="text/javascript">
    $(function() {
        $(document).on('click','#bookingOpen',function(e){
            //process here you can get id using
            $('#id').val($(this).data('id')); //and set this id to any hidden field in modal
        });
    });
</script>

<!-- Update Customer Ajax -->
<script type="text/javascript">

       $(document).ready(function () {
      // function updateData() {
           $('#submit4').click(function(e) {
               e.preventDefault();
                var url = "{{action('UpdateController@storeUpdate')}}";
                var customer = $("input[name=customer]").val();
                var associate = $( "#associate").find("option:selected" ).val();
                var firstName = $("input[name=firstName]").val();
                var lastName = $("input[name=lastName]").val();
                var notesEdit = $("textarea[id=notesEdit]").val();
                var submit = $('#submit4');
                var dataString = 'customer=' + customer + '& associate=' + associate + '& firstName=' + firstName + '& lastName=' + lastName + '& notesEdit=' + notesEdit;
               // console.log(dataString);

                $.ajax({
                    type: "POST",
                    url: url,
                    data: dataString,
                    cache: false,
                    beforeSend: function () {
                        submit.attr("disabled", true);
                        submit.html('<i class="fa fa-spinner fa-spin"></i> Updating ...');
                        NProgress.start();

                    },
                      success: function(result)
                      {
                          NProgress.set(0.99);
                           submit.attr("disabled", false);
                          submit.html('<i class="fa fa-check"></i> Function Completed'); // change submit button text
                          $('#editModal').hide();
                          alertify.alert("Customer Information has successfully been updated.")
                                  .set('onok', function(closeEvent){ window.location.reload();} );
                      },
                    error: function(jqXhr){
                        NProgress.done();
                        if( jqXhr.status === 422 ) {

                            //process validation errors here.
                            // var errors = jqXhr.responseText;
                            var errors = jqXhr.responseJSON; //this will get the errors response data.
                            //show them somewhere in the markup
                            //e.g
                            var errorsHtml = ' <strong><i class="fa fa-exclamation-circle"></i></strong> There were some problems with your input. <ul>';

                            $.each( errors, function( key, value ) {
                                errorsHtml += '<li>' + value[0] + '</li>'; //showing only the first error.
                            });
                            errorsHtml += '</ul>';

                            alertify.alert(errorsHtml).set('closable', true);
                            submit.attr("disabled", false);
                            submit.html('Submit'); // change submit button text

                        } else {
                            /// do some thing else
                        }
                    }
                    });
                });
           return false;
       });

        //});
</script>

<!-- New Booking Information AJAX -->
<script type="text/javascript">
    $(document).ready(function(){
        $('#submit5').click(function(e) {
            e.preventDefault();
            var submit          =$('#submit5');  // submit button
            var url             ="{{action('RecordController@bookingStoreRequest')}}";
            var id              =$("input[name=id]").val();
            var provider        =$("input[name=provider]:checked").val();
            var reservation     =$("input[name=reservation]").val();
            var associate       =$("input[name=associate]").val();
            var ship            =$("input[name=ship]").val();
            var land            =$("input[name=land]").val();
            var date            =$("input[name=date]").val();
            var notes           =$("textarea[name=notes]").val();
            var _token		    =$("input[name=_token]").val();
            var dataString      = 'id='+ id  +'& provider=' + provider + '& reservation=' + reservation +'& associate=' + associate + '& ship=' + ship + '& land=' + land +'& date=' + date + '& notes=' + notes +'& _token=' + _token;
            console.log(dataString);
            $.ajax({
                type: "post",
                url: url,
                data: dataString,
                cache: false,
                beforeSend: function() {
                    submit.attr("disabled", true);
                    submit.html('<i class="fa fa-spinner fa-spin"></i> Adding ...');
                    NProgress.start();
                },
                success: function(result){
                     submit.attr("disabled", false);
                    submit.html('<i class="fa fa-check"></i> Function Completed'); // change submit button text
                    NProgress.done();
                    alertify.alert('New Booking has been successfully added to the database!').set('onok', function(closeEvent){ window.location.reload();} );
                },
                error: function(jqXhr){
                    NProgress.done();
                    if( jqXhr.status === 422 ) {
                        //process validation errors here.
                        // var errors = jqXhr.responseText;
                        var errors = jqXhr.responseJSON; //this will get the errors response data.
                        //show them somewhere in the markup
                        //e.g
                        var errorsHtml = ' <strong><i class="fa fa-exclamation-circle"></i></strong> There were some problems with your input. <ul>';

                        $.each( errors, function( key, value ) {
                            errorsHtml += '<li>' + value[0] + '</li>'; //showing only the first error.
                        });
                        errorsHtml += '</ul>';

                        alertify.alert(errorsHtml).set('closable', true);

                        submit.attr("disabled", false);
                        submit.html('<i class="fa fa-plus-circle"></i> Submit');
                    } else {
                        /// do some thing else
                    }
                }
            });
            return false;
        });
    });
</script>
<!-- Add to Existing Booking AJAX -->
<script type="text/javascript">
    $(document).ready(function(){
        $('#submitAbc').click(function(e) {
            e.preventDefault();
            var submit          =$('#submitAbc');  // submit button
            var url             ="{{asset('booking/customer/add')}}";
            var id              =$("input[name=aid]").val();
            var reservation     =$("input[id=reservationCba]").val();
            var _token		    =$("input[name=_token]").val();
            var dataString      = 'id='+ id  + '& reservation=' + reservation  +'& _token=' + _token;
            console.log(dataString);
            $.ajax({
                type: "post",
                url: url,
                data: dataString,
                cache: false,
                beforeSend: function() {
                    submit.attr("disabled", true);
                    submit.html('<i class="fa fa-spinner fa-spin"></i> Adding ...');
                    NProgress.start();
                },
                success: function(result){
                     submit.attr("disabled", false);
                    submit.html('<i class="fa fa-check"></i> Function Completed'); // change submit button text
                    NProgress.done();
                    alertify.alert('Customer has been successfully added to the reservation.').set('onok', function(closeEvent){ window.location.reload();} );
                },
                error: function(jqXhr){
                    NProgress.done();
                    if( jqXhr.status === 422 ) {
                        //process validation errors here.
                        // var errors = jqXhr.responseText;
                        var errors = jqXhr.responseJSON; //this will get the errors response data.
                        //show them somewhere in the markup
                        //e.g
                        var errorsHtml = ' <strong><i class="fa fa-exclamation-circle"></i></strong> There were some problems with your input. <ul>';

                        $.each( errors, function( key, value ) {
                            errorsHtml += '<li>' + value[0] + '</li>'; //showing only the first error.
                        });
                        errorsHtml += '</ul>';

                        alertify.alert(errorsHtml).set('closable', true);

                        submit.attr("disabled", false);
                        submit.html('<i class="fa fa-plus-circle"></i> Submit');

                    }
                    else{
                        // do something
                        NProgress.done();
                        submit.attr("disabled", false);
                    }
                }
            });
            return false;
        });
    });
</script>

@if(count($menu->bok_id) < 1)

  {{-- Open Customer Delete Modal--}}
<script type="text/javascript">
    $(function() {
        $(document).on('click','#deleteOpen',function(e){
            //process here you can get id using
            $('#customer').val($(this).data('id')); //and set this id to any hidden field in modal
        });
    });

</script>

{{-- Customer Delete Post Request --}}
<script type="text/javascript">
    $(document).ready(function(){
        $('#customerDel').click(function(e) {
            e.preventDefault();
            var submit = $('#customerDel');  // submit button
            // var alert = $('.alert'); // alert div for show alert message
            var url             = "{{action('DeleteController@customerDelete')}}";
            var del             =$("input[name=delete]").val();
            var _token          =$("input[name=_token]").val();
	        var dataString = 'delete='+ del + '& _token=' + _token;
            console.log(dataString);
            $.ajax({
                type: "post",
                url: url,
                data: dataString,
                cache: false,
                beforeSend: function() {
                    submit.attr("disabled", true);
                    NProgress.start();
                    submit.html('<i class="fa fa-spinner fa-spin"></i> Deleting Customer ...');
                },
                success: function(data){
                    submit.attr("disabled", false);
                    NProgress.done();
                    alertify.alert("@if(count($menu->cus_fname) > 0){{$menu->FullName}}@else {{$menu->cus_lname}} @endif has been successfully deleted from the database!").set('onok', function(closeEvent){ window.location='{{url('home')}}';} );
                    //return data;
                },
                error: function(jqXhr){
                    NProgress.done();
                    if( jqXhr.status === 422 ) {
                        //process validation errors here.
                        // var errors = jqXhr.responseText;
                        var errors = jqXhr.responseJSON; //this will get the errors response data.
                        //show them somewhere in the markup
                        //e.g
                        var errorsHtml = ' <strong><i class="fa fa-exclamation-circle"></i></strong> There were some problems with your input. <ul>';

                        $.each( errors, function( key, value ) {
                            errorsHtml += '<li>' + value[0] + '</li>'; //showing only the first error.
                        });
                        errorsHtml += '</ul>';

                        alertify.alert(errorsHtml).set('closable', true);

                        submit.attr("disabled", false);
                        submit.html('<i class="fa fa-minus-circle"></i> Submit');

                    }
                    else{
                        // do something
                        NProgress.done();
                        submit.attr("disabled", false);
                    }
                }
            });
            return false;
        });
    });
</script>
@endif

@include('layouts._datepicker')

@include('groups.typeahead')