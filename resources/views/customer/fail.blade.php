@extends('app')
@section('title')
No Results
@endsection
@section('content')
    <div class="jumbotron">
        <div class="error-page">
            <div class="text-center">
                <h2 class="headline text-yellow" style="font-size: 75px;"><i class="fa fa-warning fa-2x"></i> <br /> No Results found</h2>
                <div class="error-content">
                    <h3>
                        Your Search did not return any results. <br />
                        Meanwhile, you may <a href='{{asset('/')}}'>return to dashboard</a> or try using the search form.</h3>
                </div><!-- /.text-center -->
            </div><!-- /.error-content -->
        </div><!-- /.error-page -->
    </div>
@endsection