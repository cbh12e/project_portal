<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name ="robots" content="noindex, nofollow">
    @if(Auth::guest() === false)
	<meta name="_token" content="{{ csrf_token() }}">
    @endif
	@yield ('meta')

	<title>{{env('APP_TITLE')}} | @yield('title', 'Default Title')</title>

    @include('layouts._header')

    @yield('css')

    @if(App::environment('production'))
            @include('layouts._ga')
    @else
            <!-- GOOGLE ANALYTICS CODE AREA --- USED IN PRODUCTION ENVIRONMENT ONLY -->
    @endif

    <!-- Favicon -->
    <link type="image/ico" rel="shortcut icon" href="{{ asset('/favicon-space-shuttle.ico')}}">

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
     <script type="text/javascript" src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
     <script type="text/javascript" src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->

</head>

<body class="@if (Auth::guest()) login-page @else @section('navcolor')skin-blue @show @endif">

@if (Auth::guest())
@section('guestbar')
@show
@else
 <div class="wrapper">
     @section('navbar')
         @include('layouts._navbar')
     @show
     @section('sidebar')
             @include('layouts._sidebar')
     @show
         <div id="content">
             <div class="content-wrapper">
                 <section class="content">
                     @include('layouts._status')
                     @yield('content')
                 </section>
             </div>
             @endif
             @if (Auth::guest())
                 @yield('content')
             @else
                 @include('layouts._footer')
         </div>
 </div>
 @endif
@include('layouts._bottom')
</body>
</html>