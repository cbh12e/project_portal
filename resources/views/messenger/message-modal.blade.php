<div class="modal" id="messageModal" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">

                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title"><i class="fa fa-comment"></i> New Message</h4>
            </div>

            {!! Form::open(['route' => 'messages.store']) !!}

                         <div class="modal-body">

                             <div class="form-group">
                                 <label class="control-label" for="recipients">To:</label>
                             @if($users->count() > 0)
                                 <div class="checkbox">
                                     @foreach($users as $user)
                                         <label title="{!!$user->name!!}"><input type="checkbox"  class="minimal-blue" name="recipients[]" value="{!!$user->id!!}">{!!$user->name!!}</label>
                                     @endforeach
                                 </div>
                                 @endif
                             </div>


                             <!-- Subject Form Input -->
                             <div class="form-group">
                                 {!! Form::label('subject', 'Subject:', ['class' => 'control-label']) !!}
                                 {!! Form::text('subject', null, ['class' => 'form-control']) !!}
                             </div>

                             <!-- Message Form Input -->
                             <div class="form-group">
                                 {!! Form::label('message', 'Message:', ['class' => 'control-label']) !!}
                                 {!! Form::textarea('message', null, ['class' => 'form-control']) !!}
                             </div>

                <!-- MODAL FOOTER -->
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-primary" name="submit" id="submit"> Send</button>
                </div>
                                 {!! Form::close() !!}
        </div>
    </div>
</div>
</div>
