@extends('app')
@section('title')
Laravel Messenger
@endsection
@section('content')

    <section class="content-header">
        <h1><i class="fa fa-comments"></i> Laravel Messenger</h1>
        <ol class="breadcrumb">
            <li><a href="{{url('/')}}"><i class="fa fa-dashboard"></i> Home</a></li>
            <li class="active"><i class="fa fa-comments"></i> Laravel Messenger</li>
        </ol>
    </section>

    <hr>

    <div class="alert alert-danger" role="alert"> <i class="fa fa-times-circle"> </i> This Feature is currently under construction. Please do not use this until this message is removed. </h2>
</div>

    {{-- <a href="{{url('messages/create')}}" class="btn btn-primary"> <i class="fa fa-plus-circle"></i> New Message</a>--}}
    <button class="btn btn-primary" data-toggle="modal" id="modalIdOpen" data-target="#messageModal"><i class="fa fa-plus-circle"></i> New Message</button>

    @if($threads->count() > 0)
        @foreach($threads as $thread)
        <?php $class = $thread->isUnread($currentUserId) ? 'alert-info' : ''; ?>
        <div class="media alert {!!$class!!}">
            <h4 class="media-heading"><a href="{{expire(url('messages/' . $thread->id))}}">{!! $thread->subject !!}</a></h4>

            <p>{!! $thread->latestMessage->body !!}</p>
            <p><small><strong>Creator:</strong> {!! $thread->creator()->name !!}</small></p>
            <p><small><strong>Participants:</strong> {!! $thread->participantsString(Auth::id()) !!}</small></p>
        </div>
        @endforeach

        {!! $threads->render() !!}
    @else
        <p>Sorry, no threads.</p>
    @endif

    @include('messenger.message-modal')
@endsection