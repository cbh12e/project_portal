@extends('app')
@section('title','Dashboard')
@section('css')
    @if(App::environment('production'))
        <link type="text/css" rel="stylesheet" href="{{cdn('/vendor/plugins/morris/morris.css')}}">
    @else
        <link type="text/css" rel="stylesheet" href="{{cdn('/AdminLTE/plugins/morris/morris.css')}}">
    @endif
@endsection

@section('content')

    @if (session('invalid_permissions'))
        <div class="alert alert-danger">
        <i class="fa fa-check-circle"></i> {{ session('invalid_permissions') }}
        </div>
    @endif

    <section class="content-header">
        <h1><i class="fa fa-dashboard"></i> Dashboard</h1>
            <ol class="breadcrumb">
                <li class="active"><i class="fa fa-dashboard"></i> Home</li>
            </ol>
    </section>
    <hr>
    <div class="container">
        <!-- Info boxes -->
		<div class="row">
            <div class="col-md-3 col-sm-6 col-xs-12">
            <!-- small box -->
                <div class="small-box bg-aqua">
                    <div class="inner">
                    <h3>{!! $bookings !!}<sup style="font-size: 20px"></sup></h3>
                    <p>Entered Bookings</p>
                </div>
                    <div class="icon">
                    <i class="fa fa-book"></i>
                </div>
                <a href="{{ url('search')}}" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
                </div><!--/.box -->
            </div><!--/.col -->
            <div class="col-md-5 col-sm-6 col-xs-12">
                <div class="small-box bg-yellow">
                    <div class="inner">
                        <h3>{!! $images !!}<sup style="font-size: 20px"></sup></h3>
                        <p>Images stored on the Server</p>
                    </div>
                    <div class="icon">
                        <i class="fa fa-files-o"></i>
                    </div>
                    <a href="{{expire(url('upload'))}}" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
                </div><!--/.box -->
            </div><!-- /.col -->

            <!-- fix for small devices only -->
            <div class="col-md-3 col-sm-6 col-xs-12">
                <!-- small box -->
                <div class="small-box bg-fuchsia">
                    <div class="inner">
                        <h3>{!! customer_count() !!}<sup style="font-size: 20px"></sup></h3>
                        <p>Number of Customers</p>
                    </div>
                    <div class="icon">
                        <i class="fa fa-users"></i>
                    </div>
                    <a href="{{ url('search')}}" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
                </div><!--/.box -->
            </div><!--/.col -->
        </div><!-- /.row -->

<div class="row">
    <div class="col-md-3 col-sm-6 col-xs-12">
        <!-- small box -->
        <div class="small-box bg-red-gradient">
            <div class="inner">
                <h3>{!! group_count() !!}<sup style="font-size: 20px"></sup></h3>
                <p>Active Groups</p>
            </div>
            <div class="icon">
                <i class="fa fa-users"></i>
            </div>
            <a href="{{ url('groups/active')}}" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
        </div><!--/.box -->
    </div><!-- /.col -->

    <div class="col-md-5 col-sm-6 col-xs-12">
        <!-- small box -->
        <div class="small-box bg-green-gradient">
            <div class="inner">
                <h3>{!! countAgents() !!}<sup style="font-size: 20px"></sup></h3>
                <p>Active Associates</p>
            </div>
            <div class="icon">
                <i class="fa fa-users"></i>
            </div>
            <a href="{{ url('search')}}" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
        </div><!--/.box -->
	</div><!-- /.col -->

    <div class="col-md-3 col-sm-6 col-xs-12">
        <!-- Info Boxes Style 2 -->
        <div class="info-box">
            <!-- small box -->
            <div class="small-box bg-orange">
                <div class="inner">
                    <h3>{!! fileLimit() !!}<sup style="font-size: 20px"></sup> MB</h3>
                    <p>Maximum File Upload Limit</p>
                </div>
                <div class="icon">
                    <i class="fa fa-files-o"></i>
                </div>
                <a href="{{exp_url('upload')}}" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
            </div><!--/.box -->
        </div><!-- /.col -->
    </div><!-- /.row -->
</div><!-- /.row -->
    </div><!-- /.container -->
@endsection