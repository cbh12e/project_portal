
<!-- Modal -->
<div class="modal" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">

                <h4 class="modal-title" id="myModalLabel"></h4>
            </div>
            <div class="modal-body">
                <div class="progress progress-striped active">
                    <div class="progress-bar progress-bar-striped" role="progressbar" data-transitiongoal="100"></div>
                    <span id="update"></span>
                    <span id="done"></span>
                </div>
            <div class="modal-footer">

            </div>
        </div>
    </div>


