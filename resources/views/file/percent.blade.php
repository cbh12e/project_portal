<script type="text/javascript">

    function TotalProgress(progress)
    {
        var percent = parseFloat(progress).toFixed();

        //Progress bar
        $('#progressbar').width(progress + '%');//update progressbar percent complete
        $('#percent').html(percent + '%'); //update status text

        NProgress.set(0.,percent);
    }
</script>