 <div class="col-lg-10 col-md-12">
    <table class="table table-striped table-bordered" id="customerTable">
        <thead>
        <tr>
            <th> Customer Name</th>
            <th class="hidden-sm hidden-xs"> Action</th>
        </tr>
        </thead>
        <tbody>

@foreach($book->customers as $customer)
        <tr>
            <td> {{$customer->CustomerFullName}}</td>
            <td>
                @if(count($book->customers) > 1)
                    <button class="btn btn-danger" data-toggle="modal" id="modalOpen" data-booking="{{$book->bok_id}}" data-customer="{{$customer->cus_id}}" data-target="#myModal"><i class="fa fa-minus-circle"></i> Detach <span class="hidden-xs hidden-sm">Customer </span></button>
                @endif
                    <a href="{{ expire(action('RecordController@viewCustomer', ['id' => $customer->cus_selector])) }}" class="btn btn-info"> <i class="fa fa-user"></i> View <span class="hidden-xs hidden-sm">Customer </span></a>
            </td>
        </tr>
@endforeach
        </tbody>
    </table>
</div>
