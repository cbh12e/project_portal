<table class="table table-striped table-bordered" id="booking">
    <thead>
    <tr>
        <th class="hidden-xs hidden-sm"> Reservation Number</th>
        <th class="hidden-xs hidden-sm"> Associate Name</th>
        <th> Ship or Vacation Name</th>
        <th> Embarkation Date</th>
        <th> Action</th>
    </tr>
    </thead>
    <tbody>
    @foreach($customer as $record)
        @if(count($record->bok_id)> 0)
            <tr>
                @if($record->bok_reservation !== null)
                    <td class="hidden-xs hidden-sm">{{$record->bok_reservation}}</td>
                @else
                    <td class="hidden-xs hidden-sm"></td>
                @endif
                <td class="hidden-xs hidden-sm">{{$record->aso_name}}</td>
                <td>
                    @if($record->bok_type === 'Land')
                    <i class="fa fa-fort-awesome"></i> {{$record->LandProvider }}
                    @else
                        <i class="fa fa-ship"></i>  {{$record->bok_ship }}
                    @endif
                </td>
                <td><!-- Desktops and Tablets -->
                    <span class="hidden-xs hidden-sm">{{human_date($record->bok_date)}}</span>
                    <!-- Mobile Devices -->
                    <span class="visible-xs visible-sm">{{human_date_mobile($record->bok_date)}}</span>
                </td>
                <td><a href="{{ expire(action('RecordController@viewBooking', ['id' => $record->bok_selector]))}}" class="btn btn-info">View</a> </td>
            </tr>
        @endif
    @endforeach
    </tbody>
</table>