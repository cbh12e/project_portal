<div class="col-lg-10 col-md-12">
        <table class="table table-striped" id="manager">
            <thead>
            <tr>
                <th> File</th>
                <th class="hidden-sm hidden-xs"> Size</th>
                <th class="hidden-sm hidden-xs"> Date </th>
                <th> Notes</th>
                <th> Delete</th>
            </tr>
            </thead>
            <tbody>
            @foreach($book->images as $row)
                <tr>
                    <td> {{-- S3 File View Link --}}
                        <a href="{{ S3($row->img_path) }}" target="_blank" class="btn btn-primary">
                            @if(is_pdf($row->img_mime))
                                {{-- PDF File FontAwesome Icon --}}
                                <i class="fa fa-file-pdf-o"></i>
                            @elseif (is_image($row->img_mime))
                                {{-- Image File FontAwesome Icon --}}
                                <i class="fa fa-file-image-o"></i>
                            @elseif (is_message($row->img_ext))
                                {{-- Email FontAwesome Icon --}}
                                <i class="fa fa-envelope"></i>
                            @elseif (is_text($row->img_mime))
                                {{-- Text File FontAwesome Icon --}}
                                <i class="fa fa-file-text-o"></i>
                            @else
                                {{-- Unknown File Type FontAwesome Icon --}}
                                <i class="fa fa-file"></i>
                            @endif
                                {{-- File Category Name --}}
                                &nbsp;{{$row->category->cat_name}}
                        </a>
                    </td>
                    <td class="hidden-sm hidden-xs"> {{ human_filesize($row->img_size)}} </td>
                    <td class="hidden-sm hidden-xs">{{ time_zone($row->created_at)}}</td>
                    <td>@if($row->img_notes == true)<button class="btn btn-info" data-toggle="modal" id="imgNotes" data-notes="{{$row->img_notes}}"><i class="fa fa-sticky-note-o"></i></button> @endif</td>
                    <td><a href="" data-toggle="modal" id="modalIdOpen" data-id="{{$row->img_id}}" data-target="#myLargeModal" class="btn btn-danger"><i class="fa fa-trash"></i></a></td>
                </tr>
            @endforeach
            </tbody>
        </table>
    </div>