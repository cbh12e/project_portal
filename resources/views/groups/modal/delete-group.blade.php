<div class="modal fade" id="deleteGroup" tabindex="-1" role="dialog"
     aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Cancel Group: {{$result->group_name}} </h4>
            </div>
            <form class="form-horizontal" role="form" method="post" action="{{url('groups/delete')}}">
            <div class="modal-body">
                <p>Are you sure you want to cancel this group with the number of :{{$result->group_number}}? This group can only be reinstated with user with administrator privileges.</p>
                <input type="hidden" name="delete" value="{{$result->group_id}}"/>
                <input type="hidden" name="_token" value="{{ csrf_token() }}">
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-primary" data-dismiss="modal"> Close</button>
                <button type="submit" class="btn btn-danger" id="deleteTrue"><i class="fa fa-times-circle"></i> Cancel</button>
            </div>
            </form>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->
