<div class="modal fade" tabindex="-1" role="dialog" id="createGroupModal">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Create New Group</h4>
            </div>
            <form class="form-horizontal" method="post" action="{{action('GroupController@groupAdd')}}" role="form">
                <div class="modal-body">
                    <fieldset>
                        <input type="hidden" name="_token" value="{{ csrf_token() }}">

                        <!-- Select Associate Name Option-->
                        <div class="form-group">
                            <label class="col-md-4 control-label" for="associate">Associate: *</label>
                            <div class="col-md-6">
                                <div class="input-group margin-bottom-sm">
                                    <span class="input-group-addon"><i class="fa fa-users"></i></span>
                                    <select name="associate" id="associate" class="form-control selectpicker">
                                    @foreach(all_agents() as $row)
                                        <option value="{{$row->aso_id}}">{{$row->AssociateName}}</option>
                                    @endforeach
                                    </select>
                                </div>
                            </div>
                        </div>

                        <!-- Group Number -->
                        <div class="form-group">
                            <label class="col-md-4 control-label" for="">Group Number:</label>
                            <div class="col-md-7">
                                <div class="input-group">
                                    <span class="input-group-addon"><i class='fa fa-list-ol'></i></span>
                                    <input type="text" name="groupNum" id="groupNum"  placeholder="Group Number" value="{{old('groupNum')}}" class="form-control input-md" autocomplete="off">
                                </div>
                            </div>
                        </div>

                        <!-- Group Name -->
                        <div class="form-group">
                            <label class="col-md-4 control-label" for="groupName">Group Name:</label>
                            <div class="col-md-7">
                                <div class="input-group">
                                    <span class="input-group-addon"><i class='fa fa-pencil'></i></span>
                                    <input type="text" name="groupName" id="groupName"  placeholder="Group Name" value="{{old('groupName')}}" class="form-control input-md" autocomplete="off">
                                </div>
                            </div>
                        </div>

                        <!-- Name of Ship -->
                        <div class="form-group">
                            <label class="col-md-4 control-label" for="ship">Ship: *</label>
                            <div class="col-md-7">
                                <div class="input-group">
                                    <span class="input-group-addon"><i class='fa fa-ship'></i></span>
                                    <input type="text" name="ship" id="ship" class="form-control" placeholder="Ship Name" value="{{old('ship')}}">
                                </div>
                            </div>
                        </div>

                        <!-- Date of Sailing -->
                        <div class="form-group">
                            <label class="col-md-4 control-label" for="date">Date of Sailing: *</label>
                            <div class="col-md-4">
                                <div class="input-group">
                                    <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                                    <input type="text" name="date" id="bDate" class="form-control" value="{{old('date')}}" placeholder="Sail Date">
                                </div>
                            </div>
                        </div>

                        <!-- Notes -->
                        <div class="form-group">
                            <label class="col-md-4 control-label" for="notes">Notes:</label>
                            <div class="col-md-7">
                                <textarea name="notes" id="notes" class="form-control" rows="5"></textarea>
                            </div>
                        </div>
                    </fieldset>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-primary">Submit</button>
                </div>
            </form>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->