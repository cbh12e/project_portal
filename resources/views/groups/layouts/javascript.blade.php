    <!-- DataTables JS -->
    <script type="text/javascript">
        $(function () {
            $('#group').dataTable({
                "paging": true,
                "lengthChange": false,
                "searching": false,
                //"ordering": true,
                "bOrder": [[ 1, "asc" ]],
                "bInfo": true,
                "autoWidth": false
            });
        });
    </script>

    @if(count($result->images)> 0)
    <!-- DataTables JS -->
    <script type="text/javascript">
        $(function () {
            $('#imageTable').dataTable({
                "paging": true,
                "lengthChange": false,
                "searching": false,
                //"ordering": true,
                "order": [[ 1, "asc" ]],
                "info": true,
                "autoWidth": false
            });
        });
    </script>
    @endif

    <!-- Datepicker -->
    <script type="text/javascript">
        $('#bDate').datepicker({
            startDate: "01/01/2015",
            endDate: "01/01/2020",
            todayHighlight: true
        });
    </script>

    <!-- Delete Group Ajax -->
    <script type="text/javascript">
        $(document).on('ready page:load', function () {
            $("#deleteTrue").click(function(e){
                e.preventDefault();
                var delete1         =$("input[name=delete]").val();
                var dataString      = 'delete='+ delete1;
                $.ajax({
                    type: "POST",
                    url : "{{url('groups/delete')}}",
                    data : dataString,
                    cache: false,
                    beforeSend: function(e) {
                        NProgress.start();
                    },
                    success: function(e){
                        NProgress.done();
                        $('#deleteGroup').hide();
                        alertify.alert("Group #{{$result->group_number}} has been successfully been set to Cancelled.")
                                .set('onok', function(closeEvent){ window.location='{{asset('groups/active')}}'});
                    },
                    error: function(jqXhr){
                        NProgress.done();
                        if( jqXhr.status === 422 ) {
                            //process validation errors here.

                            var errors = jqXhr.responseJSON; //this will get the errors response data.
                            //show them somewhere in the markup
                            //e.g
                            var errorsHtml = ' <strong><i class="fa fa-exclamation-circle"></i></strong> There were some problems with your input. <ul>';

                            $.each( errors, function( key, value ) {
                                errorsHtml += '<li>' + value[0] + '</li>'; //showing only the first error.
                            });
                            errorsHtml += '</ul>';

                            alertify.alert(errorsHtml).set('closable', true);

                            // $( '#error' ).html( errorsHtml ); //appending to a <div id="form-errors"></div> inside form

                        } else {
                            // alertify.alert("Booking was not edited.");
                        }
                    }
                });
                return false;
            });
        });
    </script>

    <!-- Group Update Ajax -->
    <script type="text/javascript">
        $(document).on('ready page:load', function () {
            $("#209A").click(function(e){
                e.preventDefault();
                var group           =$("input[name=group]").val();
                var associate       =$("#associate").find("option:selected" ).val();
                var number          =$("input[name=number]").val();
                var name            =$("input[name=name]").val();
                var ship            =$("input[name=ship]").val();
                var date            =$("input[name=date]").val();
                var notes           =$("textarea[id=notes]").val();
                var dataString      = 'group='+ group + '& associate=' + associate + '& number=' + number +'& name=' + name + '& date=' + date + '& notes=' + notes + '& ship=' + ship;

                $.ajax({
                    type: "POST",
                    url : "{{url('groups/update')}}",
                    data : dataString,
                    cache: false,
                    beforeSend: function(e) {
                        console.log(dataString);
                        NProgress.start();
                    },
                    success: function(e){
                        NProgress.done();
                        //window.location.reload();
                        //alertify.defaults.glossary.title = "Success";
                        alertify.alert("Group has successfully been updated.")
                                .set('onok', function(closeEvent){ window.location.reload();});
                    },

                    error: function(jqXhr){
                        NProgress.done();
                        if( jqXhr.status === 422 ) {
                            //process validation errors here.
                            // var errors = jqXhr.responseText;
                            var errors = jqXhr.responseJSON; //this will get the errors response data.
                            //show them somewhere in the markup
                            //e.g
                            var errorsHtml = ' <strong><i class="fa fa-exclamation-circle"></i></strong> There were some problems with your input. <ul>';

                            $.each( errors, function( key, value ) {
                                errorsHtml += '<li>' + value[0] + '</li>'; //showing only the first error.
                            });
                            errorsHtml += '</ul>';

                            alertify.alert(errorsHtml).set('closable', true);

                            // $( '#error' ).html( errorsHtml ); //appending to a <div id="form-errors"></div> inside form
                        } else {
                            // alertify.alert("Booking was not edited.");
                        }
                    }
                });
                return false;
            });
        });
    </script>


    @if($result->group_notes == true)
        {{-- Customer Notes Bootbox Modal --}}
        <script type="text/javascript">
            $(document).on('click','#groupNotes', function() {
                bootbox.dialog({
                    title: 'Group Notes',
                    message: $(this).data('notes'),
                    show: true,
                    buttons: {
                        success: {
                            label: "Close",
                            className: "btn-primary",
                            callback: function () {}
                        }
                    }
                });
            });
        </script>
    @endif

    @if(count($result->images)> 0)

        {{--  Delete Modal Click --}}
        <script type="text/javascript">
            $(function() {
                $(document).on('click','#modalIdOpen',function(e){ //process here you can get id using
                    $('#ID').val($(this).data('id')); //and set this id to any hidden field in modal
                });
            });
        </script>

        {{-- Delete File --}}
        <script type="text/javascript">
            $(document).on('ready page:load', function () {
                $('#submit10').click(function(e) {
                    e.preventDefault();
                    var submit      = $('#submit10');  // submit button
                    var url         = "{{url('group/image/delete')}}";
                    var image        =$("input[name=image]").val();
                    var dataString  = 'image='+ image;
                    //console.log(dataString);
                    $.ajax({
                        type: "post",
                        url: url,
                        data: dataString,
                        cache: false,
                        beforeSend: function(e) {
                            // alert.fadeOut();
                            NProgress.start();
                            submit.attr("disabled", true);
                            submit.html('<i class="fa fa-spinner fa-spin"></i> Deleting File... Please wait'); // change submit button text
                        },
                        success: function(e){
                            NProgress.set(0,99);
                            submit.attr("disabled", false);
                            alertify.alert("The selected file has been successfully deleted from the server.").set('onok', function(closeEvent){  NProgress.done(); window.location.reload();} );

                            // return data;
                        },
                        error: function(e) {
                            NProgress.done();
                            alertify.alert("File was not deleted.", function() {
                            });
                            submit.html('Delete File'); // change submit button text
                            submit.attr("disabled", false);

                        }
                    });
                    return false;
                });
            });
        </script>

        {{-- Image Notes Bootbox Modal --}}
        <script type="text/javascript">
            $(document).on('click','#imgNotes', function() {
                bootbox.dialog({
                    title: 'Notes for File',
                    message: $(this).data('notes'),
                    show: true,
                    buttons: {
                        success: {
                            label: "Close",
                            className: "btn-primary",
                            callback: function () {}
                        }
                    }
                });
            });
        </script>
        @endif