@extends('app')
@section('css')
        <!-- Bootstrap Datepicker Override CSS for Modals -->
<link type="text/css" href="{{ asset('/css/datepicker-override.css') }}" rel="stylesheet">
<link type="text/css" href="//cdn.datatables.net/plug-ins/1.10.9/integration/font-awesome/dataTables.fontAwesome.css" rel="stylesheet">
@endsection
@section('content')
    <div id="success">
        <!-- Success Alert -->
        @if (session('flash_message'))
            <div class="alert alert-success">
                {{ session('flash_message') }}
            </div>
        @endif
    </div>
@foreach($resultQuery as $result) @endforeach
    <div id="info">
        <section class="content-header">
            <h1><i class="fa fa-ship"></i> Group Name:  <small>{{$result->group_name}} - {{$result->group_ship}} - {{human_date($result->group_date)}} </small></h1>
            <ol class="breadcrumb">
                <li><a href="{{url('/')}}"><i class="fa fa-dashboard"></i> Home</a></li>
                <li><a href="#"><i class="fa fa-users"></i><i class="fa fa-search"></i>Group List</a></li>
                <li class="active"><i class="fa fa-users"></i> Group #{{$result->group_number}} </li>
            </ol>
        </section>
        <br />
        <div class="col-md-11">
            <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#myModal"><i class="fa fa-pencil"></i> Update Group </button>
            @if($result->group_notes == true)
                <button class="btn btn-info" data-toggle="modal" id="groupNotes" data-notes="{{$result->group_notes}}"> <i class="fa fa-sticky-note-o"></i> Group Notes</button>
            @endif
            <button type="button" class="btn btn-danger" data-toggle="modal" data-target="#deleteGroup"><i class="fa fa-trash"></i> Cancel Group </button>
            <a href="{{ expire(action('Amazon\GroupUploadController@newImagePage', ['id' => $result->group_selector]))}}" class="btn btn-primary"><i class="fa fa-upload"></i> Upload Group Document</a>
        </div>

        @if($result->ship->cruiseLine->cl_logo != null)
            <div class="col-md-1">
                <img class="img img-responsive" alt="{{$result->ship->cruiseLine->cl_name}} Logo" src="{{asset('/logos/'.$result->ship->cruiseLine->cl_logo)}}">
            </div>
        @endif
    </div>

    <hr>
    @if(count($result->images)> 0)
    <div class="col-md-5">
        <h1> Group Bookings </h1>
    </div>
    <hr/>
    @endif

    <div class="row">
        <div class="col-lg-9 col-lg-offset-1 col-xs-10">

            <table class="table table-striped table-bordered" id="group">
                <thead>
                <tr>
                    <th>Customer Name</th>
                    <th>Action</th>
                </tr>
                </thead>
                <tbody>
                @foreach($result->bookings as $record)
                    @foreach($record->customer as $customer)
                    <tr>
                        @if($customer->cus_fname == true)
                            <td>{{$customer->CustomerFullName}}</td>
                        @else
                            <td>{{$customer->cus_lname }}</td>
                        @endif
                            <td><a href="{{ expire(action('RecordController@viewBooking', ['id' => $record->bok_selector])) }}" class="btn btn-info"> <i class="fa fa-book"></i> View Booking</a>
                                <a href="{{ expire(action('RecordController@viewCustomer', ['id' => $customer->cus_selector])) }}" class="btn btn-info"> <i class="fa fa-user"></i> View Customer</a> </td>
                    </tr>
                    @endforeach
                @endforeach
                </tbody>
            </table>
        </div>
    </div>
    @if(count($result->images)> 0)
        <div class="col-md-5">
            <h1> Group Documents </h1>
        </div>
        <hr>
        <div class="row">
            <div class="col-lg-9 col-lg-offset-1 col-xs-10">
                <table class="table table-striped" id="imageTable">
                    <thead>
                    <tr>
                        <th> File</th>
                        <th class="hidden-sm hidden-xs"> Size</th>
                        <th class="hidden-sm hidden-xs"> Date </th>
                        <th> Notes</th>
                        <th> Delete</th>
                    </tr>
                </thead>
                    <tbody>
                    @foreach($result->images as $row)
                        <tr>
                            <td> {{-- S3 File View Link --}}
                                <a href="{{ S3($row->img_path) }}" target="_blank" class="btn btn-primary">
                                    @if(is_pdf($row->img_mime))
                                    {{-- PDF File FontAwesome Icon --}}
                                        <i class="fa fa-file-pdf-o"></i>
                                    @elseif (is_image($row->img_mime))
                                    {{-- Image File FontAwesome Icon --}}
                                        <i class="fa fa-file-image-o"></i>
                                    @elseif (is_message($row->img_ext))
                                    {{-- Email FontAwesome Icon --}}
                                        <i class="fa fa-envelope"></i>
                                    @elseif (is_text($row->img_mime))
                                    {{-- Text File FontAwesome Icon --}}
                                        <i class="fa fa-file-text-o"></i>
                                    @else
                                    {{-- Unknown File Type FontAwesome Icon --}}
                                        <i class="fa fa-file"></i>
                                    @endif
                                    {{-- File Category Name --}}
                                        &nbsp;{{$row->category->cat_name}}
                                </a>
                            </td>
                            <td class="hidden-sm hidden-xs"> {{ human_filesize($row->img_size)}} </td>
                            <td class="hidden-sm hidden-xs">{{ time_zone($row->created_at)}}</td>
                            <td>@if($row->img_notes == true)<button class="btn btn-info" data-toggle="modal" id="imgNotes" data-notes="{{$row->img_notes}}"><i class="fa fa-sticky-note-o"></i></button> @endif</td>
                            <td><a href="" data-toggle="modal" id="modalIdOpen" data-id="{{$row->img_id}}" data-target="#myLargeModal" class="btn btn-danger"><i class="fa fa-trash"></i></a></td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    @endif

    <form class="form-horizontal" id="booking">
        <div class="form-group">
            <label class="col-md-3 control-label"> Group ID: </label>
            <div class="col-md-2">
                <p class="form-control-static">{{$result->group_id}}</p>
            </div>
        </div>

        <div class="form-group">
            <label class="col-md-3 control-label"> Group Number: </label>
            <div class="col-md-2">
                <p class="form-control-static">{{$result->group_number}}</p>
            </div>
        </div>

        <div class="form-group">
            <label class="col-md-3 control-label"> Group Name: </label>
            <div class="col-md-8">
                <p class="form-control-static">{{$result->group_name}}</p>
            </div>
        </div>

        <div class="form-group">
            <label class="col-md-3 control-label"> Associate Name: </label>
            <div class="col-md-8">
                <p class="form-control-static">{{$result->associate->AssociateName}}</p>
            </div>
        </div>


        <div class="form-group">
            <label class="col-md-3 control-label" for="ship"> Group Ship:</label>
            <div class="col-md-5">
                <p class="form-control-static"> {{ $result->ship->ship_name }} </p>
            </div>
        </div>

        <div class="form-group">
            <label class="col-md-3 control-label" for="line"> Cruise Line:</label>
            <div class="col-md-5">
                <p class="form-control-static"> {{ $result->ship->cruiseLine->cl_name }} </p>
            </div>
        </div>

        <div class="form-group">
            <label class="col-md-3 control-label"> Date of Sailing:</label>
            <div class="col-md-2">
                <p class="form-control-static"> {{ human_date2($result->group_date)}}</p>
            </div>
        </div>
    </form>

            <!-- Group Update Modal -->
    @include('groups.modal.update-group')
            <!-- Delete Group Modal -->
    @include('groups.modal.delete-group')
            <!-- Delete Image Modal -->
    @include('groups.modal.delete-image')
    @endsection

    @section('javascript')
    {{-- Bootbox JS--}}
    {!! bootBox() !!}

    {{-- Ship Name Search Typeahead --}}
    @include('layouts.ship-typeahead')

            <!-- DataTables -->
    @include('layouts._datatables-js')

    {{-- JavaScript Files --}}
    @include('groups.layouts.javascript')
        <!-- CSRF Token AJAX -->
        @include('customer.csrf')

@endsection
@section('title')
    Group {{$result->group_number}}
@endsection