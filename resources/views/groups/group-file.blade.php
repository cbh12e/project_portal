@extends('app')
@section('title','File Upload')
@section('css')
    <link type="text/css" href="{{ cdn('/css/dropzone.css') }}" rel="stylesheet">
    @endsection
@section('content')
<div class="content-header">
    <h4><i class="fa fa-file-pdf-o"></i> File Drop for Group #{{$id->group_number }}</h4>

    <ol class="breadcrumb">
        <li><a href="{{url('/')}}"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="{{expire(url('groups/'.$id->group_id))}}"><i class="fa fa-users"></i> Group # {{$id->group_number}}</a></li>
        <li class="active"><i class="fa fa-upload"></i> File Upload</li>
    </ol>
</div>

    <form class="form-horizontal dropzone" method="post" action="{{action('Amazon\GroupUploadController@groupUpload')}}" enctype="multipart/form-data" id="my-awesome-dropzone">
        <div class="col-md-12">

            <!-- CSRF TOKEN -->
            <input type="hidden" name="_token" value="{{ csrf_token() }}">

            <!-- GROUP ID -->
            <input type="hidden" name="id" id="ID" value="{{$id->group_id }}">

            <!-- NOTES -->
            <small> All fields with an asterisk * are required! Do not store CC Info. <br /> Single File Upload Limit: {!! fileLimit() !!} MB - Combined Upload Limit: {!! totalLimit() !!} MB</small> <br /><br />


            <!-- CATEGORY NAME -->
            <div class="form-group">
                <label class="col-md-3 control-label"> File Category: *</label>
                <div class="col-md-8">
                    @foreach($record as $category)
                    <label>
                        <input type="radio" name="category"  value="{{$category->cat_id}}" class="minimal-blue"/> {{$category->cat_name}}
                    </label>
                    @endforeach
                </div>
            </div>

            <!-- this is were the previews should be shown. -->

            <!-- <div class="form-group">
                 <label class="col-md-3 control-label" for="file"> File Drop: *</label>
                 <div class="col-md-5">
                     <div class="dropzone-previews"></div>
                     <div class="input-group">
                         <input type="file" name="file[]" id="file" title="Add File" multiple class="dropzone">
                     </div>
                 </div>
             </div>-->


            <!-- NOTES -->
            <div class="form-group">
                <label class="col-md-3 control-label" for="notes"> Notes: </label>
                <div class="col-md-7">
                    <textarea class="form-control" name="notes" id="notes" rows="3"></textarea>
                </div>
            </div>
        </div>

        <div class="form-group">
            <div class="col-md-7 col-md-offset-3">
                <button type="submit" id="submit6" class="btn btn-primary"><i class="fa fa-upload"></i> Upload</button>

            </div>
        </div>


    </form>

    @include('modals.progress')

    @endsection

@section('javascript')

    @include('customer.csrf')

    <!-- File Upload Percent JS-->
    @include('file.percent')

    <script type="text/javascript">
        $(document).on('ready page:load', function () {

            Dropzone.options.myAwesomeDropzone = { // The camelized version of the ID of the form element

                // The configuration we've talked about above
                autoProcessQueue: false,
                uploadMultiple: true,
                parallelUploads: 100,
                maxFiles: 6,
                maxFilesize: {!! totalLimit() !!},
                paramName: "file",
                acceptedFiles: '.jpg, .jpeg , .pdf, .txt, .png, .docx, .rtf, .xls, .doc, .msg, .gif',

                // The setting up of the dropzone
                init: function () {
                    var myDropzone = this;
                    var submit = $('#submit6');  // submit button
                    // First change the button to actually tell Dropzone to process the queue.
                    this.element.querySelector("button[type=submit]").addEventListener("click", function (e) {

                        // Make sure that the form isn't actually being sent.
                        e.preventDefault();
                        e.stopPropagation();
                        myDropzone.processQueue();
                    });

                    // Listen to the sending multiple event. In this case, it's the sending multiple event instead
                    // of the sending event because uploadMultiple is set to true.
                    this.on("sendingmultiple", function () {
                        //$('#imageModal').modal('hide');
                        console.log();
                        submit.attr("disabled", true);
                        submit.html('<i class="fa fa-spinner fa-spin"></i> Uploading');
                        NProgress.set();
                        // Gets triggered when the form is actually being sent.
                        // Hide the success button or the complete form.
                    });
                    this.on("successmultiple", function (files, response) {

                        submit.html('<i class="fa fa-check"></i> Upload Successful');
                        submit.attr("disabled", true);

                        // Changes the Progress Bar upon a successful upload.
                        $("#progressbar")
                                .removeClass("progress-bar progress-bar-primary progress-bar-striped active")
                                .addClass("progress-bar progress-bar-striped progress-bar-success");

                        $( "#answer" ).append("Images for Group # {{$id->group_id}} has been successfully uploaded. <br /><br />");
                        $( "#action" ).append( "<button type='button' class='btn btn-success' data-dismiss='modal'>Close</button>" );

                        /**
                         * Redirects the user back to the Booking page immediately
                         * when the modal closes.
                         */

                        $('#progressModal').on('hidden.bs.modal', function () {
                            window.location="{{url('groups/list/'.$id->group_selector)}}";
                        });

                    });

                    this.on("errormultiple", function (files, response ,status) {
                        //console.log(jqXhr);
                        // Gets triggered when there was an error sending the files.
                        // Maybe show form again, and notify user of error

                        NProgress.done();

                        $('#progressModal').modal('show').on('hidden.bs.modal', function () {
                            $("#progressbar").addClass("progress-bar progress-bar-primary progress-bar-striped active");
                            $("#answer").empty();
                            $("#action").empty();
                        });

                        submit.attr("disabled", false);
                        submit.html(' <i class="fa fa-upload"></i> Upload');

                        /*
                         * Progress Bar shows up immediately
                         * when a file returns an error code.
                         *
                         */

                        $("#progressbar").removeClass("progress-bar progress-bar-primary progress-bar-striped active");
                        $("#percent").empty().append("Error");
                        $("#answer").append("<p>" + "Error: " + response, + "<br /></p>");
                        $("#action").append( "<button type='button' class='btn btn-danger' data-dismiss='modal'>Close</button>");

                    });

                    this.on("addedfile", function (file) {

                        // Create the remove button
                        var removeButton = Dropzone.createElement("<button class='btn btn-danger'>Remove File</button>");

                        // Capture the Dropzone instance as closure.
                        var _this = this;

                        // Listen to the click event
                        removeButton.addEventListener("click", function (e) {
                            // Make sure the button click doesn't submit the form:
                            e.preventDefault();
                            e.stopPropagation();

                            // Remove the file preview.
                            _this.removeFile(file);
                            // If you want to the delete the file on the server as well,
                            // you can do the AJAX request here.

                            NProgress.done();

                            $('#progressModal').modal('hide');
                            $("#progressbar").width(0);
                            $("#answer").empty();
                            $("#percent").empty();
                            $("#action").empty();


                        });
                        // Add the button to the file preview element.
                        file.previewElement.appendChild(removeButton);
                    });

                    // File upload Progress
                    this.on("totaluploadprogress", function(progress) {
                        //$("#answer").empty();
                       // $("#action").empty();

                        $('#progressModal').modal('show');

                        // File Percentage
                        TotalProgress(progress);
                    });

                    this.on("queuecomplete", function(progress) {
                        NProgress.done();
                        // console.log("Queue Complete: " + progress);
                    });
                }
            }
        });
    </script>


    {{-- Dropzone JS --}}
    <script type="text/javascript" src="{{ cdn('/js/dropzone.js')}}"></script>
    {{-- Bootbox JS--}}
    <script type="text/javascript" src="https://oss.maxcdn.com/bootbox/4.2.0/bootbox.min.js"></script>

@endsection