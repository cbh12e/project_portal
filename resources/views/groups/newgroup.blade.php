@extends('app')
@section('title','Create Group')
@section('errorBar')@overwrite
@section('content')

    {{-- Page Header --}}
    <section class="content-header">
        <h1><i class="fa fa-users"></i> Create Group <small> All fields with an asterisk <strong>*</strong> are required </small></h1>
        <ol class="breadcrumb">
            <li><a href="{{url('/')}}"><i class="fa fa-dashboard"></i> Home</a></li>
            <li class="active"><i class="fa fa-users"></i> New Group</li>
        </ol>

    </section>

    <br />
    <form class="form-horizontal" method="post" role="form" id="groupForm">
        <fieldset>

            {{-- CSRF --}}
            <input type="hidden" name="_token" value="{{ csrf_token() }}">

            {{-- Associate Select Option--}}
            <div class="form-group {{ $errors->has('associate') ? ' has-error' : '' }}">
                <label class="col-md-3 control-label" for="associate">Associate: *</label>
                <div class="col-md-3">
                    <div class="input-group">
                        <span class="input-group-addon"><i class="fa fa-users"></i></span>
                        <select name="associate" id="associate" class="form-control selectpicker">
                            @foreach(all_agents() as $row)
                                <option value="{{$row->aso_id}}">{{$row->AssociateName}}</option>
                            @endforeach
                        </select>
                    </div>
                </div>
                <div class="col-md-5">
                    @if ($errors->has('associate'))
                        <span class="help-block">
                            <strong>{{ $errors->first('associate') }}</strong>
                        </span>
                    @endif
                </div>
            </div>

            {{-- Group Number --}}
            <div class="form-group {{ $errors->has('groupNum') ? ' has-error' : '' }}">
                <label class="col-md-3 control-label" for="groupNum">Group Number: *</label>

                <div class="col-md-3">
                    <div class="input-group">
                        <span class="input-group-addon"><i class="fa fa-list-ol"></i></span>
                        <input type="text" name="groupNum" id="groupNum"  placeholder="Group Number" value="{{old('groupNum')}}" class="form-control input-md" autocomplete="off"></div>

                    @if ($errors->has('groupNum'))
                        <span class="help-block">
                            <strong>{{ $errors->first('groupNum') }}</strong>
                        </span>
                    @endif
                </div>
            </div>

            {{-- Group Name --}}
            <div class="form-group {{ $errors->has('groupName') ? ' has-error' : '' }}">
                <label class="col-md-3 control-label" for="groupName">Group Name: *</label>
                <div class="col-md-5">
                    <div class="input-group">
                        <span class="input-group-addon"><i class='fa fa-pencil'></i></span>
                        <input type="text" name="groupName" id="groupName"  placeholder="Group Name" value="{{old('groupName')}}" class="form-control input-md" autocomplete="off">

                </div>
                    @if ($errors->has('groupName'))
                        <span class="help-block">
                            <strong>{{ $errors->first('groupName') }}</strong>
                        </span>
                    @endif
                </div>
            </div>

            {{-- Group Ship Name --}}
            <div class="form-group {{ $errors->has('ship') ? ' has-error' : '' }}">
                <label class="col-md-3 control-label" for="ship">Ship: *</label>
                <div class="col-md-4">
                    <div class="input-group">
                        <span class="input-group-addon"><i class="fa fa-ship"></i></span>
                        <input type="text" name="ship" id="ship" class="form-control" placeholder="Ship Name" value="{{old('ship')}}" autocomplete="off"/>
                    </div>
                    @if ($errors->has('ship'))
                        <span class="help-block">
                            <strong>{{ $errors->first('ship') }}</strong>
                        </span>
                    @endif
                </div>
            </div>

            {{-- Group Sail Date --}}
            <div class="form-group {{ $errors->has('date') ? ' has-error' : '' }}">
                <label class="col-md-3 control-label" for="date">Date of Sailing: *</label>
                <div class="col-md-2">
                    <div class="input-group">
                        <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                        <input type="text" name="date" id="bDate" class="form-control" value="{{old('date')}}" placeholder="Sail Date" />
                    </div>
                </div>
            @if ($errors->has('date'))
                    <div class="row">
                    <div class="col-md-5">
                        <span class="help-block">
                            <strong>{{ $errors->first('date') }}</strong>
                        </span>
                    </div>
                    </div>
                    @endif
            </div>

            {{-- Group Notes --}}
            <div class="form-group {{ $errors->has('notes') ? ' has-error' : '' }}">
                <label class="col-md-3 control-label" for="notes">Notes:</label>
                <div class="col-md-4">
                    <textarea name="notes" id="notes" class="form-control" rows="4"></textarea>
                </div>
                @if ($errors->has('notes'))
                    <div class="col-md-5">
                        <span class="help-block">
                            <strong>{{ $errors->first('notes') }}</strong>
                        </span>
                    </div>
                @endif
            </div>

            {{-- Submit Form--}}
            <div class="form-group">
                <label class="col-md-3 control-label" for="submit"></label>
                <div class="col-md-4">
                    <button type="submit" name="submit" id="submit" class="btn btn-primary btn-lg"> Submit</button>
                    <button type="reset" class="btn btn-default btn-lg" >Reset</button>
                </div>
            </div>
        </fieldset>
    </form>
@endsection

@section('javascript')

    {{-- Ship Name Search Typeahead --}}
    @include('layouts.ship-typeahead')

    @include('layouts._datepicker')

    {{-- CSRF TOKEN --}}
    @include('customer.csrf')

@endsection