@extends('app')
@section('title','Create Group Customer')
@section('errorBar')@overwrite
@section('content')
    {{-- Form Name --}}
    <section class="content-header">
        <h1><i class="fa fa-user"></i> Create Group Customer<small> Fields with an asterisk <strong>*</strong> are required.</small></h1>
        <ol class="breadcrumb">
            <li><a href="{{url('/')}}"><i class="fa fa-dashboard"></i> Home</a></li>
            <li class="active"><i class="fa fa-user"></i> Create Customer to Group</li>
        </ol>
    </section>
    <br />
    <form class="form-horizontal" role="form" method="post" id="850">
        <fieldset>
            <legend> Customer Information</legend>
            <p> All fields with an asterisk * are required.</p>
            {{-- CSRF Token --}}
            <input type="hidden" name="_token" value="{{ csrf_token() }}">

            {{-- First Name --}}
            <div class="form-group {{ $errors->has('firstName') ? ' has-error' : '' }}">
                <label class="col-md-3 control-label" for="firstName">Customer First Name: *</label>
                <div class="col-md-5">
                    <div class="input-group">
                        <span class="input-group-addon"><i class='fa fa-user'></i></span>
                        <input type="text" name="firstName" id="firstName" placeholder="First Name" value="{{old('firstName')}}" class="form-control input-md" autocomplete="off" >
                    </div>
                    @if ($errors->has('firstName'))
                        <span class="help-block">
                            <strong>{{ $errors->first('firstName') }}</strong>
                        </span>
                    @endif
                </div>
            </div>

            {{-- Last Name --}}
            <div class="form-group {{ $errors->has('lastName') ? ' has-error' : '' }}">
                <label class="col-md-3 control-label" for="lastName">Customer Last Name: *</label>
                <div class="col-md-5">
                    <div class="input-group">
                        <span class="input-group-addon"><i class='fa fa-user'></i></span>
                        <input type="text" name="lastName" id="lastName" placeholder="Last Name" value="{{old('lastName')}}" class="form-control input-md"  autocomplete="off">
                    </div>
                    @if ($errors->has('lastName'))
                        <span class="help-block">
                            <strong>{{ $errors->first('lastName') }}</strong>
                        </span>
                    @endif
                </div>
            </div>

            {{-- Notes --}}
            <div class="form-group {{ $errors->has('notes') ? ' has-error' : '' }}">
                <label class="col-md-3 control-label" for="notes">Customer Notes:</label>
                <div class="col-md-5">
                    @if(old('notes'))
                        <textarea name="notes" id="notes" class="form-control" rows="5">{{old('notes')}}</textarea>
                    @else
                        <textarea name="notes" id="notes" class="form-control" rows="5"></textarea>
                    @endif
                        @if ($errors->has('notes'))
                            <span class="help-block">
                            <strong>{{ $errors->first('notes') }}</strong>
                        </span>
                        @endif
                </div>
            </div>
        </fieldset>

        <fieldset>
            <legend> Group Information</legend>

            <!-- Group ID-->
            <div class="form-group {{ $errors->has('group') ? ' has-error' : '' }}">
                <label class="col-md-3 control-label" for="group">Group Number: *</label>
                <div class="col-md-5">
                    <div class="input-group">
                        <span class="input-group-addon"><i class="fa fa-list-ol"></i></span>
                        <input type="text" name="group" id="group" class="form-control" placeholder="Group Number" value="{{old('group')}}" autocomplete="off" >
                    </div>
                    @if ($errors->has('group'))
                        <span class="help-block">
                            <strong>{{ $errors->first('group') }}</strong>
                        </span>
                    @endif
                </div>
            </div>

            <!-- Group ID-->
            <div class="form-group {{ $errors->has('reservation') ? ' has-error' : '' }}">
                <label class="col-md-3 control-label" for="reservation">Reservation Number: *</label>
                <div class="col-md-5">
                    <div class="input-group">
                        <span class="input-group-addon"><i class="fa fa-list-ol"></i></span>
                        <input type="text" name="reservation" id="reservation" class="form-control" placeholder="Reservation Number" value="{{old('reservation')}}" autocomplete="off" >
                    </div>
                    @if ($errors->has('reservation'))
                        <span class="help-block">
                            <strong>{{ $errors->first('reservation') }}</strong>
                        </span>
                    @endif
                </div>
            </div>

            {{-- Notes --}}
            <div class="form-group">
                <label class="col-md-3 control-label" for="notesB">Booking Notes: </label>
                <div class="col-md-5">
                    @if(old('notesB'))
                        <textarea name="notesB" id="notesB" class="form-control" rows="5">{{old('notesB')}}</textarea>
                    @else
                        <textarea name="notesB" id="notesB" class="form-control" rows="5"></textarea>
                    @endif
                </div>
                @if ($errors->has('notes'))
                    <span class="help-block">
                            <strong>{{ $errors->first('notesB') }}</strong>
                        </span>
                @endif
            </div>
        </fieldset>
        <fieldset>
            {{-- Submit Button --}}
            <div class="form-group">
                <label class="col-md-4 control-label" for="submit"></label>
                <div class="col-md-4">
                    <button type="submit" name="submit" id="submit" class="btn btn-primary"><i class="fa fa-check"></i> Submit</button>
                    <button type="reset" class="btn btn-default"><i class="fa fa-times"></i> Reset </button>
                </div>
            </div>
        </fieldset>
    </form>

@endsection
@section('javascript')
    @include('groups.typeahead')
@endsection