@extends('app')
@section('title')
{{Session::get('group_status')}} Group List
@endsection
@section('css')
    <link type="text/css" href="{{ cdn('/css/datepicker-override.css') }}" rel="stylesheet">
@endsection
@section('content')
@if(Session::has('flash_message'))
    <div class="alert alert-success">{{Session::get('flash_message')}}</div>
@endif

<section class="content-header">
    <h1><i class="fa fa-users"></i> {{Session::get('group_status')}} Group List</h1>
    <ol class="breadcrumb">
        <li><a href="{{url('/')}}"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active"><i class="fa fa-users"></i> {{Session::get('group_status')}} Group List</li>
    </ol>
</section>
<br />
<div class="container">
    <div class="col-md-12">
        <button class="btn btn-primary" data-toggle="modal" data-target="#createGroupModal"><i class="fa fa-plus"></i> Create New Group</button>
        <hr>
        @if(count($result) > 0)
        <p> Active Groups are automatically moved to the Sailed Group List ten days after the sail date has passed.</p>
        @endif
        <div class="col-md-11">
            @if(count($result) > 0)
            <table class="table table-striped">
                <thead>
                <tr>
                    <th class="hidden-sm hidden-xs">Group Number</th>
                    <th>@sortablelink('group_name','Group Name')</th>
                    <th class="hidden-sm hidden-xs">Associate Name</th>
                    <th>@sortablelink('group_ship','Ship Name')</th>
                    <th class="hidden-sm hidden-xs">@sortablelink('group_date','Sail Date')</th>
                    <th>Action</th>
                </tr>
            </thead>
                <tbody>
                @foreach($result as $record)
                    <tr>
                        <td class="hidden-sm hidden-xs">{{$record->group_number }}</td>
                        <td>{{$record->group_name }}</td>
                        <td class="hidden-sm hidden-xs">{{$record->associate->AssociateName }}</td>
                        <td>{{$record->ship->ship_name }}</td>
                        <td>
                            <span class="hidden-sm hidden-xs">{{human_date($record->group_date) }}</span>
                            <span class="visible-sm visible-xs">{{human_date_mobile($record->group_date) }}</span>
                        </td>
                        <td><a href="{{expire(action('GroupController@showGroup', ['id' => $record->group_selector]))}}" class="btn btn-info"> View</a></td>
                    </tr>
                @endforeach
                </tbody>
            </table>
            <div class="text-right">
                {!! $result->appends(\Input::except('page'))->render() !!}
            </div>
                @else
                {{-- Returns this when the group count returns zero --}}
                <div class="row">
                    <div class="col-md-8 col-md-offset-2">
                        <div class="jumbotron">
                            <div class="container">
                                <h1 class="text-red"><i class="fa fa-warning"></i> Alert</h1>
                                <p> There are currently no groups associated with the selected @if(Session::get('group_status') === 'Associate')associate. @else action.@endif</p>
                            </div>
                        </div>
                    </div>
                </div>
                @endif
        </div>
    </div>
</div>
@include('groups.modal.create-group')
@endsection
@section('javascript')

    {{-- Ship Information Typeahead --}}
    @include('layouts.ship-typeahead')

    {{-- Bootstrap DatePicker File--}}
    @include('layouts._datepicker')

    <!-- Bootbox JS -->
    <script type="text/javascript" src="https://oss.maxcdn.com/bootbox/4.2.0/bootbox.min.js"></script>

    <!-- New Group Modal -->
    <script type="text/javascript">
        $('#newGroupButton').on('click', function() {
        bootbox
        .dialog({
            title: 'Add New Group',
            message: $('#createGroup'),
            show: false // We will show it manually later
        }).on('shown.bs.modal', function() {
        $('#createGroup')
        .show()
    })
    .on('hide.bs.modal', function(e) {
    // Bootbox will remove the modal (including the body which contains the login form)
    // after hiding the modal
    // Therefor, we need to backup the form
    $('').hide().appendTo('body');
    })
    .modal('show');
   // });
    });
    </script>

    <!-- CSRF Token AJAX -->
    @include('customer.csrf')

@endsection