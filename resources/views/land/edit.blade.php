@extends('app')
@section('title')
    Edit Land Provider
@endsection
@section('content')
    <section class="content-header">
        <h1><i class="fa fa-pencil"></i> Edit Land Provider</h1>
        <ol class="breadcrumb">
            <li><a href="{{url('/')}}"><i class="fa fa-dashboard"></i> Home</a></li>
            <li><a href="{{exp_url('land/list')}}"><i class="fa fa-fort-awesome"></i> Land Provider List</a></li>
            <li class="active"><i class="fa fa-pencil"></i> Edit Land Provider</li>

        </ol>
    </section>
    <hr>
    <div class="container">
        <div class="col-md-12">
            <small> All fields with an asterisk * are required. </small>
            <form class="form-horizontal" method="post" action="{{exp_url('land/edit')}}" role="form">
                <fieldset>
                    <legend>Provider Information</legend>

                    {{-- CSRF TOKEN --}}
                    <input type="hidden" name="_token" value="{{ csrf_token() }}">

                    {{-- Land ID --}}
                    <input type="hidden" name="id" value="{{$land->land_id}}">

                    {{-- Provider Name --}}
                    <div class="form-group">
                        <label class="col-md-2 control-label" for="provider"> Provider Name: *</label>
                        <div class="col-md-5">
                            <div class="input-group">
                                <span class="input-group-addon"><i class="fa fa-fort-awesome"></i></span>
                                @if(old('provider'))
                                    <input type="text" name="provider" id="provider" value="{{old('provider')}}" class="form-control">
                                @elseif($land->land_name !== null)
                                    <input type="text" name="provider" id="provider" value="{{$land->land_name}}" class="form-control">
                                @else
                                    <input type="text" name="provider" id="provider" class="form-control">
                                @endif
                            </div>
                        </div>
                    </div>
                    {{-- Provider Notes --}}
                    <div class="form-group">
                        <label class="col-md-2 control-label" for="notes">Provider Notes:</label>
                        <div class="col-md-5">
                            <div class="input-group">
                                <span class="input-group-addon"><i class="fa fa-sticky-note-o"></i></span>
                                @if($land->land_notes !== null)
                                    <textarea name="notes" id="notes" class="form-control" rows="5">{{$land->land_notes}}</textarea>
                                @elseif(old('notes'))
                                    <textarea name="notes" id="notes" class="form-control" rows="5">{{old('notes')}}</textarea>
                                @else
                                    <textarea name="notes" id="notes" class="form-control" rows="5"></textarea>
                                @endif
                            </div>
                        </div>
                    </div>
                </fieldset>

                <fieldset>
                    <!-- Submit Button -->
                    <div class="form-group">
                        <div class="col-md-8 col-md-offset-2">
                            <button type="submit" class="btn btn-primary">Submit Form</button>
                        </div>
                    </div>
                </fieldset>
            </form>
        </div>
    </div>
@endsection