@extends('app')
@section('title')
    Create Provider
@endsection
@section('content')
    <section class="content-header">
        <h1>Create Provider</h1>
        <ol class="breadcrumb">
            <li><a href="{{url('/')}}"><i class="fa fa-dashboard"></i> Home</a></li>
            <li><a href="{{exp_url('land/list')}}"><i class="fa fa-fort-awesome"></i> Land Provider List</a></li>
            <li class="active">Create Provider</li>

        </ol>
    </section>
    <div class="container">
        <div class="col-md-12">
            <br />
            <small> All fields with an asterisk * are required. </small>
            <form class="form-horizontal" method="post" role="form">
                <fieldset>
                    <legend>Provider Information</legend>

                    {{-- CSRF TOKEN--}}
                    <input type="hidden" name="_token" value="{{ csrf_token() }}">

                    {{-- Provider Name --}}
                    <div class="form-group">
                        <label class="col-md-2 control-label" for="provider"> Provider Name: *</label>
                        <div class="col-md-5">
                            <div class="input-group">
                                <span class="input-group-addon"><i class="fa fa-fort-awesome"></i></span>
                                <input type="text" name="provider" id="provider" placeholder="Provider Name" value="{{old('provider')}}" class="form-control" autocomplete="off">
                            </div>
                        </div>
                    </div>

                    {{-- Notes --}}
                    <div class="form-group">
                        <label class="col-md-2 control-label" for="notes">Provider Notes:</label>
                        <div class="col-md-5">
                            <div class="input-group">
                                <span class="input-group-addon"><i class="fa fa-sticky-note-o"></i></span>
                                @if(old('notes'))
                                    <textarea name="notes" id="notes" class="form-control" rows="5">{{old('notes')}}</textarea>
                                @else
                                    <textarea name="notes" id="notes" class="form-control" rows="5"></textarea>
                                @endif
                            </div>
                        </div>
                    </div>
                </fieldset>

                <fieldset>
                    <!-- Submit Button -->
                    <div class="form-group">
                        <div class="col-md-8 col-md-offset-2">
                            <button type="submit" class="btn btn-primary" id="submit1"><i class="fa fa-plus-circle"></i> Submit</button>
                        </div>
                    </div>
                </fieldset>
            </form>
        </div>
    </div>
@endsection