@extends('app')

@section('title', 'Delete Provider')

@section('css')

@endsection

@section('content')
<div class="container">
<div class="col-md-8 col-md-offset-2">
    <form class="form-horizontal" method="post" role="form">

            <div class="jumbotron">

                <h1> Delete Provider</h1>
            <p> Are you sure you want to delete the provider? If clicked by accident, it will be able to be restored by the designated administrator.</p>

            {{-- CSRF TOKEN--}}
            <input type="hidden" name="_token" value="{{ csrf_token() }}">
            <input type="hidden" name="id" value="{{$land->land_id}}">

            <!-- Submit Button -->
            <div class="form-group">
                <div class="col-md-8 col-md-offset-2">
                    <button type="submit" class="btn btn-danger" id="submit1"> Delete</button>
                </div>
            </div>
            </div>
    </form>
</div>
</div>
@endsection

@section('javascript')

@endsection

@section('icheck')

@overwrite