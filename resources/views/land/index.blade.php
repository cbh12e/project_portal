@extends('app')
@section('title')
    Land Provider List
@endsection
@section('css')
    <link type="text/css" href="//cdn.datatables.net/plug-ins/1.10.9/integration/font-awesome/dataTables.fontAwesome.css" rel="stylesheet">
@endsection

@section('content')
    <section class="content-header">
        <h1><i class="fa fa-fort-awesome"></i> Vacation Provider List</h1>
        <ol class="breadcrumb">
            <li><a href="{{url('/')}}"><i class="fa fa-dashboard"></i> Home</a></li>
            <li class="active"><i class="fa fa-fort-awesome"></i> Vacation Provider List</li>
        </ol>
    </section>

    <hr>


    <div class="container">   <a href="{{exp_url('land/create')}}" class="btn btn-primary"><i class="fa fa-plus-circle"></i> Create New Provider</a>
        <hr>
        <div class="col-md-11 col-xs-10">
            <table class="table table-striped table-bordered" id="providerView">
                <thead>
                <tr>
                    <th>Provider Name</th>
                    <th> Action</th>

                </tr>
                </thead>

                <tbody>

                @foreach($provider as $record)
                    <tr>
                        <td>{{$record->land_name}}</td>
                        <td>
                            <a href="{{expire_url_param('land/edit',$record->land_id)}}" class="btn btn-primary"><i class="fa fa-pencil-square"></i> Edit</a>  &nbsp;
                            <a href="{{expire_url_param('land/delete',$record->land_id)}}" class="btn btn-danger"><i class="fa fa-trash"></i> Delete</a>
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
    </div>
    @endsection

    @section('javascript')

        @include('layouts._datatables-js')

    <script type="text/javascript">
        $(function () {
            $('#providerView').dataTable({
                "paging": true,
                "ordering": true,
                "info": true,
                "autoWidth": true
            });
        });
    </script>
@endsection