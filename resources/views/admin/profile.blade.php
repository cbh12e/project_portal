@extends('app')
@section('title','User Profile')
@section('css')
        <!-- Google Recaptica -->
{!! recaptica() !!}
@endsection

@section('content')
    <section class="content-header">
        <div class="page-header">
            <h1><i class="fa fa-user"></i> User Profile of  <small> {{ Auth::user()->name}} </small></h1>
        </div>
    </section>

    <hr>

        <a href="{{expire(action('HomeController@changeImage'))}}" class="btn btn-primary"> <i class="fa fa-image"></i> Change Profile Picture</a>
       <!-- <a href="{{expire(action('UpdateController@viewChangePassword'))}}" class="btn btn-primary disabled"> <i class="fa fa-lock"></i> Change Your Password</a>-->
        @if(Auth::user()->google_2fa_secret == null)
        <button data-toggle="modal" data-target="#enable2Fa" class="btn btn-primary"> <i class="fa fa-qrcode"></i> Enable Two Factor Authentication</button>
        @else
        <a href="{{expire(url('qr'))}}" class="btn btn-info"> <i class="fa fa-qrcode"></i> Get QR Code</a>
        <button type="button" data-toggle="modal" data-target="#disable2Fa" class="btn btn-danger"> <i class="fa fa-trash"></i> Disable Two Factor Authentication</button>
        @endif
    <hr>
<form class="form-horizontal">
    <div class="form-group">
        <div class="col-md-2 col-md-offset-3">
            <img src="{{ url(Auth::user()->photo)}}" class="img-thumbnail img-responsive" alt="User Image" width="100" height="50">
        </div>
    </div>

    <div class="form-group">
        <label class="col-md-3 control-label" style="font-size:22px">Name:</label>
        <div class="col-md-5">
            <p class="form-control-static" style="font-size:22px"> {{ Auth::user()->name}}</p>
        </div>
    </div>

   <div class="form-group">
        <label class="col-md-3 control-label" style="font-size:22px">Username:</label>
        <div class="col-md-2">
            <p class="form-control-static" style="font-size:22px"> {{ Auth::user()->username}} </p>
        </div>
    </div>

 <div class="form-group">
        <label class="col-md-3 control-label" style="font-size:22px">Email:</label>
        <div class="col-md-2">
            <p class="form-control-static" style="font-size:22px"> {{ Auth::user()->email}}</p>
        </div>
    </div>

    <div class="form-group">
        <label class="col-md-3 control-label" style="font-size:22px">Date Created:</label>
        <div class="col-md-2">
            <p class="form-control-static" style="font-size:22px">  {{ date('F d, Y',strtotime( Auth::user()->created_at))}} </p>
        </div>
    </div>
</form>

    {{-- Two Factor Auth Enable or Disable Modals --}}
    @if(Auth::user()->google_2fa_secret !== null)
            <!-- Enable Two Factor -->
    @include('admin.two-factor.disable-modal')
    @else
            <!-- Disable Two Factor -->
    @include('admin.two-factor.enable-modal')
    @endif

@endsection
