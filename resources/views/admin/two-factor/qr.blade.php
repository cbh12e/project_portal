@extends('app')
@section('content')

    <section class="content-header">
        <div class="page-header">
            <h1><i class="fa fa-qrcode"></i> Two Factor Authentication</h1>
        </div>
    </section>

    <div class="col-md-12">
        <p class="lead">
            Congratulations, two-factor authentication has been enabled for your account.
            Scan the barcode shown below to your Two Factor Authenticator Device using an approved App.
        </p>

        <br />
        <div class="text-center">
            <img class="img-responsive" alt="2FA" src="{{$google2fa_url}}"/>
        </div>
        <br />

        <p class="lead"> Apps that can scan the QR Code include Google Authenticator, Authy, and FreeOTP.</p>

    </div>
@endsection