<div class="modal fade" id="enable2Fa" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Enable Two Factor Authentication</h4>
            </div>
            <form class="form-horizontal" method="post" action="{{expire(url('enable2factor')) }}" >
                <div class="modal-body">

                    <p> You are about to enable Two-Factor Authentication. In order to login to your account in the future, you will need to scan the QR code on your cell phone using an approved app. Confirm?  </p>

                    <input type="hidden" name="_token" value="{{ csrf_token() }}" />

                    <!-- Google Recaptcha-->
                    <div class="form-group">
                        <label class="col-md-3 control-label"></label>
                        <div class="col-md-2">
                            <div class="g-recaptcha" data-sitekey="6Le5LAMTAAAAAPdBj1wJLxXid3Qzo-RPk50H-jPN" data-theme="dark"></div>
                        </div>
                    </div>

                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-success">Enable</button>
                    <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>

                </div>
            </form>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->