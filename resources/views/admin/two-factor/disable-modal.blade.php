<div class="modal fade" id="disable2Fa" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title"><i class="fa fa-times-circle"></i> Disable Two Factor Authentication</h4>
            </div>
            <form id="disable2Fa" class="form-horizontal" method="post" action="{{expire(url('disable2factor')) }}" >
                <div class="modal-body">

                    <p> <i class="fa fa-times-circle"></i> You are about to DISABLE Two-Factor Authentication. This action is not recommended unless you replaced your phone and need to regenerate your QR Code.  Continue Anyways?  </p>

                    <input type="hidden" name="_token" value="{{ csrf_token() }}" />

                    <!-- Google Recaptcha-->
                    <div class="form-group">
                        <label class="col-md-3 control-label"></label>
                        <div class="col-md-2">
                            <div class="g-recaptcha" data-sitekey="6Le5LAMTAAAAAPdBj1wJLxXid3Qzo-RPk50H-jPN" data-theme="dark"></div>
                        </div>
                    </div>

                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-danger"><i class="fa fa-times-circle"></i> Disable</button>
                    <button type="button" class="btn btn-success" data-dismiss="modal">Close</button>

                </div>
            </form>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->