@extends('app')

@section('title')
    Change Password
@endsection

@section('css')

    <!-- Google Recaptica -->
    {!! recaptica() !!}
@endsection

@section('content')
    <section class="content-header">
        <div class="page-header">
            <h1><i class="fa fa-user"></i> Change Your Password </h1>
        </div>
    </section>

    <hr>

    <form class="form-horizontal" method="POST">


        <input type="hidden" name="_token" value="{{ csrf_token() }}" />

        <div class="form-group">
            <label class="col-md-4 control-label">Current Password:</label>
            <div class="col-md-6">
                <input type="text" class="form-control" name="currentPassword" value="{{ old('currentPassword') }}">
            </div>
        </div>


        <div class="form-group">
            <label class="col-md-4 control-label">New Password:</label>
            <div class="col-md-6">
                <input type="password" class="form-control" name="password">
            </div>
        </div>

        <div class="form-group">
            <label class="col-md-4 control-label">Confirm New Password:</label>
            <div class="col-md-6">
                <input type="password" class="form-control" name="password_confirmation">
            </div>
        </div>

        <!-- Google Recaptcha-->
        <div class="form-group">
            <label class="col-md-3 control-label"></label>
            <div class="col-md-2">
                <div class="g-recaptcha" data-sitekey="6Le5LAMTAAAAAPdBj1wJLxXid3Qzo-RPk50H-jPN"></div>
            </div>
        </div>

    </form>
@endsection

@section('javascript')
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/cropper/0.11.1/cropper.js"></script>
@endsection