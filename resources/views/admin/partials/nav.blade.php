{{-- TODO: --}}
{{--<div class="input-group">--}}
{{--<input type="text" class="form-control" placeholder="Search...">--}}
{{--<span class="input-group-btn">--}}
{{--<button class="btn btn-default" type="button">--}}
{{--<i class="fa fa-search"></i>--}}
{{--</button>--}}
{{--</span>--}}
{{--</div>--}}

<!-- Left side column. contains the logo and sidebar -->
<aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
        <!-- Sidebar user panel -->
        <div class="user-panel">
            <div class="pull-left image">
                <img src="{{ asset(Auth::user()->photo)}}" class="img-circle" alt="User Image" />
            </div>
            <div class="pull-left info">
                <p>{{ Auth::user()->name }}</p>

                <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
            </div>
        </div>

        <!-- /.search form -->
        <!-- sidebar menu: : style can be found in sidebar.less -->
        <ul class="sidebar-menu">
            <li class="header">BACKEND ADMIN NAVIGATION BAR</li>
            <li class="treeview">

                <ul class="treeview-menu">


            <li><a href="{{ url('admin/users')}}">
                    <i class="fa fa-users"></i> <span>Users </span>
                    <small class="label pull-right bg-red"></small>
                </a>
            </li>
            <!--<li><a href="../../index2.html"><i class="fa fa-circle-o"></i> Dashboard v2</a></li>-->
        </ul>
        </li>

            <li class="{{ (Request::is('admin/dashboard') ? 'active' : '') }}">
                <a href="{{ url('admin/dashboard')}}">
                    <i class="fa fa-dashboard"></i> <span>Backend Dashboard</span>
                    <small class="label pull-right bg-red"></small>
                </a>
            </li>

            <li>
                <a href="{{ url('/')}}">
                    <i class="fa fa-dashboard"></i> <span>Dashboard</span>
                    <small class="label pull-right bg-red"></small>
                </a>
            </li>

            <li class="{{ (Request::is('admin/users') ? 'active' : '') }}">
                <a href="{{ url('admin/users') }}">
                    <i class="fa fa-users"></i> <span>Show Users</span>
                    <small class="label pull-right bg-red"></small>
                </a>
            </li>

            <li class="{{ (Request::is('calendar') ? 'active' : '') }}">
                <a href="{{url('calendar')}}">
                    <i class="fa fa-calendar"></i> <span>Calendar</span>
                    <small class="label pull-right bg-red">0</small>
                </a>
            </li>

            <li class="{{ (Request::is('mail/inbox') ? 'active' : '') }}">
                <a href="{{url('mail/inbox')}}">
                    <i class="fa fa-envelope"></i> <span>Mailbox</span>
                    <small class="label pull-right bg-yellow">0</small>
                </a>
            </li>
            <li class="{{ (Request::is('messages') ? 'active' : '') }}">
                <a href="{{url('messages')}}">
                    <i class="fa fa-comments"></i> Messenger </span>
                    <?php $count = Auth::user()->newMessagesCount(); ?>
                    @if($count > 0)
                        <small class="label pull-right bg-yellow">{!! $count !!}</small>
                    @endif
                </a>
            </li>
        </ul>
    </section>
    <!-- /.sidebar -->
</aside>


