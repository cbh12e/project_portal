@extends('admin.layouts.default')

{{-- Web site Title --}}
@section('title') Admin {{ $title }} @parent @stop

@section('content')

    <!-- Success Alert -->
    @if(session('flash_message'))
        <div class="alert alert-success">
            {{session('flash_message')}}
        </div>
    @endif

    @if (session('success'))
        <div class="alert alert-success">
            {{ session('success') }}
        </div>
    @endif


    <section class="content-header">
        <h1><i class="fa fa-dashboard"></i> Backend Dashboard</h1>
        <ol class="breadcrumb">
            <li><a href="/"><i class="fa fa-dashboard"></i> Home</a></li>
            <li class="active"><i class="fa fa-dashboard"></i> Admin Dashboard</li>
        </ol>
    </section>
    <hr>

    <div class="container">

        <!-- Info boxes -->
        <div class="row">
            <div class="col-md-3 col-sm-6 col-xs-12">
                <div class="small-box bg-green">
                    <div class="inner">
                        <h3>{!! $bookings !!}<sup style="font-size: 20px"></sup></h3>

                        <p>Bookings Stored</p>
                    </div>
                    <div class="icon">
                        <i class="fa fa-book"></i>
                    </div>
                    <a href="{{expire(url('search'))}}" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
                </div>
            </div><!-- /.col -->

            <div class="col-md-4 col-sm-7 col-xs-12">
                <div class="small-box bg-teal">
                    <div class="inner">
                        <h3>{!! $images !!}<sup style="font-size: 20px"></sup></h3>

                        <p>Images stored on the Server</p>
                    </div>
                    <div class="icon">
                        <i class="fa fa-files-o"></i>
                    </div>
                    <a href="{{expire(url('upload'))}}" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
                </div>
            </div><!-- /.col -->

            <!-- fix for small devices only -->
            <div class="clearfix visible-sm-block"></div>
            <div class="col-md-3 col-sm-6 col-xs-12">
                <div class="small-box bg-yellow">
                    <div class="inner">
                        <h3>{!! $records !!}<sup style="font-size: 20px"></sup></h3>

                        <p>Customers Entered</p>
                    </div>
                    <div class="icon">
                        <i class="fa fa-files-o"></i>
                    </div>
                    <a href="{{expire(url('search'))}}" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
                </div>
            </div><!-- /.col -->
        </div><!-- /.row -->

        <div class="row">
            <div class="col-md-3 col-sm-6 col-xs-12">
                <div class="box box-default">
                    <div class="box-header with-border">
                        <h3 class="box-title">Release Notes -- Please read</h3>
                        <div class="box-tools pull-right">
                            <button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                        </div><!-- /.box-tools -->
                    </div><!-- /.box-header -->
                    <div class="box-body collapsed-box" style="display:none">

                        <strong>Version:</strong> 1.4 <br /><strong>Release Date:</strong> 2015-06-21<br />

                        <strong> Release Notes: New Build</strong> <br />

                        Performed a code clean up on the Controllers.
                        <br /><br /> Working on an Messenger App that may be released in Version 1.5 or 1.6.
                        <br /><br /> Upgraded Amazon Web Service SDK PHP from v2 to v3.
                        <br /><br /> Added Breadcrumbs to all the pages.

                        <br /><br /><br />

                        <strong>Version:</strong> 1.2 <br /><strong>Release Date:</strong> 2015-06-16<br />

                        <strong> Release Notes: New Laravel Version 5.1 and Build</strong> <br />

                        Fixed the break of Amazon Web Services PHP SDK in the code with the updating of Laravel 5.0 to Laravel 5.1 <br /><br />
                        Made other changes that were required to the code due to the update to Laravel Version 5.1, which has Long Term Support(LTS). <br />
                        <br />This box will no longer collapse every time the dashboard page has been loaded.

                        <br /><br /><br />

                        <strong>Version:</strong> 1.1 <br /><strong>Release Date:</strong> 2015-06-11<br />

                        <strong> Release Notes: New Build </strong><br />

                        Removed the following fields from the database that was requested from client: (Booking Number, Cabin Number, Customer Street, City, State, and Zip) <br />
                        <br />  Fixed a bug in AJAX that would put all bookings to Mara in the database regardless of which associate was selected. <br />
                        <br /> Added a Feature to the dashboard that shows the amount of files, bookings and customers that are entered into the database. <br /> <br />

                        This is going to be the last bug fixes for sometime since the application will have to be upgraded from Laravel 5.0 to 5.1 and with 5.2 being released in December that will likely break the App and that will need fixes.  <br /> <br />

                        <br />                            <strong>Version:</strong> 1.03 <br /><strong>Release Date:</strong> 2015-06-09<br />

                        <strong> Release Notes: </strong> <br />

                        Added "Google Recaptcha" to the end of the form for both the "Add New Customer" and "New Document Modal" pages. <br /><br />
                        Fixed a bug where it would not remember the user when the user clicked "Remember Me" on the login page. This option has moved to the Two Factor page.<br /><br />
                        For security reasons, "Remember Me" is no longer an option for users that do not have Two Factor Authentication enabled on their account.

                        <br /><br />

                        <strong>Version:</strong> 1.02 <br /><strong>Release Date:</strong> 2015-05-28<br />

                        <strong> Release Notes: </strong> <br />

                        Added Search by Agent per request by client <br />


                        <br /><br />

                        <strong>Version:</strong> 1.01 <br /><strong>Release Date:</strong> 2015-05-27<br />

                        <strong> Release Notes: </strong> <br />

                        Added Two Factor Authentication <br />
                        Added Titles to the Login and Dashboard pages<br />
                        Fixed it so that when the web address is entered, it will go straight to the login page instead of the default Laravel page. <br /><br />
                        Testing has passed for standards of an App being initially released into production.

                        <br /><br />

                        <strong>Version:</strong> 1.0<br /> <strong>Release Date:</strong> 2015-05-25<br />

                        <strong> Release Notes: </strong>Initial Release
                        <br /><br />

                        New Features coming in an Future Update:
                        <br />
                        Advanced Search by Agent Name and sail date<br />
                        Calendar<br />
                        Add Additional Passengers to Bookings and more...<br />
                        <br />

                        Please let Casey Hargarther know of any bugs that are in the application. As a new application, there are bugs such as not being able to edit it when it has been edited once. In that case, refresh the page and proceed to edit it. <br />
                        There are in the works for them, so please be patient.

                        <br />
                        <br />
                        For security and especially for legal purposes, <strong> DO NOT </strong> store any credit card information in forms or files like what was done using <strong>Microsoft SharePoint</strong>. This is not in compliant with <strong>PCI</strong> standards and can ultimately lead up to fines if this system gets hacked. To get around this would to be to store it locally or not store it electronically at all.
                        <br /> <br />This App is written in PHP using Laravel 5, Twitter Bootstrap 3. Storage uses Amazon Amazon Web Service S3.
                    </div>
                </div>
            </div>
            <div class="col-md-4 col-sm-6 col-xs-12">

                <!-- Info Boxes Style 2 -->
                <div class="info-box">
                    <div class="small-box bg-red">
                        <div class="inner">
                            <h3>{!! $users !!}<sup style="font-size: 20px"></sup></h3>

                            <p>Number of Users</p>
                        </div>
                        <div class="icon">
                            <i class="fa fa-users"></i>
                        </div>
                        <a href="#" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
                    </div>
                </div><!-- /.info-box -->

                <div class="small-box bg-yellow">
                    <div class="inner">
                        <h3><span style="font-size: 20px;"> {!! phpVersion1() !!} </span> <sup style="font-size: 10px"></sup></h3>

                        <p>PHP Version</p>
                    </div>
                    <div class="icon">
                        <i class="fa fa-server"></i>
                    </div>
                    <a href="#" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
                </div>
            </div><!-- /.col -->

            <div class="col-md-3 col-sm-6 col-xs-12">
                <div class="small-box bg-aqua">
                    <div class="inner">
                        <h3>{!! $mem_use !!}<sup style="font-size: 20px"></sup></h3>

                        <p>Memory Use</p>
                    </div>
                    <div class="icon">
                        <i class="fa fa-dashboard"></i>
                    </div>
                    <a href="#" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
                </div>

                <div class="small-box bg-orange">
                    <div class="inner">
                        <h3>{{PHP_OS}}<sup style="font-size: 20px"></sup></h3>

                        <p>Server OS</p>
                    </div>
                    <div class="icon">
                        <i class="fa fa-server"></i>
                    </div>
                    <a href="{{expire(url('upload'))}}" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
                </div>
            </div><!-- /.col -->

            <div class="col-md-3 col-sm-6 col-xs-12">
                <div class="small-box bg-red">
                    <div class="inner">
                        <h3>{!! $_SERVER['SERVER_SOFTWARE'] !!}<sup style="font-size: 20px"></sup></h3>

                        <p>Web Server</p>
                    </div>
                    <div class="icon">
                        <i class="fa fa-server"></i>
                    </div>
                    <a href="#" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
                </div>
            </div><!-- /.col -->

            <div class="col-md-4 col-sm-6 col-xs-12">
                <div class="small-box bg-maroon">
                    <div class="inner">
                        <h3>{!! $_SERVER['HTTP_HOST'] !!}<sup style="font-size: 20px"></sup></h3>

                        <p>Hostname</p>
                    </div>
                    <div class="icon">
                        <i class="fa fa-server"></i>
                    </div>
                    <a href="#" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
                </div>
            </div><!-- /.col -->

            <div class="col-md-3 col-sm-6 col-xs-12">
                <div class="small-box bg-olive">
                    <div class="inner">
                        <h3>{!!  App::VERSION() !!}<sup style="font-size: 20px"></sup></h3>

                        <p>Laravel Version</p>
                    </div>
                    <div class="icon">
                        <i class="fa fa-server"></i>
                    </div>
                    <a href="#" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
                </div>
                </div><!-- /.info-box -->
            </div><!-- /.col -->
        </div><!-- /.row -->
@endsection