@extends('app')
@section('title')
Change Profile Picture
@endsection
@section('content')

    @if (count($errors) > 0)
        <div class="alert alert-danger">
            <strong>Whoops!</strong> There were some problems with your input.<br><br>
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif
    <section class="content-header">
        <div class="page-header text-center">
            <h1> <i class="fa fa-image"></i> Change Profile Picture</h1>
        </div>
    </section>
    <hr>
    <div class="container">
    <form class="form-horizontal" method="post" action="{{expire(asset('profile/image'))}}" enctype="multipart/form-data">

        <input type="hidden" name="_token" value="{{ csrf_token() }}"/>

        <div class="form-group">
            <label class="col-md-3 control-label"> Profile Picture: *</label>
                <div class="col-md-2">
                    <input type="file" name="photo">
                </div>
        </div>

    <div class="form-group">
        <div class=" div class col-md-2">
            <button type="submit" class="btn btn-primary"> Submit</button>
            </div>
        </div>
    </form>
    </div>

    @endsection

@section('javascript')

    @endsection