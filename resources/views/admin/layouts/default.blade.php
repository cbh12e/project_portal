@extends('admin.layouts.sidenav')

{{-- Web site Title --}}
@section('title')
    @parent
@endsection

{{-- Styles --}}
@section('styles')
    @parent

    <link href="{{ asset('assets/admin/css/admin.css') }}" rel="stylesheet">

@endsection

{{-- Sidebar --}}

@section('navcolor')
        skin-red
        @overwrite

{{-- Scripts --}}
@section('scripts')
    @parent

    <script src="{{ asset('assets/admin/js/admin.js') }}"></script>

@endsection
