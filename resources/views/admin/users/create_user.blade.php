<!-- Modal -->
<div class="modal fade" id="newUser" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">Create New User</h4>
      </div>
        <form class="form-horizontal" role="form" method="POST" action="{{ expire(url('admin/users/create')) }}">
      <div class="modal-body">

              <input type="hidden" name="_token" value="{{ csrf_token() }}">

              <div class="form-group">
                  <label class="col-md-4 control-label">Name</label>
                  <div class="col-md-6">
                      <input type="text" class="form-control" name="name" value="{{ old('name') }}">
                  </div>
              </div>

              <div class="form-group">
                  <label class="col-md-4 control-label">Username</label>
                  <div class="col-md-6">
                      <input type="text" class="form-control" name="username" value="{{ old('username') }}">
                  </div>
              </div>

              <div class="form-group">
                  <label class="col-md-4 control-label">E-Mail Address</label>
                  <div class="col-md-6">
                      <input type="email" class="form-control" name="email" value="{{ old('email') }}">
                  </div>
              </div>

              <div class="form-group">
                  <label class="col-md-4 control-label">Password</label>
                  <div class="col-md-6">
                      <input type="password" class="form-control" name="password">
                  </div>
              </div>

              <div class="form-group">
                  <label class="col-md-4 control-label">Confirm Password</label>
                  <div class="col-md-6">
                      <input type="password" class="form-control" name="password_confirmation">
                  </div>
              </div>
              <!-- Google Recaptcha-->
              <div class="form-group">
                  <label class="col-md-3 control-label"></label>
                  <div class="col-md-2">
                      <div class="g-recaptcha" data-sitekey="6Le5LAMTAAAAAPdBj1wJLxXid3Qzo-RPk50H-jPN" data-theme="dark"></div>
                  </div>
              </div>
      </div>
              <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
          <button type="submit" class="btn btn-primary">Register</button>
      </div>
        </form>

  </div>
</div>
</div>