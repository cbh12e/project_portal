@extends('admin.layouts.default')

{{-- Web site Title --}}
@section('title')
    Show Users @parent
@stop

{{-- Content --}}

@section('css')
    <!-- Google Recaptica -->
    {!! recaptica() !!}
@endsection

@section('content')


    @if(session('flash_message'))
        <div class="alert alert-success">
            {{session('flash_message')}}
        </div>
    @endif

    <section class="content-header">

        <h1><i class="fa fa-user"></i> <i class="fa fa-users"></i> Users</h1>
        <ol class="breadcrumb">
            <li><a href="{{url('home')}}"><i class="fa fa-dashboard"></i> Home</a></li>
            <li><a href="{{expire(url('admin/dashboard'))}}"><i class="fa fa-dashboard"></i> Dashboard</a></li>
            <li class="active"><i class="fa fa-users"></i> Users</li>
        </ol>

    </section>
<hr>
    <div class="pull-right">
        <button type="button" class="btn btn-primary btn-sm" data-toggle="modal" data-target="#newUser">
            <i class="fa fa-user-plus"></i> Create User </button>
        @if(Auth::user()->id === 1)
            &nbsp;
            <a href="{{url('enveditor')}}" class="btn btn-primary btn-sm"> Edit Configuration </a>
        @endif
    </div>
    <table id="table" class="table table-hover">

        <thead>
        <tr>
        <th class="hidden-xs hidden-sm"></th>
        <th>Name</th>
        <th>Username</th>
        <th class="hidden-xs hidden-sm">Status</th>
        <th class="hidden-xs hidden-sm">E-Mail</th>
        <th class="hidden-xs hidden-sm">Associate Assignment</th>
        <th class="hidden-xs hidden-sm">Two Factor Auth</th>
        <th>Action</th>
        </tr>
        </thead>
        <tbody>
        @foreach($users as $user)
        <tr>
            <td class="hidden-xs hidden-sm"> @if (count($user->photo) === 1) <img src="{{ url($user->photo)}}" class="img-thumbnail img-responsive" alt="{{$user->username}}" width="75" height="25">@endif</td>
            <td>{{ $user->name }}</td>
            <td>{{ $user->username }}</td>
            <td class="hidden-xs hidden-sm">{{ $user->active }}</td>
            <td class="hidden-xs hidden-sm">{{ $user->email }}</td>
            <td class="hidden-xs hidden-sm">{{ $user->active }}</td>
            <td class="hidden-xs hidden-sm">
                @if($user->google_2fa_secret !== null)
                    <span class="label label-success">Enabled</span>
                @else <span class="label label-danger">Disabled</span>
                @endif</td>
            <td><a href="{{expire(action('Admin\UserController@getProfile', ['id' => $user->id]))}}" class="btn btn-primary btn-sm"><i class="fa fa-user"></i> Profile</a>
                <span class="hidden-xs hidden-sm">
            @if($user->id !== 1 && $user->id !== Auth::user()->id)
                <button class="btn btn-danger btn-sm" data-toggle="modal" id="deleteUser" data-id="{{$user->id}}" data-target="#deleteModal"><i class="fa fa-user-times"></i> Delete</button>
                @endif
            </span>
            </td>
        </tr>
        @endforeach

        </tbody>
    </table>

    @include('admin.users.delete_modal')

    @include('admin.users.create_user')

@endsection

{{-- Scripts --}}
@section('javascript')
    <script type="text/javascript">
        $(function() {
            $(document).on('click','#deleteUser',function(e){
                //process here you can get id using
                $('#id').val($(this).data('id')); //and set this id to any hidden field in modal
            });
        });
    </script>

    <!-- DATATABLES SCRIPT -->
    <script type="text/javascript" src="{{asset('js/dataTables.m.js')}}" ></script>
    <script type="text/javascript" src="{{asset('js/dataTables.b.js')}}" ></script>

    <script type="text/javascript">
            $('#table').dataTable({
                "bPaginate": true,
                "bSort": false,
                "bInfo": true,
                "bAutoWidth": true,
                "lengthMenu": [[5, 10, 15, -1], [5, 10, 15, "All"]]
            });

    </script>

@endsection