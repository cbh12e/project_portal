@extends('admin.layouts.default')

@section('title')
    User Profile
@endsection

    @section('content')
        <section class="content-header">
            <div class="page-header">
                <h1><i class="fa fa-user"></i> User Profile of  <small> {{ $user->name}} </small></h1>
            </div>
        </section>

        <hr>

        <form class="form-horizontal">



            <div class="form-group">
                <div class="col-md-2 col-md-offset-3">
                    <img src="{{ url($user->photo)}}" class="img-thumbnail" alt="User Image" width="100" height="50">
                </div>
            </div>

            <div class="form-group">
                <label class="col-md-3 control-label" style="font-size:22px">Name:</label>
                <div class="col-md-5">
                    <p class="form-control-static" style="font-size:22px"> {{ $user->name}}</p>
                </div>
            </div>


            <div class="form-group">
                <label class="col-md-3 control-label" style="font-size:22px">Username:</label>
                <div class="col-md-2">
                    <p class="form-control-static" style="font-size:22px"> {{ $user->username}} </p>
                </div>
            </div>

            <div class="form-group">
                <label class="col-md-3 control-label" style="font-size:22px">Email:</label>
                <div class="col-md-2">
                    <p class="form-control-static" style="font-size:22px"> {{ $user->email}}</p>
                </div>
            </div>

            <div class="form-group">
                <label class="col-md-3 control-label" style="font-size:22px">Date Created:</label>
                <div class="col-md-2">
                    <p class="form-control-static" style="font-size:22px">  {{ date('F d Y',strtotime( $user->created_at))}} </p>
                </div>
            </div>
        </form>
@endsection