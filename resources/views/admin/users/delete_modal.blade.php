<div class="modal fade" id="deleteModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Delete User</h4>
            </div>
            <form id="deleteForm" class="form-horizontal" method="post" action="{{expire(url('admin/users/delete')) }}" >
            <div class="modal-body">

               <p> Warning: Are you sure that you want to delete this user?  </p>

                <input type="hidden" name="_token" value="{{ csrf_token() }}" />

                <input type="hidden" name="id" id="id" />

            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                <button type="submit" class="btn btn-danger">Delete</button>
            </div>
            </form>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->