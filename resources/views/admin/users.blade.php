@extends('app')
@section('title')
Users
@endsection

@section('content')

<table class="table table-bordered">

    <thead>
    <tr>
        <td> Name</td>
        <td> Username</td>
        <td> Email Address</td>
        <td> Last Logged in</td>
        <td> Date Created</td>
        <td> Action</td>
    </tr>
    </thead>
    <tbody>

@foreach($users as $user)
<tr>
    <td>{{$user->name}}</td>
    <td>{{$user->username }}</td>
    <td>{{$user->email}}</td>
    <td>{{$user->updated_at}}</td>
    <td>{{$user->created_at}}</td>
</tr>
    @endforeach
</tbody>
</table>
@endsection