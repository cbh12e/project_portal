@extends('app')
@section('title')
File Manager
@endsection
@section('content')

        {{-- Top Bar --}}
        <section class="content-header">
            <h1> <i class="fa fa-server"></i> File Manager</h1>
            <ol class="breadcrumb">
                <li><a href="{{url('/')}}"><i class="fa fa-dashboard"></i> Home</a></li>
                        @foreach ($breadcrumbs as $path => $disp)
                            <li> <i class="fa fa-folder"></i> <a href="{{expire(asset('upload?folder='.$path))}}">{{ $disp }}</a></li>
                        @endforeach
                        <li class="active"><i class="fa fa-folder-open"></i> {{ $folderName }}</li>
            </ol>
        </section>

        <hr>
           {{-- <div class="text-right">
                <button type="button" class="btn btn-success btn-md" data-toggle="modal" data-target="#modal-folder-create" disabled>
                    <i class="fa fa-plus-circle"></i> New Folder
                </button>
                <button type="button" class="btn btn-primary btn-md" data-toggle="modal" data-target="#modal-file-upload" disabled>
                    <i class="fa fa-upload"></i> Upload File
                </button>
            </div>
        <br /> --}}

        <div class="row">
            <div class="col-md-12">
                <table id="manager" class="table table-striped">
                    <thead>
                    <tr>
                        <th>Name</th>
                        <th>Type</th>
                        <th>Date</th>
                        <th>Size</th>
                        <th>Actions</th>
                    </tr>
                    </thead>
                    <tbody>

                    {{-- The Subfolders --}}
                    @foreach ($subfolders as $path => $name)
                        <tr>
                            <td>
                                <a href="{{expire(asset('upload?folder='.$path))}}"> <i class="fa fa-folder fa-lg fa-fw"></i> {{ $name }} </a>
                            </td>
                            <td>Folder</td>
                            <td>-</td>
                            <td>-</td>
                            <td>
                               {{--- <button type="button" class="btn btn-xs btn-danger" onclick="delete_folder('{{ $name }}')" disabled>
                                    <i class="fa fa-times-circle fa-lg"></i> Delete
                                </button> --}}
                            </td>
                        </tr>
                    @endforeach

                    {{-- The Files --}}
                    @foreach ($files as $file)
                        <tr>
                            <td>
                                <a href="{{ S3($file['s3Path']) }}" target="_blank">
                                    @if (is_image($file['mimeType']))
                                        <i class="fa fa-file-image-o fa-lg fa-fw"></i>
                                    @elseif (is_pdf($file['mimeType']))
                                        <i class="fa fa-file-pdf-o fa-lg fa-fw"></i>
                                    @else
                                        <i class="fa fa-file-o fa-lg fa-fw"></i>
                                    @endif
                                    {{ $file['name'] }}
                                </a>
                            </td>
                                <td>{{ $file['mimeType'] or 'Unknown' }}</td>
                            <td>{{ date('M d, Y h:i a', strtotime($file['modified']))}}</td>
                            <td>{{ human_filesize($file['size']) }}</td>
                            <td>
                            @if (is_pdf($file['mimeType']))
                                @endif
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    @include('admin.upload._modals')
    @endsection
@section('javascript')

    <script type="text/javascript">
/*
      // Confirm file delete
        function delete_file(name) {
            $("#delete-file-name1").html(name);
            $("#delete-file-name2").val(name);
            $("#modal-file-delete").modal("show");
        }

        // Confirm folder delete
        function delete_folder(name) {
            $("#delete-folder-name1").html(name);
            $("#delete-folder-name2").val(name);
            $("#modal-folder-delete").modal("show");
        }

        // Preview image
        function preview_image(path) {
            $("#preview-image").attr("src", path);
            $("#modal-image-view").modal("show");
        }        // Preview image
*/
/*        function preview_pdf(path) {
            $("#preview-pdf").attr("src", path);
            $("#modal-pdf-view").modal("show");
        }*/
/*
        // Startup code
        $(function() {
            $("#uploads-table").DataTable();
        }); /*
    </script>

    <!-- DataTables JS Files -->
    <script src="{{ asset('AdminLTE/plugins/datatables/jquery.dataTables.min.js') }}"></script>
    <script src="{{ asset('AdminLTE/plugins/datatables/dataTables.bootstrap.min.js') }}"></script>

    <script type="text/javascript">
        $(function () {
            $('#manager').dataTable({
                "bPaginate": true,
                "bSort": false,
                "bInfo": true,
                "bAutoWidth": false,
                "lengthMenu": [[20,25,50,75,100, -1], [20,25,50,75,100, "All"]]
            });
        });
    </script>
@endsection