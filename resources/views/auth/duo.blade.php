<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name ="robots" content="noindex, nofollow">

    <title>CruiseOne | Two Factor Authentication</title>

    @if(App::environment('production'))
    <!-- Bootstrap CSS -->
    <link type="text/css" href="{{ cdn('/vendor/bootstrap/css/bootstrap.min.css') }}" rel="stylesheet">
    <!-- AdminLTE CSS -->
    <link type="text/css" href="{{ cdn('/vendor/adminlte/css/AdminLTE.min.css') }}" rel="stylesheet">
    @else
    <!-- Bootstrap CSS -->
    <link type="text/css" href="{{ cdn('/AdminLTE/bootstrap/css/bootstrap.min.css') }}" rel="stylesheet">
    <!-- AdminLTE CSS -->
    <link type="text/css" href="{{ cdn('/AdminLTE/dist/css/AdminLTE.min.css') }}" rel="stylesheet">
    @endif
    <!-- Override CSS -->
    <link type="text/css" href="{{ cdn('/css/overrides.css') }}" rel="stylesheet">
    <!-- Google Fonts CSS -->
    <link type='text/css' href='//fonts.googleapis.com/css?family=Roboto:400,300' rel='stylesheet'>
    <!-- Font Awesome CDN -->
    <script type="text/javascript" src="https://use.fontawesome.com/03581d7396.js"></script>
    <!-- Favicon -->
    <link type="image/ico" rel="shortcut icon" href="{{ asset('/favicon-space-shuttle.ico')}}">

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>
<body class="login-page">
<div class="login-box">
</div><!-- /.login-box -->
<div class="container">
    <div class="col-md-6 col-md-offset-3">
        <form method="post" id="duo_form" role="form">
            <input type="hidden" name="_token" value="{{ csrf_token() }}">
        </form>
        <div class="embed-responsive embed-responsive-4by3">
            <iframe id="duo_iframe" class="embed-responsive-item" frameborder="0" allowtransparency="true" style="background:transparent;"></iframe>
        </div><!-- /.embed-responsive -->
    </div><!-- /.col -->
</div><!-- /.container -->

<@if(App::environment('production'))
        <!-- jQuery 2.2.3 -->
<script type="text/javascript" src="{{ cdn('/vendor/plugins/jQuery/jquery-2.2.3.min.js')}}"></script>
<!-- Bootstrap JS -->
<script type="text/javascript" src="{{ cdn('/vendor/bootstrap/js/bootstrap.min.js')}}"></script>
@else
        <!-- jQuery 2.1.4 -->
<script type="text/javascript" src="{{ cdn('/AdminLTE/plugins/jQuery/jQuery-2.1.4.min.js')}}"></script>
<!-- Bootstrap JS -->
<script type="text/javascript" src="{{ cdn('/AdminLTE/bootstrap/js/bootstrap.min.js')}}"></script>
@endif

<!-- JQuery UI -->
<script type="text/javascript" src="{{ cdn('/AdminLTE/plugins/jQueryUI/jquery-ui.js')}}"></script>
<!-- Duo Security -->
<script type="text/javascript" src="{{ cdn('/js/Duo-Web-v2.js')}}"></script>
<script type="text/javascript">
    Duo.init({
        'host':'{{$duoInfo['HOST']}}',
        'post_action':'{{$duoInfo['POST']}}',
        'sig_request':'{{$duoInfo['SIG']}}'
    });
</script>
</body>
</html>