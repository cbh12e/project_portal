@extends('app')
@section('content')

    <section class="content-header">
        <h1> <i class="fa fa-amazon"></i> Two Factor</h1>
</section>
    <div class="col-md-12">
        <p class="lead">
            Congratulations, your two-factor authentication has been enabled.
            This bar code will not show up ever again, so scan the barcode shown below to your Two Factor Authenticator Device using an approved App.
        </p>

        <div class="text-center">
            <img class="img-responsive" alt="2FA" src="{{$twofa}}"/>
        </div>

    </div>
    @endsection