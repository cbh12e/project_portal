@extends('app')
@section('content')
<div class="login-box">
	<div class="login-logo">
		<b>{{env("GUEST_NAVIGATION_TITLE")}}</b></a>
	</div>
	<!-- /.login-logo -->

	<div class="login-box-body">
		<div class="login-box-msg">Reset Password</div>

		@if(session('success'))

			<div class="alert alert-success">
				{{Session::get('success')}}
			</div>
		@endif

		@if (count($errors) > 0)
			<div class="container">
				<div class="alert alert-danger col-md-3">
					<strong>Whoops!</strong> There were some problems with your input.<br><br>
					<ul>
						@foreach ($errors->all() as $error)
							<li>{{ $error }}</li>
						@endforeach
					</ul>
				</div>
			</div>
			@endif

		@if (session('status'))
			<div class="alert alert-success">
				{{ session('status') }}
			</div>
		@endif

		<form class="form-horizontal" role="form" method="POST" action="{{ url('/password/email') }}">
			<input type="hidden" name="_token" value="{{ csrf_token() }}">
			<div class="form-group">
				<div class="input-group">
					<span class="input-group-addon"> <i class="fa fa-envelope"></i></span>
					<input type="email" class="form-control" name="email" value="{{ old('email') }}" placeholder="E-Mail Address">
				</div>
			</div>
			<div class="form-group">
				<button type="submit" class="btn btn-primary btn-block btn-flat"> Send Password Reset Link</button>
			</div>
		</form>
	</div>
</div>
@endsection