<!DOCTYPE html>
<html lang="en-us">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name ="robots" content="noindex, nofollow">
    <title>CruiseOne | Two Factor Authentication</title>

    <!-- Core CSS-->
    <link type="text/css" href="{{ cdn('AdminLTE/bootstrap/css/bootstrap.min.css') }}" rel="stylesheet">
    <link type="text/css" href="{{ cdn('AdminLTE/dist/css/AdminLTE.min.css') }}" rel="stylesheet">
    <!-- Override CSS -->
    <link type="text/css" href="{{ cdn('css/overrides.css') }}" rel="stylesheet">
    <!-- Google Fonts CSS -->
    <link type='text/css' href='//fonts.googleapis.com/css?family=Roboto:400,300' rel='stylesheet'>
    <!-- Font Awesome CDN -->
    <link type="text/css" href="//maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css" rel="stylesheet">
    <!-- iCheck CSS -->
    <link type="text/css" href="{{ cdn('AdminLTE/plugins/iCheck/all.css') }}" rel="stylesheet">
    <!-- Favicon -->
    <link type="image/ico" rel="shortcut icon" href="{{ asset('/favicon-space-shuttle.ico')}}">

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->

</head>
<body class="login-page">
<div class="login-box">
    <div class="login-logo">
        <b>{{env("GUEST_NAVIGATION_TITLE")}}</b></a>
    </div><!-- /.login-logo -->

    <div class="login-box-body">
        <div class="login-box-msg">Two Factor Authentication has been enabled for your account. Enter your six digit code to proceed.</div>
        <form class="form-horizontal" role="form" action="{{twoFactorSigned(asset('2factor'))}}" method="POST" accept-charset="UTF-8">
            <!-- CSRF Token -->
            <input type="hidden" name="_token" value="{{ csrf_token() }}">
            <!-- Two Factor Auth Code Field-->
            <div class="form-group">
                <div class="input-group">
                    <span class="input-group-addon"> <i class="fa fa-lock"></i></span>
                    <label class="sr-only" for="code">Two Factor Code</label>
                    <input type="tel" class="form-control" name="code" placeholder="Two Factor Code" autocomplete="off">
                </div>
            </div>

            <!-- Remember Me Checkbox -->
            <div class="row">
                <div class="col-xs-8 text-center">
                    <div class="checkbox icheck">
                        <label for="checkbox">
                            <input type="checkbox" name="remember"> Remember Me
                        </label>
                    </div>
                </div>
            </div>

            <br />
            <!-- Submit Button-->
            <div class="form-group">
                <button type="submit" class="btn btn-primary btn-block btn-flat"><i class="fa fa-sign-in"></i> Login</button>
             </div>
        </form>
    </div>
</div>

    <!-- JQuery 2.1.4 .min JS -->
    <script type="text/javascript" src="{{ cdn('AdminLTE/plugins/jQuery/jQuery-2.1.4.min.js')}}"></script>
    <!-- Bootstrap .min JS -->
    <script type="text/javascript" src="{{ cdn('AdminLTE/bootstrap/js/bootstrap.min.js')}}"></script>
    <!-- JQuery UI -->
    <script type="text/javascript" src="{{ cdn('AdminLTE/plugins/jQueryUI/jquery-ui.js')}}"></script>
    <!-- iCheck -->
    <script type="text/javascript" src="{{ cdn('AdminLTE/plugins/iCheck/icheck.min.js')}}"></script>

    <!-- iCheck JS -->
    <script type="text/javascript">
      $(function () {
            $('input').iCheck({
                checkboxClass: 'icheckbox_square-blue',
                radioClass: 'iradio_square-blue',
                increaseArea: '35%' // optional
            });
        });
    </script>
</body>
</html>