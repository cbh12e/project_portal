@extends('app')
@section('title')
Login
@endsection
@section('content')
	<div class="login-box">
				<div class="login-logo">
					<b>{{env("GUEST_NAVIGATION_TITLE")}}</b></a>
				</div><!-- /.login-logo -->

			<div class="login-box-body">
				<div class="login-box-msg">Enter your login information to continue</div>
                        @if(Session::has('flash_message'))
                            <!-- Two Factor Authentication Failure Alert -->
						<div class="alert alert-danger alert-dismissable" role="alert">
							<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
							<strong>Whoops!</strong> There were some problems with your input.<br><br>
							<ul>
								<li>{{Session::get('flash_message')}}</li>
							</ul>
						</div>
                        @endif
                        @if(session('success'))
						<div class="alert alert-success alert-dismissable" role="alert">
							<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
							{{Session::get('success')}}
						</div>
                        @endif
						@if (count($errors) > 0)
					<div class="container">
						<div class="col-md-3">
							<div class="alert alert-danger alert-dismissable" role="alert">
								<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
								<strong>Whoops!</strong> There were some problems with your input.<br><br>
								<ul>
									@foreach ($errors->all() as $error)
									<li>{{ $error }}</li>
									@endforeach
								</ul>
							</div>
						</div>
					</div>
					@endif

					@if(session('failure'))
							<!-- Exception -->
					<div class="container">
						<div class="col-md-3">
							<div class="alert alert-danger alert-dismissable" role="alert">
								<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
								<strong><i class="fa fa-exclamation-circle"></i></strong>
								<p>{{ Session::get('failure') }}</p>
							</div>
						</div>
					</div>
					@endif

					@if(session('suspended'))
							<!-- Exception -->
					<div class="container">
						<div class="col-md-3">
							<div class="alert alert-danger alert-dismissable" role="alert">
								<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
								<strong><i class="fa fa-exclamation-circle"></i></strong>
								<p>{{ Session::get('suspended') }}</p>
							</div>
						</div>
					</div>
					@endif

					@if(session('inactive'))
							<!-- Exception -->
					<div class="container">
						<div class="col-md-3">
							<div class="alert alert-danger alert-dismissable" role="alert">
								<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
								<strong><i class="fa fa-exclamation-circle"></i></strong>
								<p>{{ Session::get('inactive') }}</p>
							</div>
						</div>
					</div>
				@endif
				@if(session('exception'))
							<!-- Exception -->
					<div class="container">
						<div class="col-md-3">
							<div class="alert alert-danger alert-dismissable" role="alert">
								<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
								<strong><i class="fa fa-exclamation-circle"></i></strong>
								<p>{{ Session::get('exception') }}</p>
							</div>
						</div>
					</div>
				@endif

				<form class="form-horizontal" role="form" method="POST" action="{{ exp_url('/auth/login') }}">

					<!-- CSRF Token -->
					<input type="hidden" name="_token" value="{{csrf_token()}}">

					<!-- Username -->
					<div class="form-group">
						<div class="input-group">
							<span class="input-group-addon"> <i class="fa fa-user"></i></span>
							<input type="text" class="form-control" name="username" value="{{old('username')}}" placeholder="Username">
						</div>
					</div>
					<!-- Password -->
					<div class="form-group">
						<div class="input-group">
							<span class="input-group-addon"> <i class="fa fa-lock"></i></span>
							<input type="password" class="form-control" name="password" placeholder="Password">
						</div>
					</div>

					<!-- Login -->
					<div class="form-group">
						<button type="submit" class="btn btn-primary btn-block btn-flat"><i class="fa fa-sign-in"></i> Login</button>
							<div class="text-center">
								<a class="btn btn-link" href="{{ url('/password/email') }}">Forgot Your Password?</a>
							</div>
					</div>
				</form>
			</div>
	</div>
@endsection