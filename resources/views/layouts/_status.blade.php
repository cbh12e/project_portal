@section('errorBar')
    @if (count($errors) > 0)
        <div class="alert alert-danger alert-dismissable" role="alert">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <strong>Whoops!</strong> There were some problems with your input.<br><br>
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
        @endif
        @show

        @if(App::environment('local'))
                <!-- Exception -->
        <div class="alert alert-info" role="alert">
            <p> <strong><i class="fa fa-info-circle"></i></strong>
                This application is currently running in development mode.
            </p>
        </div>
        @endif

        @if(App::environment('stage'))
                <!-- Exception -->
        <div class="alert alert-info" role="alert">
            <p> <strong><i class="fa fa-info-circle"></i></strong>
                This application is currently running in staging mode.
            </p>
        </div>
        @endif

        @if(Agent::browser() === 'Chrome')
                <!-- Internet Browser Message-->
        <div class="alert alert-danger" role="alert">
            <p> <strong><i class="fa fa-times-circle"></i></strong>&nbsp;
                <i class="fa fa-chrome"></i> Google Chrome is currently having problems opening up PDF files in the browser. Use either <i class="fa fa-firefox"> </i> Firefox or <i class="fa fa-internet-explorer"></i> Internet Explorer to open PDF files.
            </p>
        </div>
        @endif

        @if(session('exception'))
                <!-- Exception -->
        <div class="alert alert-danger alert-dismissable" role="alert">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <p> <strong><i class="fa fa-exclamation-circle"></i></strong>
                {{ Session::get('exception') }}
            </p>
        </div>
        @endif

        @if(session('affirmative'))
                <!-- Successful -->
        <div class="alert alert-success alert-dismissable" role="alert">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <p> <strong><i class="fa fa-check-circle"></i></strong>
                {{ Session::get('affirmative') }}
            </p>
        </div>
    @endif

    <noscript>
        <div class="alert alert-warning" role="alert">
            <p> <strong><i class="fa fa-times-circle"></i></strong>
                This web application needs JavaScript to be enabled in order for it to work properly.
            </p>
        </div>
    </noscript>