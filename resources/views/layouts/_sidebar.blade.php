{{-- Left side column. contains the logo and sidebar --}}
<aside class="main-sidebar">
    <section class="sidebar">
        {{-- Sidebar user panel --}}
        <div class="user-panel">
            <div class="pull-left image">
                <img src="{{ asset(Auth::user()->photo)}}" class="img-circle" alt="User Image" />
            </div>
            <div class="pull-left info">
                <p>{{ Auth::user()->name }}</p>
            </div>
        </div>
        {{-- Search Form --}}
        <form action="{{expire(action('Search\LookupController@customerLookup'))}}" method="get" class="sidebar-form" role="search">
            <div class="input-group">
                <label for="bTerm" class="sr-only">Search</label>
                <select class="search_bar form-control" name="q" id="bTerm" style="width: 100%"></select>
                <span class="input-group-btn">
                    <button type='submit' class="btn btn-flat"><i class="fa fa-search"></i></button>
                </span>
            </div>
        </form> {{-- Search Form --}}
        {{-- sidebar menu: : style can be found in sidebar.less --}}
        <ul class="sidebar-menu">
            <li class="header"><strong>MAIN NAVIGATION BAR</strong></li>
                <li class="treeview">
                    {{-- Customer Treeview --}}
                    <a href="#"><i class="fa fa-user"></i> <span>Customer Options</span> <i class="fa fa-angle-left pull-right"></i></a>
                    <ul class="treeview-menu">
                        <li class="{{ active('add') }}">
                            <a href="{{ url('add')}}"><i class="fa fa-circle-o"></i> <span>Create Customer</span></a></li>
                        <li class="{{ active('groups/new') }}">
                            <a href="{{ url('groups/new')}}"><i class="fa fa-circle-o"></i> <span>Create Group Customer</span></a></li>
                        <li><a><i class="fa fa-circle-o"></i> Search by Associate</a></li>
                        <li> <form action="{{exp_url('search/associate')}}" method="GET" role="search" class="sidebar-form">
                                <div class="input-group">
                                    <label class="sr-only" for="associateSidebar">Search by Associate</label>
                                    <select name="a" id="associateSidebar" class="form-control">
                                        @foreach(all_agents() as $agent)
                                            <option value="{{$agent->aso_selector}}">{{$agent->AssociateName}}</option>
                                        @endforeach
                                    </select>
                                    <span class="input-group-btn">
                                        <button type='submit'  class="btn btn-flat"><i class="fa fa-search"></i></button>
                                    </span>
                                </div>
                            </form>
                        </li>
                        <li><a href="{{ url('search')}}"><i class="fa fa-circle-o"></i> <span>Show All Customers</span>
                                @if(count(customer_count()) > 0)
                                <small class="label pull-right bg-blue">{!! customer_count() !!}</small>
                                @endif
                            </a>
                        </li>
                    </ul>
                </li>
                {{-- Group Treeview --}}
            <li class="treeview">
                <a href="#"><i class="fa fa-users"></i> <span>Group Options</span> <i class="fa fa-angle-left pull-right"></i></a>
                <ul class="treeview-menu">
                    <li class="{{ active('groups/add') }}">
                        <a href="{{ url('groups/add')}}"><i class="fa fa-circle-o"></i> <span>Create Group</span>
                            <small class="label pull-right bg-red"></small>
                        </a>
                    </li>
                    <li><a><i class="fa fa-circle-o"></i> Search by Associate</a></li>
                    <li>
                        <form action="{{ expire(URL::action( 'GroupController@associateSearch' ))}}" method="get" class="sidebar-form" role="search">
                            <div class="input-group">
                                <label class="sr-only" for="associateSidebar1">Search by Associate</label>
                                <select name="a" id="associateSidebar1" class="form-control select2" style="width: 100%;">
                                @foreach(all_agents() as $agent)
                                        <option value="{{$agent->aso_selector}}">{{$agent->AssociateName}}</option>
                                    @endforeach
                                </select>
                                <span class="input-group-btn">
                                    <button type='submit'  class="btn btn-flat"><i class="fa fa-search"></i></button>
                                </span>
                            </div>
                        </form>
                    </li>
                    <li class="{{ active('groups/active') }}">
                        <a href="{{ url('groups/active')}}">
                            <i class="fa fa-circle-o"></i> <span>Active Group List</span>
                            @if(count(group_count()) > 0)
                            <small class="label pull-right bg-green-active">{!! group_count() !!}</small>
                            @endif
                        </a>
                    </li>
                    <li class="{{ active('groups/past') }}">
                        <a href="{{ url('groups/past')}}">
                            <i class="fa fa-circle-o"></i> <span>Sailed Group List</span>
                            @if(count(group_sailed_count()) > 0)
                            <small class="label pull-right bg-primary">{!! group_sailed_count() !!}</small>
                            @endif
                        </a>
                    </li>
                    <li class="{{ active('groups/cancelled') }}">
                        <a href="{{ url('groups/cancelled')}}">
                            <i class="fa fa-circle-o"></i> <span>Cancelled Group List</span>
                            @if(count(group_cancel_count()) > 0)
                            <small class="label pull-right bg-red">{!! group_cancel_count() !!}</small>
                            @endif
                        </a>
                    </li>
                </ul>
            </li>
            {{-- Dashboard --}}
            <li class="{{ active('home') }}">
                <a href="{{ url('/')}}">
                    <i class="fa fa-dashboard"></i> <span>@if(Auth::user()->admin == 1) Regular @endif Dashboard</span>
                    <small class="label pull-right bg-red"></small>
                </a>
            </li>
            {{-- Admin Dashboard--}}
            @if(Auth::user()->admin == 1)
                <li class="{{ active('admin/dashboard') }}">
                    <a href="{{ exp_url('admin/dashboard')}}">
                        <i class="fa fa-dashboard"></i> <span> Administrator Dashboard</span>
                        <small class="label pull-right bg-red"></small>
                    </a>
                </li>
            @endif
            {{-- Cruise Line List --}}
            <li class="{{ active('ship') }}">
                <a href="{{exp_url('ship')}}">
                    <i class="fa fa-ship"></i> Cruise Lines List
                </a>
            </li>
            {{-- Land Provider List --}}
            <li class="{{ active('land/list') }}">
                <a href="{{exp_url('land/list')}}">
                    <i class="fa fa-fort-awesome"></i> Land Providers List
                </a>
            </li>
            {{-- File Directory --}}
            <li class="{{ active('upload') }}">
                <a href="{{exp_url('upload')}}">
                    <i class="fa fa-server"></i> File Manager
                </a>
            </li>
            {{-- User Management--}}
            @if(Auth::user()->admin == 1)
                <li class="{{ active('admin/users') }}">
                    <a href="{{ exp_url('admin/users')}}">
                        <i class="fa fa-users"></i> <span>User Management</span>
                        <small class="label pull-right bg-red"></small>
                    </a>
                </li>
            @endif
        </ul>
    </section>{{-- /.sidebar --}}
</aside>