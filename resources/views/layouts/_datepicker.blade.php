{{-- Datepicker --}}
<script type="text/javascript">
    $('#bDate').datepicker({
        startDate: "01/01/{{date("Y")}}",
        endDate: "04/30/{{date("Y") + 3}}",
        todayHighlight: true
    });
</script>