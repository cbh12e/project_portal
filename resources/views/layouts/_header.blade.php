@if(App::environment('production'))
    <!-- Bootstrap CSS -->
    <link type="text/css" href="{{ asset('/vendor/bootstrap/css/bootstrap.min.css') }}" rel="stylesheet">
    <!-- AdminLTE CSS -->
    <link type="text/css" href="{{ asset('/vendor/adminlte/css/AdminLTE.min.css') }}" rel="stylesheet">
@else
    <!-- Bootstrap CSS -->
    <link type="text/css" href="{{ cdn('/AdminLTE/bootstrap/css/bootstrap.min.css') }}" rel="stylesheet">
    <!-- AdminLTE CSS -->
    <link type="text/css" href="{{ cdn('/AdminLTE/dist/css/AdminLTE.min.css') }}" rel="stylesheet">
@endif
    <!-- Font Awesome CDN JS -->
    <script type="text/javascript" src="https://use.fontawesome.com/03581d7396.js"></script>
@if(Auth::guest() === false)
@if(App::environment('production'))
    <!-- AdminLTE Skins -->
    <link type="text/css" href="{{ asset('/vendor/adminlte/css/skins/skin-blue.min.css') }}" rel="stylesheet">
    <link type="text/css" href="{{ asset('/vendor/adminlte/css/skins/skin-red.min.css') }}" rel="stylesheet">
    <!-- iCheck CSS -->
    <link type="text/css" href="{{ asset('/vendor/plugins/iCheck/all.css') }}" rel="stylesheet">
    <!-- Select2 CSS -->
    <link type="text/css" href="{{ asset('/vendor/plugins/select2/select2.min.css')}}" rel="stylesheet" />
@else
        <!-- AdminLTE Skins DEV + STAGE -->
    <link type="text/css" href="{{ asset('/AdminLTE/dist/css/skins/skin-blue.min.css') }}" rel="stylesheet">
    <link type="text/css" href="{{ asset('/AdminLTE/dist/css/skins/skin-red.min.css') }}" rel="stylesheet">
    <!-- iCheck CSS -->
    <link type="text/css" href="{{ asset('/AdminLTE/plugins/iCheck/all.css') }}" rel="stylesheet">
    <!-- Select2 CSS -->
    <link type="text/css" href="{{ asset('/AdminLTE/plugins/select2/select2.min.css')}}" rel="stylesheet" />
@endif
    <!-- Override CSS -->
    <link type="text/css" href="{{ asset('/css/overrides.css') }}" rel="stylesheet">
    <!-- Additional CSS-->
    <link type="text/css" href="{{ asset('/css/typeahead.css') }}" rel="stylesheet">
    <!-- Select2 CSS Stylesheet -->
    <link type="text/css" href="{{ asset('/css/select2-bootstrap.css') }}" rel="stylesheet">
    <!-- NProgress CSS -->
    <link type="text/css" href='{{ asset('/css/nprogress.css')}}' rel="stylesheet">
    <!-- Bootstrap Datepicker CSS -->
    <link type="text/css" href="{{ asset('/css/datepicker/bootstrap-datepicker.css') }}" rel="stylesheet">
    <!-- Alertify JS CSS -->
    <link type="text/css" href="{{ asset('/css/alertify/alertify.css') }}" rel="stylesheet">
    <!-- Bootstrap Select -->
    <link type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.11.0/css/bootstrap-select.min.css" rel="stylesheet">
    <!-- NProgress JS File-->
    <script type="text/javascript" src="{{ asset('/js/nprogress.js') }}"></script>
@endif