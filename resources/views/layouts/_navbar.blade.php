<header class="main-header">
    <!-- Logo -->
    <a href="{{ url('/') }}" class="logo">{{env('APP_TITLE')}}</a>
    <!-- Header Navbar: style can be found in header.less -->
    <nav class="navbar navbar-static-top" role="navigation">
        <!-- Sidebar toggle button-->
        <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
            <span class="sr-only">Toggle navigation</span>
        </a>
        <!-- Collect the nav links, forms, and other content for toggling -->
        <div class="collapse navbar-collapse pull-left" id="navbar-collapse">
            <ul class="nav navbar-nav">
            </ul>
            <form class="navbar-form navbar-left" role="search" method="get" action="{{expire(action('Search\LookupController@reservationLookup'))}}">
                <div class="form-group">
                    <div class="input-group">
                        <label class="sr-only" for="rLookup">Reservation Search</label>
                        <select class="navbar-search-input form-control rTerm" name="r" id="rLookup" style="width: 350px;"></select>
                        <span class="input-group-btn">
                        <button type='submit' class="btn btn-flat"><i class="fa fa-search"></i></button>
                    </span>
                    </div>
                </div>
            </form>
        </div><!-- /.navbar-collapse -->
        <div class="navbar-custom-menu">
            <ul class="nav navbar-nav">
                <!-- User Account: style can be found in dropdown.less -->
                <li class="dropdown user user-menu">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                        <img src="{{ asset(Auth::user()->photo)}}" class="user-image" alt="User Image"/>
                        <span class="hidden-xs">{{ Auth::user()->name }}</span>
                    </a>
                    <ul class="dropdown-menu">
                        <!-- User image -->
                        <li class="user-header">
                            <img src="{{ asset(Auth::user()->photo)}}" class="img-circle" alt="User Image" />
                            <p>
                                {{ Auth::user()->name }}
                            </p>
                        </li>
                        <!-- Menu Body -->
                        <li class="user-body">
                        </li>
                        <!-- Menu Footer-->
                        <li class="user-footer">
                            <div class="pull-left">
                                <a href="{{ exp_url('user/profile')}}" class="btn btn-default btn-flat"><i class="fa fa-user"></i> Profile</a>
                            </div>
                            <div class="pull-right">
                                <a href="{{ url('/auth/logout') }}" class="btn btn-default btn-flat"><i class="fa fa-sign-out"></i> Sign out</a>
                            </div>
                        </li>
                    </ul>
                </li>
            </ul>
        </div>
    </nav>
</header>