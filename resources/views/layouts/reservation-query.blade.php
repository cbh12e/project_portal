{{-- Reservation Search Select2 --}}
<script type="text/javascript">
    $(".rTerm").select2({
        ajax: {
            url: "{{expire(url('search/reservations/q'))}}",
            dataType: 'json',
            delay: 250,
            data: function (params) {
                return {
                    term: params.term, // search term
                    page: params.page
                };
            },
            processResults: function (data, page) {
            // parse the results into the format expected by Select2.
            // since we are using custom formatting functions we do not need to
            // alter the remote JSON data
                return {
                    maximumSelectionLength: 5,
                    results: $.map(data, function (item) {
                        return {
                            text: item.reservation + ': ' + item.name1 + ' - ' + item.date1,
                            // slug: item.slug,
                            id: item.bok_id
                        }
                    })
                };
            },
            cache: true
        },
        escapeMarkup: function (markup) { return markup; }, // let our custom formatter work
        placeholder: "Reservation Search",
        minimumInputLength: 3,
        theme: "bootstrap"
    });

</script>