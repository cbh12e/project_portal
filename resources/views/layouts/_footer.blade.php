<!-- Footer -->     
<footer class="main-footer">
    <div class="pull-right hidden-xs">
        <b>Version</b>
        @if(App::environment('local') === true)
            Project Tallahassee
        @endif
            1.4.5
    </div>
    <strong>Copyright <i class="fa fa-copyright"></i> 2015-{{date('Y') }}  {{env('APP_FOOTER_TITLE')}} </strong> <small> &nbsp; | {{env('APP_FOOTER_TITLE_B')}}</small> &nbsp; &nbsp;  All rights reserved.
</footer>
