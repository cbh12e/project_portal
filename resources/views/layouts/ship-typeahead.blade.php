<script type="text/javascript" src="{{cdn('/js/typeahead-a.js')}}"></script>

{{-- Ship Name Information Typeahead --}}
<script type="text/javascript">
    jQuery(document).ready(function($) {
        var engine = new Bloodhound({
            datumTokenizer: Bloodhound.tokenizers.whitespace('ship'), {{-- DISPLAYS THE VALUE OF WHAT WILL BE ENTERED IN INPUT--}}
            queryTokenizer: Bloodhound.tokenizers.whitespace,
            remote:
            {
                url: '{{typeahead('tallahassee/query?ship=%QUERY')}}', {{-- URL NEEDED TO GET TO RUN THE QUERY --}}
                wildcard: '%QUERY' {{-- WILDCARD MUST BE THIS WAY OR TYPEAHEAD WILL NOT WORK--}}
            }
        });
        engine.initialize();

        $("#ship").typeahead({
            hint: true,
            highlight: true,
            minLength: 2,
            limit: 4
        }, {
            source: engine.ttAdapter(),
            // This will be appended to "tt-dataset-" to form the class name of the suggestion menu.
            name: 'ship',
            // the key from the array we want to display (name,id,email,etc...)
            displayKey: 'ship', {{-- INPUT VALUE OF THE FORM--}}
            templates: {

                empty: [
                    '<div class="user-search-result">&nbsp;<i class="fa fa-warning" style="color: red;"></i>&nbsp; No ships were found</div>'
                ].join('\n'),
                {{-- Suggestion bar displaying extra inputs --}}
                suggestion: function (data) {
                    return '<div class="user-search-result">'+'<img class="img-responsive img-thumbnail" alt="Logo" src="'+ data.logo + '"/>' +'&nbsp;' + data.ship +  '</div>'
                }
            }
        });
    });
</script>

{{-- Land Provider Information Typeahead --}}
<script type="text/javascript">
    jQuery(document).ready(function($) {
        var engine = new Bloodhound({
            datumTokenizer: Bloodhound.tokenizers.whitespace('vacation_pvr'), {{-- DISPLAYS THE VALUE OF WHAT WILL BE ENTERED IN INPUT--}}
            queryTokenizer: Bloodhound.tokenizers.whitespace,
            remote:
            {
                url: '{{typeahead('monticello/query?land=%QUERY')}}', {{-- URL NEEDED TO GET TO RUN THE QUERY --}}
                wildcard: '%QUERY' {{-- WILDCARD MUST BE THIS WAY OR TYPEAHEAD WILL NOT WORK--}}
            }
        });
        engine.initialize();

        $("#landT").typeahead({
            hint: true,
            highlight: true,
            minLength: 2,
            limit: 4
        },
                {
                    source: engine.ttAdapter(), // This will be appended to "tt-dataset-" to form the class name of the suggestion menu.
                    name: 'landT', // the key from the array we want to display (name,id,email,etc...)
                    displayKey: 'vacation_pvr', {{-- INPUT VALUE OF THE FORM--}}
                templates: {
                    empty: [
                        '<div class="user-search-result"> &nbsp; <i class="fa fa-warning" style="color: red;"></i> &nbsp; No vacation providers were found</div>'
                    ].join('\n'), {{-- Suggestion bar displaying extra inputs --}}
                    suggestion: function (data) {
                        return '<div class="user-search-result">'+ ' ' +data.vacation_pvr +  '</div>'
                    }
                }
                });
    });
</script>