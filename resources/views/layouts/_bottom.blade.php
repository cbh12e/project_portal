@if (Auth::guest())
@if(App::environment('production'))
    <!-- jQuery 2.2.3 -->
    <script type="text/javascript" src="{{ asset('/vendor/plugins/jQuery/jquery-2.2.3.min.js')}}"></script>
    <!-- Bootstrap JS -->
    <script type="text/javascript" src="{{ asset('/vendor/bootstrap/js/bootstrap.min.js')}}"></script>
    @else
    <!-- jQuery 2.1.4 -->
    <script type="text/javascript" src="{{ asset('/AdminLTE/plugins/jQuery/jQuery-2.1.4.min.js')}}"></script>
    <!-- Bootstrap JS -->
    <script type="text/javascript" src="{{ asset('/AdminLTE/bootstrap/js/bootstrap.min.js')}}"></script>
@endif
@else
@if(App::environment('production'))
    {{-- PRODUCTION ENVIRONMENT --}}
    <!-- jQuery 2.1.4 -->
    <script type="text/javascript" src="{{ asset('/AdminLTE/plugins/jQuery/jquery-2.2.3.min.js')}}"></script>
    <!-- Bootstrap 3.3.6 JS -->
    <script type="text/javascript" src="{{ asset('/vendor/bootstrap/js/bootstrap.min.js')}}"></script>
    <!-- JQuery UI -->
    <script type="text/javascript" src="{{ asset('/vendor/plugins/jQueryUI/jquery-ui.js')}}"></script>
    <!-- Bootstrap Datepicker -->
    <script type="text/javascript" src="{{ asset('/js/datepicker/bootstrap-datepicker.js') }}"></script>
    <!-- Select2 -->
    <script type="text/javascript" src="{{ asset('/vendor/plugins/select2/select2.min.js')}}"></script>
    <!-- Alertify JS -->
    <script type="text/javascript" src="{{ asset('/js/alertify.js') }}"></script>
    <!-- iCheck -->
    <script type="text/javascript" src="{{ asset('/vendor/plugins/iCheck/icheck.min.js')}}"></script>
    <!-- SlimScroll 1.3.0 -->
    <script type="text/javascript" src="{{ asset('/vendor/plugins/slimScroll/jquery.slimscroll.min.js')}}" ></script>
@else
    {{-- DEVELOPMENT ENVIRONMENT --}}
    <!-- jQuery 2.2.3 -->
    <script type="text/javascript" src="{{ cdn('/AdminLTE/plugins/jQuery/jQuery-2.1.4.min.js')}}"></script>
    <!-- Bootstrap JS -->
    <script type="text/javascript" src="{{ cdn('/AdminLTE/bootstrap/js/bootstrap.min.js')}}"></script>
    <!-- JQuery UI -->
    <script type="text/javascript" src="{{ cdn('/AdminLTE/plugins/jQueryUI/jquery-ui.js')}}"></script>
    <!-- Bootstrap Datepicker -->
    <script type="text/javascript" src="{{ cdn('/js/datepicker/bootstrap-datepicker.js') }}"></script>
    <!-- Select2 -->
    <script type="text/javascript" src="{{ cdn('/AdminLTE/plugins/select2/select2.min.js')}}"></script>
    <!-- Alertify JS -->
    <script type="text/javascript" src="{{ cdn('/js/alertify.js') }}"></script>
    <!-- iCheck -->
    <script type="text/javascript" src="{{ asset('/AdminLTE/plugins/iCheck/icheck.min.js')}}"></script>
    <!-- SlimScroll 1.3.0 -->
    <script type="text/javascript" src="{{ cdn('/AdminLTE/plugins/slimScroll/jquery.slimscroll.min.js')}}" ></script>

@endif
    <!-- Laravel Javascript Validation -->
    <script type="text/javascript" src="{{ asset('/vendor/jsvalidation/js/jsvalidation.js')}}"></script>
@yield('validation')
    <!-- Latest compiled and minified JavaScript -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.11.0/js/bootstrap-select.min.js"></script>
    {{-- Typeahead Layouts JS --}}
@include('layouts.typeahead')
@include ('layouts.reservation-query')
@section('icheck')

    <script type="text/javascript">
        /**
         * Overrides the default Radio Check Bars
         */
        $('input[type="checkbox"].minimal-blue, input[type="radio"].minimal-blue').iCheck({
            checkboxClass: 'icheckbox_minimal-blue',
            radioClass: 'iradio_minimal-blue'
        });
    </script>
    @show

            <!-- Alertify Selection JS -->
    <script type="text/javascript" src="{{asset('/js/selection-overrides.js')}}"></script>
    @endif
    @yield('javascript')
    @if(App::environment('production'))
        <script type="text/javascript" src="{{ asset('/vendor/adminlte/js/app.min.js') }}"></script>
        @else
                <!-- AdminLTE App JS -->
        <script type="text/javascript" src="{{ asset('/AdminLTE/dist/js/app.min.js') }}"></script>
    @endif