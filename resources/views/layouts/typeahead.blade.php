{{-- Customer Search Select2 --}}
<script type="text/javascript">
    $("#bTerm").select2({
        ajax: {
            url: "{{expire(url('search/select/query'))}}",
            dataType: 'json',
            delay: 250,
            data: function (params) {
                return {
                    term: params.term, // search term
                    page: params.page
                };
            },
            processResults: function (data, page) {
            // parse the results into the format expected by Select2.
            // since we are using custom formatting functions we do not need to
            // alter the remote JSON data
            return {
                results: $.map(data, function (item) {
                    return {
                        text: item.customer,
                        // slug: item.slug,
                        id: item.identifier
                    }
                })
            };
            },
            cache: true
        },
        escapeMarkup: function (markup) { return markup; }, // let our custom formatter work
        placeholder: "Customer Search",
        minimumInputLength: 2,
        theme: "bootstrap"
        //templateResult: formatRepo, // omitted for brevity, see the source of this page
        //templateSelection: formatRepoSelection // omitted for brevity, see the source of this page
        });
</script>