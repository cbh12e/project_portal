@extends('app')
@section('title')
500 Error
@endsection
@section('content')
<div class="error-page">
    <h2 class="headline text-red">500</h2>
    <div class="error-content">
        <h3><i class="fa fa-warning text-red"></i> Oops! Something went wrong.</h3>
        <p>
            We will work on fixing that right away.
            Meanwhile, you may <a href='../../index.html'>return to dashboard</a> or try using the search form.
        </p>

        <form class='search-form' action="{{asset('search/customer/')}}" method="get">
            <div class='input-group'>
                <input type="text" name="term" class='form-control' placeholder="Search"/>
                <input type="hidden" name="_token" value="{{ csrf_token() }}"/>
                <div class="input-group-btn">
                    <button type="submit" class="btn btn-warning btn-flat"><i class="fa fa-search"></i></button>
                </div>
            </div><!-- /.input-group -->
        </form>
    </div><!-- /.error-content -->
</div><!-- /.error-page -->
@endsection