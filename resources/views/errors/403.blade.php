@extends('app')
@section('title')
403 - Forbidden
@endsection
@section('content')
    <div class="jumbotron">
        <div class="error-page">
            <div class="text-center">
                <h2 class="headline text-red" style="font-size: 75px;"><i class="fa fa-lock fa-2x"></i> <br /> 403 - Forbidden</h2>
                <div class="error-content">
                    <h3> Either your timestamp expired or you do not have the proper permissions required to access this page.</h3>
                </div><!-- /.text-center -->
            </div><!-- /.error-content -->
        </div><!-- /.error-page -->
    </div>
@endsection