@extends('app')
@section('title')
404 Error
@endsection
@section('content')
<div class="error-page">
    @if (session('404'))
        <div class="jumbotron">
            <div class="container">
                <h1 class="text-yellow text-center"><i class="fa fa-warning fa-2x"></i> <br />404</h1>
                <h1 class="text-center"> Page not found.</h1> <br />
                <!-- Not Found Alert -->
                <p class="lead">
                    {{ Session::get('404') }}
                </p>
            </div>
        </div>
    @elseif(Auth::guest())
        <div class="jumbotron">
            <div class="container">
                <h1 class="text-yellow text-center"><i class="fa fa-warning fa-2x"></i> <br />404</h1>
                <h1 class="text-center"> Page was not found.</h1> <br />
                <!-- Not Found Alert -->
                <p class="lead">
                    We could not find the page you were looking for.
                    Meanwhile, you may <a href='{{asset('/')}}'>return to the home page.</a>
                </p>
            </div>
        </div>
        @else
        <div class="jumbotron">
            <div class="container">
                <h1 class="text-yellow text-center"><i class="fa fa-warning fa-3x"></i> <br />404</h1>
                <h1 class="text-center"> Page Not Found.</h1> <br />
                <!-- Not Found Alert -->
                <p class="lead">
                    We could not find the page you were looking for.
                    Meanwhile, you may <a href='{{asset('/')}}'>return to the dashboard.</a>
                </p>
            </div>
        </div>
    @endif
</div><!-- /.error-page -->
@endsection