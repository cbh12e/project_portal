@extends('app')
@section('title','File Upload')
@section('css')
    <link type="text/css" href="{{ asset('/css/dropzone.css') }}" rel="stylesheet">
    @endsection
@section('content')
<div class="content-header">
    <h4><i class="fa fa-file-pdf-o"></i> File Drop for Booking #{{$id->bok_id }}</h4>
    <ol class="breadcrumb">
        <li><a href="{{url('/')}}"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="{{expire(url('search/booking/'.$id->bok_selector))}}"><i class="fa fa-ship"></i> Booking # {{$id->bok_id}}</a></li>
        <li class="active"><i class="fa fa-upload"></i> File Upload</li>
    </ol>
</div>
    @if(App::environment('local'))
    <!-- Button used for testing -->
    <button id="dev" class="btn btn-warning" data-toggle="modal" data-target="#progressModal"> Progress Bar Modal</button>
    @endif

<a class="btn btn-primary" href="{{expire(url('search/booking/'.$id->bok_selector))}}"><i class="fa fa-ship"></i> Back to Booking</a>

    <form class="form-horizontal dropzone" method="post" role="form" action="{{action('Amazon\UploadController@s3ImageRequest')}}" enctype="multipart/form-data" id="my-awesome-dropzone">
        <div class="col-md-12">
            <!-- CSRF Token -->
            <input type="hidden" name="_token" value="{{ csrf_token() }}">

            <!-- Booking ID -->
            <input type="hidden" name="id" id="ID" value="{{$id->bok_id }}">

            <!-- Notes -->
            <small> All fields with an asterisk * are required! Do not store CC Info. <br /> Single File Upload Limit: {!! fileLimit() !!} MB - Combined Upload Limit: {!! totalLimit() !!} MB</small> <br /><br />

            <!-- Category -->
            <div class="form-group">
                <label class="col-md-3 control-label"> File Category: *</label>
                <div class="col-md-8">
                    @foreach($record as $category)
                    <label>
                        <input type="radio" name="category"  value="{{$category->cat_id}}" class="minimal-blue"/> {{$category->cat_name}}
                    </label>
                    @endforeach
                </div>
            </div>

            <!-- Notes -->
            <div class="form-group">
                <label class="col-md-3 control-label" for="notes"> Notes: </label>
                <div class="col-md-7">
                    <textarea class="form-control" name="notes" id="notes" rows="3"></textarea>
                </div>
            </div>
        </div>

        <!-- Submit -->
        <div class="form-group">
            <div class="col-md-7 col-md-offset-3">
                <button type="submit" id="submit6" class="btn btn-primary"><i class="fa fa-upload"></i> Upload File</button>
            </div>
        </div>
    </form>

    @include('modals.progress')
@endsection

@section('javascript')

           <!-- File Upload Percent JS-->
    @include('file.percent')

    <!-- File Upload Dropzone -->
    <script type="text/javascript">
        // Dropzone
        $(document).ready(function() {

            Dropzone.options.myAwesomeDropzone = { // The camelized version of the ID of the form element

                // The configuration we've talked about above
                autoProcessQueue: false,
                uploadMultiple: true,
                parallelUploads: 100,
                maxFiles: 6,
                maxFilesize: {!! totalLimit() !!},
                paramName: "file",
                acceptedFiles: '.jpg, .jpeg , .pdf, .txt, .png, .docx, .rtf, .xls, .doc, .msg, .gif',

                // The setting up of the dropzone
                init: function () {
                    var myDropzone = this;
                    var submit = $('#submit6');  // submit button
                    // First change the button to actually tell Dropzone to process the queue.
                    this.element.querySelector("button[type=submit]").addEventListener("click", function (e) {
                        // Make sure that the form isn't actually being sent.
                        e.preventDefault();
                        e.stopPropagation();
                        myDropzone.processQueue();
                    });
                    // Listen to the sending multiple event. In this case, it's the sending multiple event instead
                    // of the sending event because uploadMultiple is set to true.
                    this.on("sendingmultiple", function () {
                        //$('#imageModal').modal('hide');
                        console.log();
                        submit.attr("disabled", true);
                        submit.html('<i class="fa fa-spinner fa-spin"></i> Uploading');
                        // Gets triggered when the form is actually being sent.
                        // Hide the success button or the complete form.
                    });
                    this.on("successmultiple", function (files, response) {

                        //NProgress.set(0.99);
                        //console.log("Success Files: " + files,"Success Response: " + response);

                        submit.html('<i class="fa fa-check"></i> Upload Successful');
                        submit.attr("disabled", true);
                       // alertify.defaults.glossary.title = "Alert";
                        //alertify.alert("Images for Booking #{{$id->bok_id}} have been successfully uploaded.").set('onok', function(closeEvent){NProgress.done(); window.location.reload();} );

                        // Changes the Progress Bar upon a successful upload.
                        $("#progressbar")
                                .removeClass("progress-bar progress-bar-primary progress-bar-striped active")
                                .addClass("progress-bar progress-bar-success progress-bar-striped");
                        $( "#answer" ).append("Images for Booking # {{$id->bok_id}} has been successfully uploaded. <br /><br />");
                        $( "#action" ).append(
                                "<button type='button' class='btn btn-primary' data-dismiss='modal'> <i class='fa fa-upload'></i> Upload More Files</button>",
                                "<a  href='{{expire(url('search/booking/'.$id->bok_selector))}}' class='btn btn-success'> Return to Booking</a>"
                        );
                        $('#progressModal').on('hidden.bs.modal', function () {
                            /**
                             * This allows the user to load even more files after the first file was
                             * loaded instead of going back to click to upload.
                             */

                                        $("#progressbar").addClass("progress-bar progress-bar-primary progress-bar-striped active");
                                        $("#answer").empty();
                                        $("#action").empty();
                                    });

                            submit.attr("disabled", false);
                            submit.html(' <i class="fa fa-upload"></i> Upload File');
                        });


                    this.on("errormultiple", function (files, response ,status) {
                        //console.log(jqXhr);
                        // Gets triggered when there was an error sending the files.
                        // Maybe show form again, and notify user of error

                        NProgress.done();

                        $('#progressModal').modal('show')
                                .on('hidden.bs.modal', function () {
                                    /**
                                     * Reverts jQuery back to defaults
                                     * to allow a reattempt upload.
                                     *
                                     */
                            $("#progressbar").addClass("progress-bar progress-bar-primary progress-bar-striped active");
                            $("#answer").empty();
                            $("#action").empty();
                        });

                        submit.attr("disabled", false);
                        submit.html(' <i class="fa fa-upload"></i> Upload File');

                        /**
                         * Progress Bar shows up immediately when a file returns an error code.
                         *
                         */
                        $("#progressbar").removeClass("progress-bar progress-bar-primary progress-bar-striped active");
                        $("#percent").empty().append("Error");
                        $("#answer").append("<p>" + "Error: " + response, + "<br /></p>");
                        $("#action").append( "<button type='button' class='btn btn-danger' data-dismiss='modal'>Close</button>");

                    });

                    this.on("addedfile", function (file) {

                        // Create the remove button
                        var removeButton = Dropzone.createElement("<button class='btn btn-danger'>Remove File</button>");

                        // Capture the Dropzone instance as closure.
                        var _this = this;

                        // Listen to the click event
                        removeButton.addEventListener("click", function (e) {

                            // Make sure the button click doesn't submit the form:
                            e.preventDefault();
                            // Stops Propagation
                            e.stopPropagation();
                            // Remove the file preview.
                           _this.removeFile(file);

                            // Finishes NProgress
                            NProgress.done();

                            // Clear the progress bar to allow uploads with proper files
                            $('#progressModal').modal('hide');
                            $("#progressbar").width(0);
                            $("#answer").empty();
                            $("#percent").empty();
                            $("#action").empty();

                            // If you want to the delete the file on the server as well,
                            // you can do the AJAX request here.
                        });
                        // Add the button to the file preview element.
                        file.previewElement.appendChild(removeButton);
                    });

                    // File upload Progress
                    this.on("totaluploadprogress", function(progress) {
                        $('#progressModal').modal('show');
                        TotalProgress(progress);
                    });

                    this.on("queuecomplete", function(progress) {
                       NProgress.done();
                       // console.log("Queue Complete: " + progress);
                    });
                }
            }
        });
    </script>

    {{-- Dropzone JS --}}
    <script type="text/javascript" src="{{ asset('/js/dropzone.js')}}"></script>
    {{-- Bootbox JS--}}
    <script type="text/javascript" src="https://oss.maxcdn.com/bootbox/4.2.0/bootbox.min.js"></script>

@endsection