<script type="text/javascript">
    $(document).on('ready page:load', function () {
        $("#submit").click(function(e){
            e.preventDefault();
            var submit          =$("#submit");
            var associate       =$("#associate").find("option:selected" ).val();
            var booking         =$("input[name=booking]").val();
            var reservation     =$("input[name=reservation]").val();
            var date            =$("input[name=date]").val();
            var notes           =$("textarea[id=notes]").val();

                    @if($book->bok_type === 'Sea')
            //noinspection JSDuplicatedDeclaration
            var ship            =$("input[name=ship]").val();
                    //noinspection JSDuplicatedDeclaration
                    @elseif($book->bok_type === 'Land')
            var ship            =$("input[name=land]").val();
                    @endif
            var dataString      = 'booking='+ booking + '& associate=' + associate + '& reservation='+ reservation + '& date=' + date + '& notes=' + notes + '& ship=' + ship;
            //noinspection JSDuplicatedDeclaration
            $.ajax({
                type: "POST",
                  @if($book->bok_type === 'Sea')
                url : "{{action('UpdateController@updateBooking')}}",
                @else
                url : "{{action('UpdateController@updateLandBooking')}}",
                @endif
                data : dataString,
                cache: false,
                beforeSend: function(e) {
                    submit.attr("disabled", true);
                    submit.html('<i class="fa fa-spinner fa-spin"></i> Editing ...');
                    NProgress.start();
                    NProgress.set(0.25);
                    NProgress.inc();
                },
                success: function(e){
                    NProgress.set(0.99);
                    $('#updateModal').hide();
                    submit.attr("disabled", false);
                    alertify.alert("Booking has been successfully been edited.")
                            .set('onok', function(closeEvent){ NProgress.done(); window.location='{{url('booking/'.$book->bok_selector)}}';});
            },
                error: function(jqXhr){
                    NProgress.done();

                    if( jqXhr.status === 422 ) {
                        //process validation errors here.
                        // var errors = jqXhr.responseText;
                        var errors = jqXhr.responseJSON; //this will get the errors response data.
                        var errorsHtml = ' <strong><i class="fa fa-exclamation-circle"></i></strong> There were some problems with your input. <ul>';

                        $.each( errors, function( key, value ) {
                            errorsHtml += '<li>' + value[0] + '</li>'; //showing only the first error.
                        });
                        errorsHtml += '</ul>';

                        alertify.alert(errorsHtml).set('closable', true);
                        // $( '#error' ).html( errorsHtml ); //appending to a <div id="form-errors"></div> inside form
                        submit.attr("disabled", false);
                        submit.html(' <i class="fa fa-check"></i> Save Changes');
                    }
                    else {

                        alertify.alert('There is a serious error that is preventing this form from submitting properly. Report this bug to a designed administrator').set('closable', true);
                        // $( '#error' ).html( errorsHtml ); //appending to a <div id="form-errors"></div> inside form
                        submit.attr("disabled", true);
                        submit.html(' <i class="fa fa-times"></i> Error');
                    }
                }
            });
            return false;
        });
    });
</script>


<script type="text/javascript">
    $(function() {
        $( ".files" ).sortable({
            revert: true
        });
    });
</script>

@include('layouts._datepicker')

@if(count($book->customers) === 1)
<script type="text/javascript">
    $(function() {
        $(document).on('click','#modalOpen',function(e){
            //process here you can get id using
            $('#ID').val($(this).data('id')); //and set this id to any hidden field in modal
            $('#delete').val($(this).data('id')); //and set this id to any hidden field in modal
        });
    });
</script>

<script type="text/javascript">
    $(document).on('ready page:load', function () {
        $('#submit2').click(function(e) {
            e.preventDefault();
            var submit      = $('#submit2');  // submit button
            var url         = "{{action('DeleteController@deleteBook')}}";
            var booking     =$("input[name=booking]").val();
            var dataString  = 'booking='+ booking;
            //console.log(dataString);
            $.ajax({
                type: "post",
                url: url,
                data: dataString,
                cache: false,
                beforeSend: function(e) {
                    // alert.fadeOut();
                    submit.attr("disabled", true);
                    NProgress.start();
                    submit.html('<i class="fa fa-spinner fa-spin"></i> Deleting... Please wait'); // change submit button text
                },
                success: function(e){
                    NProgress.done();
                    //window.location.reload();
                    alertify.alert("Booking #{{$book->bok_reservation}} has been successfully deleted from the database.").set('onok', function(closeEvent){ window.location='{{asset('home')}}'});
                    submit.attr("disabled", false);
                    // return data;
                },
                error: function(e) {
                    NProgress.done();
                    submit.attr("disabled", false);
                    alertify.alert("Booking #{{$book->bok_reservation}} was not deleted.", function() {
                    });
                }
            });
            return false;
        });
    });
</script>

@else
<script type="text/javascript">
    $(function() {
        $(document).on('click','#modalOpen',function(e){
            //process here you can get id using

            $('input[name=customer]').val($(this).data('customer')); //and set this id to any hidden field in modal
            $('input[name=booking]').val($(this).data('booking')); //and set this id to any hidden field in modal
            console.log(e);
        });
    });
</script>

<script type="text/javascript">
    $(document).on('ready page:load', function () {
        $('#submit2').click(function(e) {
            e.preventDefault();
            var submit      = $('#submit2');  // submit button
            var url         = "{{action('RecordController@detachCustomer')}}";
            var booking     =$("input[name=booking]").val();
            var customer     =$("input[name=customer]").val();
            var dataString  = 'booking='+ booking + '& customer=' + customer;
            //console.log(dataString);
            $.ajax({
                type: "post",
                url: url,
                data: dataString,
                cache: false,
                beforeSend: function(e) {
                    // alert.fadeOut();
                    submit.attr("disabled", true);
                    NProgress.start();
                    submit.html('<i class="fa fa-spinner fa-spin"></i> Detaching Customer...'); // change submit button text
                },
                success: function(e){
                    NProgress.done();
                    //window.location.reload();
                    alertify.alert("Customer has been successfully detached.").set('onok', function(closeEvent){ window.location.reload()});
                    submit.attr("disabled", true);
                    submit.html('<i class="fa fa-check"></i> Function Completed'); // change submit button text
                    // return data;
                },
                error: function(e) {
                    NProgress.done();
                    submit.attr("disabled", false);
                    alertify.alert("Customer was not detached.", function() {
                    });
                    submit.html('Detach'); // change submit button text
                }
            });
            return false;
        });
    });
</script>

@endif

<script type="text/javascript">
    $(document).on('ready page:load', function () {
        $(document).on('click','#newImage',function(e){
            //process here you can get id using
            $('#ID').val($(this).data('id')); //and set this id to any hidden field in modal
        });
    });
</script>


{{-- Delete File --}}
<script type="text/javascript">
    $(document).on('ready page:load', function () {
        $('#submit10').click(function(e) {
            e.preventDefault();
            var submit      = $('#submit10');  // submit button
            var url         = "{{url('customer/booking/image/delete')}}";
            var image        =$("input[name=image]").val();
            var dataString  = 'image='+ image;
            //console.log(dataString);
            $.ajax({
                type: "post",
                url: url,
                data: dataString,
                cache: false,
                beforeSend: function(e) {
                    // alert.fadeOut();
                    NProgress.start();
                    submit.attr("disabled", true);
                    submit.html('<i class="fa fa-spinner fa-spin"></i> Deleting File... Please wait'); // change submit button text
                },
                success: function(e){
                    NProgress.set(0,99);
                    submit.attr("disabled", true);
                    submit.html('<i class="fa fa-check"></i> Function Completed'); // change submit button text
                    alertify.alert("The selected file has been successfully deleted from the server.").set('onok', function(closeEvent){  NProgress.done(); window.location.reload();} );

                   // return data;
                },
                error: function(e) {
                    NProgress.done();
                    alertify.alert("File was not deleted.", function() {
                    });
                    submit.html('Delete File'); // change submit button text
                    submit.attr("disabled", false);

                }
            });
            return false;
        });
    });
</script>

@if (count($book->customers) < 5)
<script type="text/javascript">
    $(function() {
        $(document).on('click','#modalOpen1',function(e){
            //process here you can get id using

            $('input[name=associate]').val($(this).data('associate')); //and set this id to any hidden field in modal
            $('input[name=booking]').val($(this).data('booking')); //and set this id to any hidden field in modal
            console.log(e);
        });
    });

    </script>

<script type="text/javascript">
    $(document).on('ready page:load', function () {
        $("#submit7").click(function(e){
            e.preventDefault();
            var submit          =$("#submit7");
            var booking         =$("input[name=booking]").val();
            var associate            =$("input[name=associate]").val();
            var firstName           =$("input[name=firstName]").val();
            var lastName           =$("input[name=lastName]").val();
            var notes            =$("textarea[name=notes]").val();
            var dataString      = 'booking='+ booking + '& associate=' + associate + '& firstName=' + firstName + '& lastName=' + lastName + '& notes=' + notes;
            $.ajax({
                type: "POST",
                url : "{{action('RecordController@newCustomerExistingBooking')}}",
                data : dataString,
                cache: false,
                beforeSend: function(e) {
                    submit.attr("disabled", true);
                    submit.html('<i class="fa fa-spinner fa-spin"></i> Creating ...');
                    NProgress.start();
                    NProgress.set(0.25);
                    NProgress.inc();
                },
                success: function(e){
                    NProgress.set(0.99);
                    //$('#updateModal').hide();
                    submit.attr("disabled", true);
                    submit.html('<i class="fa fa-check"></i> Function Completed'); // change submit button text
                    alertify.alert("New Customer has been created and added to the shown booking successfully.")
                            .set('onok', function(closeEvent){ NProgress.done(); window.location.reload();});
                },

                error: function(jqXhr){
                    NProgress.done();

                    if( jqXhr.status === 422 ) {
                        //process validation errors here.
                        // var errors = jqXhr.responseText;
                        var errors = jqXhr.responseJSON; //this will get the errors response data.
                        var errorsHtml = ' <strong><i class="fa fa-exclamation-circle"></i></strong> There were some problems with your input. <ul>';

                        $.each( errors, function( key, value ) {
                            errorsHtml += '<li>' + value[0] + '</li>'; //showing only the first error.
                        });
                        errorsHtml += '</ul>';

                        alertify.alert(errorsHtml).set('closable', true);
                        // $( '#error' ).html( errorsHtml ); //appending to a <div id="form-errors"></div> inside form
                        submit.attr("disabled", false);
                        submit.html('Submit');
                    }

                    else {

                        alertify.alert('There is a serious error that is preventing this form from submitting properly. Report this bug to a designed administrator').set('closable', true);
                        // $( '#error' ).html( errorsHtml ); //appending to a <div id="form-errors"></div> inside form
                        submit.attr("disabled", true);
                        submit.html(' <i class="fa fa-times"></i> Error');
                    }
                }

            });
            return false;
        });
    });
</script>

    @endif