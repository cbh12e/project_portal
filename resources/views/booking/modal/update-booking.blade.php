<div class="modal" id="updateModal" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content ">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Edit Booking with Reservation #{{$book->bok_reservation}}</h4>
            </div>
            <div class="modal-body">
                <form class="form-horizontal" method="post" id="199" role="form"  @if($book->bok_type === 'Land') action="{{action('UpdateController@updateLandBooking')}}" @endif>
                    <fieldset>
                        <!-- Booking ID Modal Update-->
                        <input type="hidden" name="booking" id="booking" value="@if( old('booking') == true){{old('booking')}}@else{{$book->bok_selector}}@endif">
                        <!-- CSRF Token -->
                        <input type="hidden" name="_token" value="{{ csrf_token() }}">

                        {{-- Associate --}}
                        <div class="form-group">
                            <label class="col-md-3 control-label" for="associate">Associate:</label>
                            <div class="col-md-5">
                                <div class="input-group">
                                    <span class="input-group-addon"><i class="fa fa-users"></i></span>
                                    <select name="associate" id="associate" class="form-control selectpicker">
                                        @if (all_agents())
                                            @foreach(all_agents() as $associate)
                                                <option value="{{ $associate->aso_id }}" {{ $book->aso_id == $associate->aso_id ? 'selected="selected"' : '' }}>{{ $associate->AssociateName }}</option>
                                            @endforeach
                                        @endif
                                    </select>
                                </div>
                            </div>
                        </div>


                        <!-- Reservation Modal Update-->
                        <div class="form-group">
                            <label class="col-md-3 control-label" for="reservation"> Reservation Number:</label>
                            <div class="col-md-5">
                                <div class="input-group">
                                    <span class="input-group-addon"><i class="fa fa-list-ol"></i></span>
                                    <input type="text" class="form-control" name="reservation" id="reservation"  value="@if( old('reservation') == true){{old('reservation')}}@else{{$book->bok_reservation}}@endif"/>
                                </div>
                            </div>
                        </div>

                        @if($book->bok_type === 'Sea')
                        <!-- Ship Name Modal Update-->
                        <div class="form-group">
                            <label class="col-md-3 control-label" for="ship"> Ship Name:</label>
                            <div class="col-md-8">
                                <div class="input-group">
                                    <span class="input-group-addon"><i class="fa fa-ship"></i></span>
                                    <input type="text" class="form-control" name="ship" id="ship"  value="@if( old('ship') == true){{old('ship')}}@else{{$book->ship->ship_name}}@endif"/>
                                </div>
                            </div>
                        </div>

                        @elseif($book->bok_type === 'Land')
                            <div class="form-group">
                                <label class="col-md-3 control-label" for="landT"> Vacation Type:</label>
                                <div class="col-md-8">
                                    <div class="input-group">
                                        <span class="input-group-addon"><i class="fa fa-hotel"></i></span>
                                        <input type="text" class="form-control" name="land" id="landT"  value="@if( old('land') == true){{old('land')}}@else{{$book->land->land_name}}@endif"/>
                                    </div>
                                </div>
                            </div>
                        @endif

                        <!-- Sail Date Modal Update-->
                        <div class="form-group">
                            <label class="col-md-3 control-label" for="date"> Sail Date</label>
                            <div class="col-md-4">
                                <div class="input-group">
                                    <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                                    <input type="text" class="form-control" name="date" id="bDate"  value="@if( old('date') == true){{old('date')}}@else{{human_date($book->bok_date)}}@endif"/>
                                </div>
                            </div>
                        </div>

                        <!-- Notes Modal Update -->
                        <div class="form-group">
                            <label class="col-md-3 control-label" for="notes">Notes: </label>
                            <div class="col-md-7">
                                @if(count(old('notes'))==1)
                                <textarea name="notes" id="notes" class="form-control" rows="5">{{old('notes')}}</textarea>
                                @elseif(count($book->bok_notes) == 1)
                                    <textarea name="notes" id="notes" class="form-control" rows="5">{{$book->bok_notes}}</textarea>
                                @else
                                    <textarea name="notes" id="notes" class="form-control" rows="5"></textarea>
                            @endif
                            </div>
                        </div>

                        <!-- Submit Button Modal Update-->
                        <div class="modal-footer">
                            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                            <button type="submit" id="submit" class="btn btn-primary" title="Submit a record"> <i class="fa fa-check"></i> Save Changes</button>
                        </div>
                    </fieldset>
                </form>
            </div>
        </div>
    </div>
</div>
