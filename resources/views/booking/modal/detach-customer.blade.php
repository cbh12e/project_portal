<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">

            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Detach Customer From Booking</h4>
            </div>
            <form class="form-horizontal" method="post" action="{{asset('booking/customer/detach')}}">

            <div class="modal-body">
                <p> This action will detach the selected customer from this booking. Proceed?</p>
                <input type="hidden" name="customer" id="customer">

                <!-- Customer ID -->
                <input type="hidden" name="booking" id="booking" >


                <input type="hidden" name="_token" value="{{ csrf_token() }}">
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                <button type="submit" class="btn btn-warning" id="submit2">Detach</button>
            </div>
                </form>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->