<div class="modal fade" id="aModal" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Add Customer to an Existing Booking</h4>
            </div>
            <div class="modal-body">
                <form class="form-horizontal" method="post" role="form" action="{{asset('booking/customer/add')}}">
                    <fieldset>
                        <!-- CSRF Token -->
                        <input type="hidden" name="_token" value="{{ csrf_token() }}">

                        <!-- Customer Select -->
                        <input type="hidden" name="id" id="id" value="{{ $menu->cus }}" ><br />

                        <!-- Reservation Number-->
                        <div class="form-group">
                            <label class="col-md-3 control-label" for="reservation">Reservation Number:</label>
                            <div class="col-md-8">
                                <div class="input-group">
                                    <span class="input-group-addon"><i class="fa fa-users"></i></span>
                                    <input type="text" name="reservation" id="reservationCba" class="form-control" placeholder="Reservation Number" value="{{old('reservation')}}" autocomplete="off" >
                                </div>
                            </div>
                        </div>
                    </fieldset>

                    <!-- Submit Button -->
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default pull-left" data-dismiss="modal" title="Close the modal.">Close</button>
                        <button type="submit" class="btn btn-primary" id="submitAbc" title="Add customer to group."><i class="fa fa-plus-circle"></i> Add </button>
                    </div>
                </form>
                <!-- /.modal-content -->
            </div><!-- /.modal-dialog -->
        </div>
    </div>
</div>