<div class="modal fade" id="cModal" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Add New Customer to an Existing Booking</h4>
            </div>
            <div class="modal-body">

                <p> This modal is used to add new customers that do not exist in the database. Should an existing customer be added, validation will fail.</p>

        <form class="form-horizontal" method="post" role="form" id="209B">
    <fieldset>
        <legend> Customer Information</legend>

        {{-- CSRF Token --}}
        <input type="hidden" name="_token" value="{{ csrf_token() }}">

        {{-- Booking ID--}}
        <input type="hidden" name="booking" value="booking">

        {{-- Associate ID --}}
        <input type="hidden" name="associate" value="associate">

        {{-- First Name --}}
        <div class="form-group">
            <label class="col-md-4 control-label" for="firstName">Customer First Name: *</label>
            <div class="col-md-7">
                <div class="input-group">
                    <span class="input-group-addon"><i class='fa fa-user'></i></span>
                    <input type="text" name="firstName" id="firstName"  placeholder="First Name" value="@if( old('firstName') == true){{old('firstName')}}@endif" class="form-control input-md" autocomplete="off" >
                </div>
            </div>
        </div>

        {{-- Last Name --}}
        <div class="form-group">
            <label class="col-md-4 control-label" for="lastName">Customer Last Name: *</label>
            <div class="col-md-7">
                <div class="input-group">
                    <span class="input-group-addon"><i class='fa fa-user'></i></span>
                    <input type="text" name="lastName" id="lastName" placeholder="Last Name" value="@if( old('lastName') == true){{old('lastName')}}@endif" class="form-control input-md"  autocomplete="off">
                </div>
            </div>
        </div>

        {{-- Notes --}}
        <div class="form-group">
            <label class="col-md-4 control-label" for="notes">Customer Notes:</label>
            <div class="col-md-7">
                @if(old('notes'))
                    <textarea name="notes" id="notes" class="form-control" rows="5">{{old('notes')}}</textarea>
                @else
                    <textarea name="notes" id="notes" class="form-control" rows="5"></textarea>
                @endif
            </div>
        </div>
    </fieldset>

    <!-- Submit Button -->
    <div class="modal-footer">
        <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Close</button>
        <button type="submit" class="btn btn-primary" id="submit7"><i class="fa fa-plus-circle"></i> Submit</button>
    </div>
</form>
                <!-- /.modal-content -->
            </div><!-- /.modal-dialog -->
        </div>
    </div>
</div>