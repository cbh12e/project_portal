<div class="modal fade" id="myLargeModal" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Delete Image</h4>
            </div>
            <form class="form-horizontal" method="post" action="{{url('customer/booking/image/delete')}}" id="delete" role="form">
                <div class="modal-body">
                    <p>Are you sure you want to delete this image? This action cannot be undone.</p>
                    <input type="hidden" class="form-control" name="image"  id="ID"/>
                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-danger" id="submit10"><i class="fa fa-times"></i> Delete</button>
                </div>
            </form>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->