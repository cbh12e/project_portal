<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Delete Booking</h4>
            </div>
            <div class="modal-body">
                <p>Are you sure you want to delete this Booking? All files associated with this booking will be deleted. This action cannot be undone.</p>
                <input type="hidden" name="booking"  id="ID"/>
                <input type="hidden" name="_token" value="{{ csrf_token() }}">
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal"> No</button>
                <button type="button" class="btn btn-danger" id="submit2"><i class="fa fa-times-circle"></i> Delete</button>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->