{{-- Booking Information Typeahead --}}
<script type="text/javascript">
    jQuery(document).ready(function($) {
        var engine = new Bloodhound({

            // '...' = displayKey: '...'
            datumTokenizer: Bloodhound.tokenizers.whitespace('reservation'), {{-- DISPLAYS THE VALUE OF WHAT WILL BE ENTERED IN INPUT--}}
            queryTokenizer: Bloodhound.tokenizers.whitespace,
            remote:
            {
                url: '{{typeahead('quincy/query?reservation=%QUERY')}}', {{-- URL NEEDED TO GET TO RUN THE QUERY --}}
                wildcard: '%QUERY' {{-- WILDCARD MUST BE THIS WAY OR TYPEAHEAD WILL NOT WORK--}}
            }
        });
        engine.initialize();
        $("#reservationCba").typeahead({
            hint: true,
            highlight: true,
            minLength: 2
        }, {
            source: engine.ttAdapter(),
            // This will be appended to "tt-dataset-" to form the class name of the suggestion menu.
            name: 'reservation',
            // the key from the array we want to display (name,id,email,etc...)
            displayKey: 'reset', {{-- INPUT VALUE OF THE FORM--}}
            templates: {
                empty: [
                    '<div class="user-search-result">' +  '<h4>'  + '&nbsp;<i class="fa fa-warning" style="color: red;"></i>' +  '&nbsp; No records were found' + '</h4>'+ '</div>'
                ].join('\n'),
                {{-- Suggestion bar displaying extra inputs --}}
                suggestion: function (data) {
                    return '<div class="user-search-result">'+ data.reset + '<span class="label pull-right bg-blue">' + data.count +'</span>' + '<h5>' + data.name + ' - ' + data.date + '</h5>'  + '</div>'
                }
            }
        });
    });
</script>