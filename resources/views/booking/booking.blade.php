@extends('app')
@section('css')
    <!-- Bootstrap Datepicker Override CSS for Modals -->
    <link type="text/css" href="{{ cdn('/css/datepicker-override.css') }}" rel="stylesheet">
    <link type="text/css" href="{{ cdn('/css/dropzone.css') }}" rel="stylesheet">

    @include('layouts._datatables-css')
    <!-- Datepicker Modal Override -->
    <style type="text/css">
      .datepicker {
            top: 185px !important;
        }
    </style>
@endsection
@section('content')

<div id="menu">
@foreach($books as $book) @endforeach
    <!-- Breadcrumbs -->
    <section class="content-header">
        <h1>
            <span class="hidden-sm hidden-xs"><i class="fa fa-book"></i> Booking ID  <small>{{$book->bok_id}} {{$book->bok_ship}} - {{ human_date($book->bok_date)}} </small> </span>
            <span class="visible-sm visible-xs"><i class="fa fa-book"></i> Booking ID  <small>{{$book->bok_id}} </small></span>
        </h1>
        <ol class="breadcrumb">
            <li><a href="{{url('/')}}"><i class="fa fa-dashboard"></i> Home</a></li>
            @if($book->group_id == false)
                <li><a href="{{url('search')}}"><i class="fa fa-search"></i> Search</a></li>
            @else
                <li><a href="{{expire(action('GroupController@showGroup', ['id' => $book->groups->group_selector]))}}"><i class="fa fa-users"></i> Group # {{$book->groups->group_number}}</a></li>
            @endif
            <li>
                <span class="active hidden-sm hidden-xs"><i class="fa fa-book"></i> Booking #{{$book->bok_id}} </span>
                <span class="active visible-sm visible-xs"><i class="fa fa-book"></i> {{$book->bok_ship}} - {{ human_date($book->bok_date)}}</span>
            </li>
        </ol>
    </section>
    <hr>

        <!-- Booking Options -->
    <a href="{{ expire(action('RecordController@addImage', ['id' => $book->bok_selector]))}}" class="btn btn-primary"><i class="fa fa-upload"></i> Upload Document</a>
    @if ($book->bok_notes == true)
            <button class="btn btn-info" data-toggle="modal" id="bookingNotes" data-n="{{$book->bok_notes}}"><i class="fa fa-sticky-note-o"></i> View Notes</button>
        @endif
        <button class="btn btn-success" data-toggle="modal" id="modalIdOpen" data-id="{{$book->bok_selector}}" data-target="#updateModal"><i class="fa fa-pencil-square"></i> Edit <span class="hidden-xs hidden-sm">Booking </span></button>

    @if (count($book->customers) === 1)
    <button class="btn btn-danger" data-toggle="modal" id="modalOpen" data-id="{{$book->bok_selector}}" data-target="#myModal"><i class="fa fa-trash"></i> Delete <span class="hidden-xs hidden-sm">Booking </span></button>
    @endif

    @if (count($book->customers) < 5)
    <button class="btn btn-info" data-toggle="modal" id="modalOpen1" data-booking="{{$book->bok_selector}}" data-associate="{{$book->associate->aso_id}}" data-target="#cModal"><i class="fa fa-plus-circle"></i> New Customer</button>
        @endif
</div>

    <hr>
<div class="container">

        @if(count($book->images) > 0)
            {{-- Customer Table--}}
        @include('table.customer-table')

        <div class="ajaxContent">
        {{-- Image Table --}}
        @include('table.image-table')
        </div>
        <div class="col-md-10">
            <!-- Booking ID -->
            <form class="form-horizontal" id="booking">
                <div class="form-group">
                    <label class="col-md-3 control-label" style="font-size:14px"> Reservation Number: </label>
                    <div class="col-md-2">
                        <p class="form-control-static" style="font-size:14px">{{$book->bok_reservation}}</p>
                    </div>
                </div>

                @if($book->group_id === true)
                    <!-- Group Number -->
                    <div class="form-group">
                        <label class="col-md-3 control-label" style="font-size:14px"> Group Number: </label>
                        <div class="col-md-5">
                            <p class="form-control-static" style="font-size:14px">{{$book->groups->group_number}}</p>
                        </div>
                    </div>

                    <!-- Group Name -->
                    <div class="form-group">
                        <label class="col-md-3 control-label" style="font-size:14px"> Group Name: </label>
                        <div class="col-md-5">
                            <p class="form-control-static" style="font-size:14px">{{$book->groups->group_name}}</p>
                        </div>
                    </div>
                @endif

                <!-- Agent Name -->
                <div class="form-group">
                    <label class="col-md-3 control-label" style="font-size:14px"> Associate: </label>
                    <div class="col-md-5">
                        <p class="form-control-static" style="font-size:14px">{{$book->associate->AssociateName}}</p>
                    </div>
                </div>

                @if ($book->bok_type === 'Sea')

                <!-- Ship Name -->
                <div class="form-group">
                    <label class="col-md-3 control-label" for="ship" style="font-size:14px"> Ship Name:</label>
                    <div class="col-md-5">
                        <p class="form-control-static" style="font-size:14px"> {{ $book->ship->ship_name }} </p>
                    </div>
                </div>

                <!-- Cruise Line -->
                <div class="form-group">
                    <label class="col-md-3 control-label" for="line" style="font-size:14px"> Cruise Line:</label>
                    <div class="col-md-5">
                        <p class="form-control-static" style="font-size:14px"> {{ $book->ship->cruiseLine->cl_name }} </p>
                    </div>
                </div>

                <!-- Sail Date -->
                <div class="form-group">
                    <label class="col-md-3 control-label" style="font-size:14px"> Date of Sailing:</label>
                    <div class="col-md-2">
                        <p class="form-control-static" style="font-size:14px"> {{ human_date($book->bok_date)}}</p>
                    </div>
                </div>
                @elseif($book->bok_type === 'Land')

                        <!-- Ship Name -->
                <div class="form-group">
                    <label class="col-md-3 control-label" for="ship" style="font-size:14px"> Vacation Provider:</label>
                    <div class="col-md-5">
                        <p class="form-control-static" style="font-size:14px"> {{ $book->land->land_name }} </p>
                    </div>
                </div>

                <!-- Sail Date -->
                <div class="form-group">
                    <label class="col-md-3 control-label" style="font-size:14px"> Vacation Date:</label>
                    <div class="col-md-2">
                        <p class="form-control-static" style="font-size:14px"> {{ human_date($book->bok_date)}}</p>
                    </div>
                </div>
                @endif
            </form>

            {{-- Delete Image Modal --}}
            @include('booking.modal.image-delete')

        @else
                <div class="row">
                    <div class="col-md-8 col-md-offset-1">
                        <div class="jumbotron">
                            <div class="container">
                                <h1 class="text-yellow"><i class="fa fa-warning"></i> Alert</h1>
                                <p> There are no documents associated with this booking.</p>
                                <p> <a href="{{ expire(action('RecordController@addImage', ['id' => $book->bok_selector]))}}" class="btn btn-primary btn-lg" role="button"><i class="fa fa-upload"></i> Upload Document</a></p>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                {{-- Customer Table --}}
            @include('table.customer-table')
            </div>
                        <form class="form-horizontal">
                            <div class="form-group">
                                <label class="col-md-3 control-label" style="font-size:22px"> Reservation Number: </label>
                                <div class="col-md-2">
                                    <p class="form-control-static" style="font-size:22px">{{$book->bok_reservation}}</p>
                                </div>
                            </div>

                            @if($book->group_id == true)
                                <div class="form-group">
                                    <label class="col-md-3 control-label" style="font-size:22px"> Group Number: </label>
                                    <div class="col-md-5">
                                        <p class="form-control-static" style="font-size:22px">{{$book->groups->group_number}}</p>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="col-md-3 control-label" style="font-size:22px"> Group Name: </label>
                                    <div class="col-md-8">
                                        <p class="form-control-static" style="font-size:22px">{{$book->groups->group_name}}</p>
                                    </div>
                                </div>
                            @endif

                            <div class="form-group">
                                <label class="col-md-3 control-label" style="font-size:22px"> Associate: </label>
                                <div class="col-md-5">
                                    <p class="form-control-static" style="font-size:22px">{{$book->associate->AssociateName}}</p>
                                </div>
                            </div>
                            @if($book->bok_type === 'Sea')
                                <div class="form-group">
                                <label class="col-md-3 control-label" for="ship" style="font-size:22px"> Ship Name:</label>
                                <div class="col-md-5">
                                    <p class="form-control-static" style="font-size:22px"> {{ $book->ship->ship_name }} </p>
                                </div>
                            </div>
                                <div class="form-group">
                                    <label class="col-md-3 control-label" for="line" style="font-size:22px"> Cruise Line:</label>
                                    <div class="col-md-5">
                                        <p class="form-control-static" style="font-size:22px"> {{ $book->ship->cruiseLine->cl_name }} </p>
                                    </div>
                                </div>
                            <div class="form-group">
                                <label class="col-md-3 control-label" style="font-size:22px"> Date of Sailing:</label>
                                <div class="col-md-2">
                                    <p class="form-control-static" style="font-size:22px"> {{ human_date($book->bok_date)}}</p>
                                </div>
                            </div>
                                @elseif($book->bok_type === 'Land')
                                <div class="form-group">
                                    <label class="col-md-3 control-label" for="vacation" style="font-size:22px"> Vacation Provider:</label>
                                    <div class="col-md-5">
                                        <p class="form-control-static" style="font-size:22px"> {{ $book->land->land_name }} </p>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="col-md-3 control-label" style="font-size:22px"> Vacation Date:</label>
                                    <div class="col-md-2">
                                        <p class="form-control-static" style="font-size:22px"> {{ human_date($book->bok_date)}}</p>
                                    </div>
                                </div>
                            @endif
                        </form>
        </div>
    @endif
</div>

    {{-- Booking Update Modal --}}
    @include('booking.modal.update-booking')
<div id="info">

    @if (count($book->customers) === 1)
        @include('booking.modal.delete-booking')
    @else
        @include('booking.modal.detach-customer')
    @endif

        @include('booking.modal.create-customer')

</div>
@endsection
@section('javascript')
    {{-- Ship Name Search Typeahead --}}
    @include('layouts.ship-typeahead')
    {{--  Dropzone JS --}}
    <script type="text/javascript" src="{{ cdn('/js/dropzone.js')}}" ></script>

    {{-- Bootbox JS--}}
    {!! bootBox() !!}

    @include('layouts.pagination', ['dataType' => 'listImage', 'id' => $book->bok_selector])

    {{-- JavaScript Scripts --}}
    @include('booking.js')
    {{-- CSRF Token AJAX --}}
    @include('customer.csrf')
    {{--  Delete Modal Click --}}
    <script type="text/javascript">
        $(function() {
            $(document).on('click','#modalIdOpen',function(e){ //process here you can get id using
             $('#ID').val($(this).data('id')); //and set this id to any hidden field in modal
              });
        });
    </script>
    {{-- Booking Notes Bootbox Modal --}}
    <script type="text/javascript">
        $(document).on('click','#bookingNotes', function() {
            bootbox.dialog({
                title: 'Notes',
                message: $(this).data('n'),
                show: true,
                buttons: {
                    success: {
                        label: "Close",
                        className: "btn-primary",
                        callback: function () {

                        }
                    }
                }
            });
        });
    </script>

    {{-- Image Notes Bootbox Modal --}}
    <script type="text/javascript">
        $(document).on('click','#imgNotes', function() {
            bootbox.dialog({
                title: 'Notes for File',
                message: $(this).data('notes'),
                show: true,
                buttons: {
                    success: {
                        label: "Close",
                        className: "btn-primary",
                        callback: function () {}
                    }
                }
            });
        });
    </script>

    <script type="text/javascript">
        $('#submit1').on('click', function () {
            var $btn = $(this).button('loading');
    // business logic...
        $btn.button('reset')
        })
    </script>

    @include('layouts._datatables-js')

    <!-- DataTables JS -->
    <script type="text/javascript">
        $(function () {
            $('#customerTable').dataTable({
                "bPaginate": false,
                "bSort": true,
                "searching": false,
                "bInfo": false,
                "bAutoWidth": true,
                "bOrder": [[ 1, "asc" ]]
            });
        });
    </script>

    <!-- DataTables JS -->
    <script type="text/javascript">
        $(function () {
            $('#manager').dataTable({
                "bPaginate": true,
                "bSort": true,
                "bInfo": false,
                "searching": false,
                "bAutoWidth": true,
                "lengthMenu": [[5,10,15,25,50,75,100, -1], [5,10,15,25,50,75,100, "All"]],
                "bOrder": [[ 1, "asc" ]]
            });
        });
    </script>

    <!-- Land Sea Radio Selector -->
    @include('layouts.land-sea')

@endsection
@section('title')
Booking {{$book->bok_id}}
@endsection

@section('validation')
    {!! JsValidator::formRequest('App\Http\Requests\Booking\ExistingBookingNewCustomerRequest', '#209B') !!}
@endsection

@section ('icheck')
@overwrite