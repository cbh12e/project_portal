/**
 * Created by Casey Hargarther on 2/9/2016.
 */


/**
 * Overrides the default alertify js theme with Bootstrap
 */
alertify.defaults.transition = "slide";
alertify.defaults.theme.ok = "btn btn-primary";
alertify.defaults.theme.cancel = "btn btn-danger";
alertify.defaults.theme.input = "form-control";