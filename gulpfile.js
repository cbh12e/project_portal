var elixir = require('laravel-elixir');

/*
 |--------------------------------------------------------------------------
 | Elixir Asset Management
 |--------------------------------------------------------------------------
 |
 | Elixir provides a clean, fluent API for defining some basic Gulp tasks
 | for your Laravel application. By default, we are compiling the Less
 | file for our application, as well as publishing vendor resources.
 |
 */

elixir(function(mix) {
    mix.less('app.less');
});

elixir(function(mix) {
 mix.copy('vendor/almasaeed2010/adminlte/bootstrap', 'resources/assets/AdminLTE/bootstrap');
 mix.copy('vendor/almasaeed2010/adminlte/dist', 'resources/assets/AdminLTE/dist');
 mix.copy('vendor/almasaeed2010/adminlte/plugins', 'resources/assets/AdminLTE/plugins');
 mix.copy('public/css', 'resources/assets/css/folder');
 mix.copy('public/js', 'resources/assets/js/folder');
});

elixir(function(mix) {
 mix.styles([
  "../AdminLTE/bootstrap/css/bootstrap.css",
  "../AdminLTE/dist/css/AdminLTE.css",
  "../AdminLTE/dist/css/skins/skin-blue.css",
  "../AdminLTE/dist/css/skins/skin-red.css"
 ], 'public/css/AdminLTE-combined.css');
});


elixir(function(mix) {
 mix.styles([
  "/folder/typeahead.css",
  "/folder/select2-bootstrap.css",
  "/folder/nprogress.css"
 ], 'public/css/auth-overrides-combined-1.css');
});

elixir(function(mix) {
 mix.styles([
  "/folder/overrides.css",
  "/folder/datepicker/bootstrap-datepicker.css"
 ], 'public/css/auth-overrides-combined-2.css');
});

elixir(function(mix) {
 mix.scripts([
  "../AdminLTE/plugins/jQuery/jQuery-2.1.4.min.js",
  "../AdminLTE/bootstrap/js/bootstrap.min.js",
     "../AdminLTE/plugins/jQueryUI/jquery-ui.js",
     "folder/datepicker/bootstrap-datepicker.js"

 ], 'public/js/corejs-combined.js');
});

elixir(function(mix) {
 mix.scripts([
  "../AdminLTE/plugins/iCheck/icheck.min.js",
  "../AdminLTE/plugins/slimScroll/jquery.slimscroll.min.js"
 ], 'public/js/icheck-jquery-combined.js');
});
